import { configInfo } from '@/api/config';
import { articleTypeList } from '@/api/articleType';
import { linkList } from '@/api/link';
export default defineNuxtRouteMiddleware(async (to, from) => {
  const config = useConfig();
  const articleType = useArticleType();
  const linkData = useLinkData();
  const defaultActive: any = useDefaultActive();
  // 判断是否为刷新页面
  if (to.path === from.path) {
    const configInfoRes = await configInfo();
    if (configInfoRes.status === 20000) {
      config.value = configInfoRes.data;
    } else {
      config.value = {};
    }
    const typelistRes = await articleTypeList();
    if (typelistRes.status === 20000) {
      articleType.value = typelistRes.data;
    } else {
      articleType.value = null;
    }
    const linkListtRes = await linkList();
    if (linkListtRes.status === 20000 && linkListtRes.data.list.length > 0) {
      linkData.value = linkListtRes.data.list;
    } else {
      linkData.value = {};
    }
  }
  if (config.value.baidu_statistics_url) {
    useHead({
      script: [{ src: config.value.baidu_statistics_url }],
    });
  }
  if (articleType.value && articleType.value.length) {
    let title = '';
    let keywords = '';
    let description = '';
    if (to.path == '/') {
      title = config.value.title || '';
      if (articleType.value[0].seo_title) {
        title += '-' + articleType.value[0].seo_title || '';
      }
      if (articleType.value[0].seo_keyword) {
        keywords = articleType.value[0].seo_keyword || '';
      }
      if (articleType.value[0].seo_description) {
        description = articleType.value[0].seo_description || '';
      }
      defaultActive.value = '0';
    } else if (
      to.path + '.html' == '/articleType/' + to.params.identification ||
      to.path + '.html' == '/singlePage/' + to.params.identification
    ) {
      if (!defaultActive.value) {
        defaultActive.value = to.params.identification + '';
      }
      const typeIndexArr = defaultActive.value.split('-');
      title = config.value.title || '';
      if (articleType.value[typeIndexArr[0]].seo_title) {
        title += '-' + articleType.value[typeIndexArr[0]].seo_title;
        if (articleType.value[typeIndexArr[0]].seo_keyword) {
          keywords = articleType.value[typeIndexArr[0]].seo_keyword || '';
        }
        if (articleType.value[typeIndexArr[0]].seo_description) {
          description = articleType.value[typeIndexArr[0]].seo_description || '';
        }
      }
      if (
        typeIndexArr[1] &&
        articleType.value[typeIndexArr[0]].children[typeIndexArr[1]].seo_title
      ) {
        title += '-' + articleType.value[typeIndexArr[0]].children[typeIndexArr[1]].seo_title;
        if (articleType.value[typeIndexArr[0]].children[typeIndexArr[1]].seo_keyword) {
          keywords = articleType.value[typeIndexArr[0]].children[typeIndexArr[1]].seo_keyword || '';
        }
        if (articleType.value[typeIndexArr[0]].children[typeIndexArr[1]].seo_description) {
          description =
            articleType.value[typeIndexArr[0]].children[typeIndexArr[1]].seo_description || '';
        }
      }
    }
    const meta = [];
    if (keywords) {
      meta.push({
        name: 'keywords',
        content: keywords,
      });
    }
    if (description) {
      meta.push({
        name: 'description',
        content: description,
      });
    }
    const linkHead: any = [];
    if (config.value.ico_image) {
      linkHead.push({
        rel: 'icon',
        href: config.value.ico_image.http_url,
      });
    }
    useHead({
      title: title,
      meta: meta,
      link: linkHead,
    });
  }

  //   console.log('要去那个页面:' + to.path);
  //   console.log('来自那个页面:' + from.path);
  // abortNavigation()  //停止当前导航，可以使用error进行报错
  //  return  navigateTo('/')
});
