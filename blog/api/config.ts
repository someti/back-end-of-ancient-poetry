export async function configInfo() {
  return await useRequest('config/info', {
    method: 'POST',
  });
}
