export async function articleDetails(data: any) {
  return await useRequest('article/details', {
    method: 'POST',
    params: data,
  });
}
