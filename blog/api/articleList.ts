export async function articleList(data: any = {}) {
  if (!data.limit) {
    data.limit = 20;
  }
  if (!data.page) {
    data.page = 1;
  }
  return await useRequest('article/list', {
    method: 'POST',
    params: data,
  });
}
