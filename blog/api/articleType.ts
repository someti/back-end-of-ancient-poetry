export async function articleTypeList() {
  return await useRequest('articleType/list', {
    method: 'POST',
  });
}
