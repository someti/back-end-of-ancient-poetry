import { Request, Response } from 'express';

import { waitTime } from '../src/utils/utils';
const list = {
  success: true,
  data: [
    {
      id: 1,
      name: 'admin',
      status: 1,
      sort: 1,
      created_at: '2021-11-27 23:40:07',
      updated_at: null,
    },
    {
      id: 2,
      name: 'test',
      status: 0,
      sort: 2,
      created_at: '2021-11-27 23:40:09',
      updated_at: null,
    },
  ],
  total: 2,
};
export default {
  // 支持值为 Object 和 Array
  'GET /api/getList': async (req: Request, res: Response) => {
    await waitTime(2000);
    return res.json(list);
  },
};
