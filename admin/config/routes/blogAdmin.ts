export default [
  {
    path: '/blogAdmin',
    redirect: '/blogAdmin/dashboard',
  },
  {
    path: '/blogAdmin/dashboard',
    component: './admin/dashboard/index',
  },
  // {
  //   path: '/blogAdmin',
  //   redirect: '/blogAdmin/dashboard',
  // },

  // {
  //   path: '/blogAdmin/dashboard',
  //   component: './blogAdmin/dashboard/index',
  // },
  {
    path: '/blogAdmin/article/articleType',
    component: './blogAdmin/article/articleType/index',
  },
  {
    path: '/blogAdmin/article/label',
    component: './blogAdmin/article/label/index',
  },
  {
    path: '/blogAdmin/article/article',
    component: './blogAdmin/article/article/index',
  },
  {
    path: '/blogAdmin/setting/link',
    component: './blogAdmin/setting/link/index',
  },
  {
    path: '/blogAdmin/setting/picture',
    component: './blogAdmin/setting/picture/index',
  },
  {
    path: '/blogAdmin/setting/project',
    component: './blogAdmin/setting/project/index',
  },
  {
    path: '/blogAdmin/crawler/video',
    component: './blogAdmin/crawler/video/index',
  },
];
