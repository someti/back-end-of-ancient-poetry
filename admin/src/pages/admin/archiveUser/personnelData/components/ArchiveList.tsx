import React, { useRef, useState } from 'react';
import { Modal, Button } from 'antd';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { getArchiveList } from '@/services/admin/archiveUser/personnelData/api';
import CImageInfoArr from '@/components/common/CImageInfoArr';
interface PorpsType {
  id?: any;
}
const ArchiveList: React.FC<PorpsType> = (porps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const actionRef = useRef<ActionType>();
  const columns: ProColumns<API.AdminArchiveTypeList>[] = [
    {
      title: '模板名称',
      dataIndex: 'name',
      hideInSearch: true,
      width: 200,
      fixed: true,
    },
    {
      title: '模板描述',
      dataIndex: 'description',
      hideInSearch: true,
      width: 200,
      align: 'center',
      copyable: true,
      ellipsis: true,
    },
    {
      title: '上传描述',
      dataIndex: 'sc_description',
      hideInSearch: true,
      width: 200,
      align: 'center',
      copyable: true,
      ellipsis: true,
    },
    {
      title: '文件',
      dataIndex: 'images',
      hideInSearch: true,
      width: 100,
      align: 'center',
      render: (text, record) => [<CImageInfoArr list={record.images} key={record.id} />],
    },
  ];
  return (
    <>
      <Button
        type="primary"
        size="small"
        onClick={() => {
          setIsModalOpen(true);
        }}
      >
        档案明细
      </Button>
      <Modal
        title="档案明细"
        width={1300}
        visible={isModalOpen}
        destroyOnClose
        footer={null}
        onCancel={() => {
          setIsModalOpen(false);
        }}
      >
        <ProTable
          scroll={{ x: 1100 }}
          actionRef={actionRef}
          rowKey="id"
          columns={columns}
          request={async () => {
            const res = await getArchiveList({ personnel_id: porps.id });
            return { success: true, data: res.data };
          }}
          options={{ fullScreen: true }}
          search={false}
          pagination={false}
        />
      </Modal>
    </>
  );
};
export default ArchiveList;
