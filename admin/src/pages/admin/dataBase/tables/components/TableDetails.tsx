import React, { useRef, useState } from 'react';
import { Modal, Space } from 'antd';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { exportExcel } from '@/utils/ExportExcel';
import { gettableData } from '@/services/admin/dataBase/api';
interface PorpsType {
  name: string;
}
const TableDetails: React.FC<PorpsType> = (porps) => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [tableData, setTableData] = useState<any>(null);
  let maps = {};
  const columns: ProColumns<API.AdminDataBasetableDataList>[] = [
    {
      title: '字段名',
      dataIndex: 'COLUMN_NAME',
      align: 'center',
      width: 70,
      fixed: true,
      hideInSearch: true,
    },
    {
      title: '数据类型',
      dataIndex: 'COLUMN_TYPE',
      align: 'center',
      hideInSearch: true,
      width: 80,
    },
    {
      title: '默认值',
      dataIndex: 'COLUMN_DEFAULT',
      align: 'center',
      hideInSearch: true,
      width: 40,
    },
    {
      title: '允许非空',
      dataIndex: 'IS_NULLABLE',
      align: 'center',
      hideInSearch: true,
      width: 45,
    },
    {
      title: '自动递增',
      dataIndex: 'EXTRA',
      align: 'center',
      hideInSearch: true,
      width: 45,
    },
    {
      title: '备注',
      dataIndex: 'COLUMN_COMMENT',
      align: 'center',
      hideInSearch: true,
      width: 100,
    },
  ];
  return (
    <>
      <a
        onClick={async () => {
          await setIsModalVisible(true);
        }}
      >
        {porps.name}
      </a>
      <Modal
        title={'表' + porps.name + '详情'}
        visible={isModalVisible}
        width={750}
        footer={null}
        onCancel={() => {
          setIsModalVisible(false);
        }}
      >
        <ProTable
          scroll={{ x: 750 }}
          actionRef={actionRef}
          rowKey="COLUMN_NAME"
          columns={columns}
          search={false}
          pagination={false}
          request={async () => {
            const res = await gettableData({ table: porps.name });
            await setTableData(res.data);
            return { success: true, data: res.data.data };
          }}
          options={{ fullScreen: true }}
          rowSelection={{ fixed: true }}
          tableAlertRender={({ selectedRowKeys, onCleanSelected }) => (
            <Space size={24}>
              <span>
                已选 {selectedRowKeys.length} 项
                <a style={{ marginLeft: 8 }} onClick={onCleanSelected}>
                  取消选择
                </a>
              </span>
            </Space>
          )}
          columnsState={{
            onChange: (map) => {
              maps = map;
            },
          }}
          tableAlertOptionRender={({ selectedRows }) => {
            return (
              <Space size={16}>
                <a
                  onClick={() => {
                    exportExcel({
                      fileName: '表' + porps.name + '详情',
                      columns,
                      maps,
                      selectedRows,
                    });
                  }}
                >
                  导出数据
                </a>
              </Space>
            );
          }}
          headerTitle={
            tableData && (
              <>
                <Space size={24}>
                  <span>共计 {tableData.tableNum} 个字段）</span>
                </Space>
              </>
            )
          }
        />
      </Modal>
    </>
  );
};
export default TableDetails;
