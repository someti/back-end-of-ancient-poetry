import React, { useState } from 'react';
import { Drawer, Divider } from 'antd';
import ReactJson from 'react-json-view';
const Details: React.FC<API.AdminOperationLogList> = (porps) => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  return (
    <>
      <a
        onClick={async () => {
          await setIsModalVisible(true);
        }}
      >
        {porps.name}
      </a>
      <Drawer
        title={'请求:' + porps.name + '详情'}
        visible={isModalVisible}
        width={750}
        onClose={() => {
          setIsModalVisible(false);
        }}
      >
        <Divider orientation="left">Header</Divider>
        <ReactJson src={JSON.parse(porps.header)} />

        <Divider orientation="left">Data</Divider>
        <ReactJson src={JSON.parse(porps.data)} />
      </Drawer>
    </>
  );
};
export default Details;
