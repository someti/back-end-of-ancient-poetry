import React, { useEffect, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Spin } from 'antd';
import { useModel } from 'umi';
const Index: React.FC = () => {
  const { initialState, setInitialState } = useModel('@@initialState');
  const [configInfo, setConfigInfo] = useState<any>({});
  const fetchApi = async () => {
    const config = await initialState?.fetchConfigInfo?.();
    if (config) {
      await setInitialState((s: any) => ({
        ...s,
        currentConfig: config,
      }));
    }
    await setConfigInfo(config);
  };
  useEffect(() => {
    fetchApi();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <PageContainer fixedHeader={true} title={false}>
      <Spin spinning={configInfo.logViewerUrl ? false : true}>
        <iframe src={configInfo.logViewerUrl} width="100%" height="1000px" />
      </Spin>
    </PageContainer>
  );
};
export default Index;
