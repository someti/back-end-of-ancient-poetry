import React, { useRef } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { getList, setStatus, del, setSorts } from '@/services/admin/information/archiveType/api';
import { Switch, message } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import FormIndex from './components/FormIndex';
import CDel from '@/components/common/CDel';
import NumberInput from '@/components/common/NumberInput';
import { getSort } from '@/utils/utils';
const Index: React.FC = () => {
  const actionRef = useRef<ActionType>();
  const columns: ProColumns<API.AdminArchiveTypeList>[] = [
    {
      title: '模板名称',
      dataIndex: 'name',
      hideInSearch: true,
      width: 200,
      fixed: true,
    },
    {
      title: '模板描述',
      dataIndex: 'description',
      hideInSearch: true,
      width: 200,
      align: 'center',
      copyable: true,
      ellipsis: true,
    },
    {
      title: '排序',
      dataIndex: 'sort',
      hideInSearch: true,
      sorter: true,
      width: 80,
      align: 'center',
      render: (text, record) => [
        <NumberInput
          key={record.id}
          value={record.sort}
          onBlur={async (value: number) => {
            const res = await setSorts(record.id, { sort: value });
            if (res.status === 20000) {
              message.success(res.message);
            } else {
              actionRef.current?.reload();
            }
          }}
        />,
      ],
    },
    {
      title: '状态',
      dataIndex: 'status',
      hideInSearch: true,
      width: 100,
      align: 'center',
      render: (text, record) => [
        <Switch
          key={record.id + record.status}
          checkedChildren="显示"
          unCheckedChildren="隐藏"
          defaultChecked={record.status === 1 ? true : false}
          onChange={async (checked: boolean) => {
            const res = await setStatus(record.id, { status: checked ? 1 : 0 });
            if (res.status === 20000) {
              message.success(res.message);
            } else {
              actionRef.current?.reload();
            }
          }}
        />,
      ],
    },
    {
      title: '创建时间',
      dataIndex: 'created_at',
      key: 'created_at',
      valueType: 'dateTime',
      sorter: true,
      hideInSearch: true,
      width: 180,
      align: 'center',
    },
    {
      title: '更新时间',
      key: 'updated_at',
      dataIndex: 'updated_at',
      valueType: 'dateTime',
      sorter: true,
      hideInSearch: true,
      width: 180,
      align: 'center',
    },
    {
      title: '操作',
      key: 'option',
      width: 300,
      valueType: 'option',
      align: 'center',
      fixed: 'right',
      render: (_, row) => [
        <FormIndex
          pid={row.id}
          key={row.id}
          onConfirm={() => {
            return actionRef.current?.reload();
          }}
        />,
        <FormIndex
          id={row.id}
          key={row.id}
          onConfirm={() => {
            return actionRef.current?.reload();
          }}
        />,
        <CDel
          key={row.id}
          onCancel={async () => {
            const res = await del(row.id);
            if (res.status === 20000) {
              message.success(res.message);
              actionRef.current?.reload();
            }
          }}
        />,
      ],
    },
  ];
  return (
    <PageContainer fixedHeader={true} title={false}>
      <ProTable
        scroll={{ x: 1300 }}
        actionRef={actionRef}
        rowKey="id"
        columns={columns}
        request={async (params, sort) => {
          const data = await getSort(params, sort);
          const res = await getList(data);
          return { success: true, data: res.data };
        }}
        options={{ fullScreen: true }}
        search={false}
        pagination={false}
        headerTitle={
          <FormIndex
            onConfirm={() => {
              return actionRef.current?.reload();
            }}
          />
        }
      />
    </PageContainer>
  );
};
export default Index;
