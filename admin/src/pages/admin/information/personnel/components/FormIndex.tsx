import React, { useRef, useState } from 'react';
import { Button, message, Form, Input, Radio, Spin, Select, DatePicker } from 'antd';
import { DrawerForm } from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { PlusOutlined, FormOutlined } from '@ant-design/icons';
import { add, edit, update } from '@/services/admin/information/personnel/api';
import { getDepartmentList } from '@/services/admin/information/department/api';
interface PorpsType {
  onConfirm: () => void;
  id?: number;
}
const FormIndex: React.FC<PorpsType> = (porps) => {
  const restFormRef = useRef<ProFormInstance>();
  const [loading, setLoading] = useState(porps.id ? false : true);
  const [departmentList, setDepartmentList] = useState<any>([]);
  const [departmentListLoading, setDepartmentListLoading] = useState<boolean>(false);
  const formData: API.AdminPersonnelForm = {
    id: null,
    name: '',
    status: 1,
    department_id: null,
    sex: 0,
    nation: '',
    date_of_birth: '',
    education: null,
    marital_status: 0,
    position: '',
    political_outlook: '',
    personnel_type: '',
    entry_time: '',
  };
  return (
    <DrawerForm<API.AdminPersonnelForm>
      layout="horizontal"
      labelCol={{ span: 3 }}
      wrapperCol={{ span: 20 }}
      formRef={restFormRef}
      initialValues={formData}
      title={porps.id ? '编辑' : '添加'}
      autoFocusFirstInput
      isKeyPressSubmit
      trigger={
        porps.id ? (
          <Button type="primary" size="small">
            <FormOutlined />
            编辑
          </Button>
        ) : (
          <Button type="primary">
            <PlusOutlined />
            添加
          </Button>
        )
      }
      drawerProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        render: (props, defaultDoms) => {
          return [
            ...defaultDoms,
            <Button
              loading={loading ? false : true}
              key="extra-reset"
              onClick={async () => {
                const id = props.form?.getFieldValue('id');
                if (id) {
                  await setLoading(false);
                  const res = await edit(id);
                  if (res.status === 20000) {
                    restFormRef.current?.setFieldsValue(res.data);
                    await setLoading(true);
                  }
                } else {
                  props.reset();
                }
              }}
            >
              重置
            </Button>,
          ];
        },
      }}
      onFinish={async (values: any) => {
        let res: any = {};
        if (porps.id) {
          res = await update(porps.id, values);
        } else {
          res = await add(values);
        }
        if (res.status === 20000) {
          message.success(res.message);
          porps.onConfirm();
          return true;
        }
        return false;
      }}
      onVisibleChange={async (visible) => {
        if (visible) {
          if (porps.id) {
            await setLoading(false);
            const res = await edit(porps.id);
            if (res.status === 20000) {
              await restFormRef.current?.setFieldsValue(res.data);
              await setLoading(true);
            }
          }

          await setDepartmentListLoading(true);
          const routeRes = await getDepartmentList();
          if (routeRes.status == 20000) {
            await setDepartmentListLoading(false);
            await setDepartmentList(routeRes.data);
          }
        }
      }}
    >
      <Spin spinning={loading ? false : true}>
        {loading && (
          <>
            <Form.Item label="姓名" name="name" rules={[{ required: true, message: '请输入姓名' }]}>
              <Input allowClear placeholder="请输入姓名" />
            </Form.Item>
            <Form.Item
              label="部门"
              name="department_id"
              rules={[{ required: true, message: '请选择部门' }]}
            >
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={departmentListLoading}
                notFoundContent="暂无部门"
                placeholder="请选择部门"
              >
                {departmentList.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item name="sex" label="性别" rules={[{ required: true, message: '请选择性别' }]}>
              <Radio.Group name="sex">
                <Radio value={0}>女</Radio>
                <Radio value={1}>男</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item
              label="民族"
              name="nation"
              rules={[{ required: true, message: '请输入民族' }]}
            >
              <Input allowClear placeholder="请输入民族" />
            </Form.Item>
            <Form.Item
              label="出生日期"
              name="date_of_birth"
              rules={[{ required: true, message: '请选择出生日期' }]}
            >
              <DatePicker placeholder="请选择出生日期" allowClear style={{ width: '100%' }} />
            </Form.Item>
            <Form.Item
              label="学历"
              name="education"
              rules={[{ required: true, message: '请选择学历' }]}
            >
              <Select allowClear placeholder="请选择学历">
                <Select.Option value="1">小学</Select.Option>
                <Select.Option value="2">初中</Select.Option>
                <Select.Option value="3">高中</Select.Option>
                <Select.Option value="4">大专</Select.Option>
                <Select.Option value="5">本科</Select.Option>
                <Select.Option value="6">研究生</Select.Option>
                <Select.Option value="7">博士</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="marital_status"
              label="婚姻状况"
              rules={[{ required: true, message: '请选择婚姻状况' }]}
            >
              <Radio.Group name="marital_status">
                <Radio value={0}>未婚</Radio>
                <Radio value={1}>已婚</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item label="职位" name="position">
              <Input allowClear placeholder="请输入职位" />
            </Form.Item>
            <Form.Item label="政治面貌" name="political_outlook">
              <Input allowClear placeholder="请输入政治面貌" />
            </Form.Item>
            <Form.Item label="人员类别" name="personnel_type">
              <Input allowClear placeholder="请输入人员类别" />
            </Form.Item>
            <Form.Item label="入职时间" name="entry_time">
              <DatePicker placeholder="请选择入职时间" allowClear style={{ width: '100%' }} />
            </Form.Item>
            <Form.Item
              name="status"
              label="状态"
              rules={[{ required: true, message: '请选择状态' }]}
            >
              <Radio.Group name="status">
                <Radio value={0}>禁用</Radio>
                <Radio value={1}>启用</Radio>
              </Radio.Group>
            </Form.Item>
          </>
        )}
      </Spin>
    </DrawerForm>
  );
};
export default FormIndex;
