import React, { useRef, useState, useEffect } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { getList, setStatus, del, delAll } from '@/services/admin/information/archive/api';
import { getPidList } from '@/services/admin/information/archiveType/api';
import { getDepartmentList } from '@/services/admin/information/department/api';
import { getPersonnelList } from '@/services/admin/information/personnel/api';
import { Switch, Space, message } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { exportExcel } from '@/utils/ExportExcel';
import { getSort, setLsetData } from '@/utils/utils';
import FormIndex from './components/FormIndex';
import CDel from '@/components/common/CDel';
import CDelAll from '@/components/common/CDelAll';
import CImageInfoArr from '@/components/common/CImageInfoArr';
const Index: React.FC = () => {
  const actionRef = useRef<ActionType>();
  const [typeList, setTypeList] = useState<any>([]);
  const [departmentList, setDepartmentList] = useState<any>([]);
  const [personnelList, setPersonnelList] = useState<any>([]);
  const fetchApi = async () => {
    const typeRes = await getPidList({});
    if (typeRes.status == 20000) {
      await setTypeList(typeRes.data);
    }
    const res = await getDepartmentList();
    if (res.status == 20000) {
      const departmentData = [];
      for (let i = 0; i < res.data.length; i++) {
        departmentData[res.data[i].id] = {
          text: res.data[i].name,
          status: res.data[i].id,
        };
      }
      if (departmentData.length) await setDepartmentList({ ...departmentData });
    }
    const resPersonnel = await getPersonnelList();
    if (resPersonnel.status == 20000) {
      const personnelData = [];
      for (let i = 0; i < resPersonnel.data.length; i++) {
        personnelData[resPersonnel.data[i].id] = {
          text: resPersonnel.data[i].name,
          status: resPersonnel.data[i].id,
        };
      }
      if (personnelData.length) await setPersonnelList({ ...personnelData });
    }
  };
  useEffect(() => {
    fetchApi();
  }, []);
  let maps = {};
  const columns: ProColumns<API.AdminArchiveList>[] = [
    {
      title: '编号',
      dataIndex: 'id',
      hideInSearch: true,
      align: 'center',
      width: 80,
      fixed: true,
    },
    {
      title: '文件',
      dataIndex: 'images',
      hideInSearch: true,
      width: 100,
      align: 'center',
      render: (text, record) => [<CImageInfoArr list={record.images} key={record.id} />],
    },
    {
      title: '模板描述',
      dataIndex: 'description',
      hideInSearch: true,
      width: 200,
      align: 'center',
      copyable: true,
      ellipsis: true,
    },
    {
      title: '模板',
      dataIndex: 'archive_to',
      hideInSearch: true,
      width: 100,
      align: 'center',
      render: (text, record) => [<span key={record.id}>{record.archive_to?.name}</span>],
    },
    {
      title: '模板',
      dataIndex: 'article_type_id',
      valueType: 'cascader',
      fieldProps: { options: typeList, fieldNames: { value: 'id', label: 'name' } },
      hideInTable: true,
    },
    {
      title: '部门',
      dataIndex: 'department_to',
      hideInSearch: true,
      width: 120,
      align: 'center',
      render: (text, record) => [
        <span key={record.id}>{record.personnel_to?.department_to?.name}</span>,
      ],
    },
    {
      title: '部门',
      dataIndex: 'department_id',
      valueType: 'select',
      fieldProps: {
        showSearch: true,
      },
      valueEnum: departmentList,
      hideInTable: true,
    },
    {
      title: '人员',
      dataIndex: 'personnel_to',
      hideInSearch: true,
      width: 120,
      align: 'center',
      render: (text, record) => [<span key={record.id}>{record.personnel_to?.name}</span>],
    },
    {
      title: '人员',
      dataIndex: 'personnel_id',
      valueType: 'select',
      fieldProps: {
        showSearch: true,
      },
      valueEnum: personnelList,
      hideInTable: true,
    },
    {
      title: '状态',
      dataIndex: 'status',
      valueType: 'select',
      valueEnum: {
        0: {
          text: '禁用',
          status: '0',
        },
        1: {
          text: '启用',
          status: '1',
        },
      },
      hideInTable: true,
    },
    {
      title: '状态',
      dataIndex: 'status',
      hideInSearch: true,
      width: 100,
      align: 'center',
      render: (text, record) => [
        <Switch
          key={record.id + record.status}
          checkedChildren="启用"
          unCheckedChildren="禁用"
          defaultChecked={record.status === 1 ? true : false}
          onChange={async (checked: boolean) => {
            const res = await setStatus(record.id, { status: checked ? 1 : 0 });
            if (res.status === 20000) {
              message.success(res.message);
            } else {
              actionRef.current?.reload();
            }
          }}
        />,
      ],
    },

    {
      title: '创建时间',
      dataIndex: 'created_at',
      key: 'created_at',
      valueType: 'dateTime',
      sorter: true,
      hideInSearch: true,
      width: 180,
      align: 'center',
    },
    {
      title: '创建时间',
      key: 'created_at',
      dataIndex: 'created_at',
      valueType: 'dateTimeRange',
      hideInTable: true,
      search: {
        transform: (value) => {
          return {
            'created_at[0]': value[0],
            'created_at[1]': value[1],
          };
        },
      },
    },
    {
      title: '更新时间',
      key: 'updated_at',
      dataIndex: 'updated_at',
      valueType: 'dateTime',
      sorter: true,
      hideInSearch: true,
      width: 180,
      align: 'center',
    },
    {
      title: '更新时间',
      key: 'updated_at',
      dataIndex: 'updated_at',
      valueType: 'dateTimeRange',
      hideInTable: true,
      search: {
        transform: (value) => {
          return {
            updated_at: value,
          };
        },
      },
    },
    {
      title: '操作',
      key: 'option',
      width: 150,
      valueType: 'option',
      align: 'center',
      fixed: 'right',
      render: (_, row) => [
        <FormIndex
          id={row.id}
          key={row.id}
          onConfirm={() => {
            return actionRef.current?.reload();
          }}
        />,
        <CDel
          key={row.id}
          onCancel={async () => {
            const res = await del(row.id);
            if (res.status === 20000) {
              message.success(res.message);
              actionRef.current?.reload();
            }
          }}
        />,
      ],
    },
  ];
  return (
    <PageContainer fixedHeader={true} title={false}>
      <ProTable
        scroll={{ x: 1300 }}
        actionRef={actionRef}
        rowKey="id"
        columns={columns}
        request={async (params: any, sort) => {
          if (params.article_type_id) {
            params.article_type_id = params.article_type_id[params.article_type_id.length - 1];
          }
          const data: any = await getSort(params, sort);
          const res = await getList(data);
          return setLsetData(res);
        }}
        options={{ fullScreen: true }}
        rowSelection={{ fixed: true }}
        tableAlertRender={({ selectedRowKeys, onCleanSelected }) => (
          <Space size={24}>
            <span>
              已选 {selectedRowKeys.length} 项
              <a style={{ marginLeft: 8 }} onClick={onCleanSelected}>
                取消选择
              </a>
            </span>
          </Space>
        )}
        columnsState={{
          onChange: (map) => {
            maps = map;
          },
        }}
        tableAlertOptionRender={({ selectedRows, selectedRowKeys, onCleanSelected }) => {
          return (
            <Space size={16}>
              <CDelAll
                key={selectedRowKeys.length}
                count={selectedRowKeys.length}
                onCancel={async () => {
                  const res = await delAll({ idArr: selectedRowKeys });
                  if (res.status === 20000) {
                    message.success(res.message);
                    actionRef.current?.reload();
                    onCleanSelected();
                  }
                }}
              />
              <a
                onClick={() => {
                  const selectedRowsCope = JSON.parse(JSON.stringify(selectedRows));
                  selectedRowsCope.map((item: any) => {
                    item.archive_to = item.archive_to?.name || '';
                    item.department_to = item.personnel_to?.department_to?.name || '';
                    item.personnel_to = item.personnel_to?.name || '';
                    if (item.images.length) {
                      const images = [];
                      for (let i = 0; i < item.images.length; i++) {
                        images.push(item.images[i].http_url);
                      }
                      item.images = images.join(',');
                    } else {
                      item.images = '';
                    }
                    return item;
                  });
                  exportExcel({
                    fileName: '档案管理',
                    columns,
                    maps,
                    selectedRows: selectedRowsCope,
                  });
                }}
              >
                导出数据
              </a>
            </Space>
          );
        }}
        headerTitle={
          <FormIndex
            onConfirm={() => {
              return actionRef.current?.reload();
            }}
          />
        }
      />
    </PageContainer>
  );
};
export default Index;
