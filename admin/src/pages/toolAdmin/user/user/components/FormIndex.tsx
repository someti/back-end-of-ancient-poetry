import React, { useRef, useState } from 'react';
import { Button, message, Form, Input, Radio, Spin, InputNumber } from 'antd';
import { DrawerForm } from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { PlusOutlined, FormOutlined } from '@ant-design/icons';
import { edit, update } from '@/services/toolAdmin/user/user/api';
interface PorpsType {
  onConfirm: () => void;
  id: number;
}
const FormIndex: React.FC<PorpsType> = (porps) => {
  const restFormRef = useRef<ProFormInstance>();
  const [loading, setLoading] = useState(porps.id ? false : true);
  const formData: API.ToolAdminUserForm = {
    id: null,
    name: '',
    mobile: '',
    type: 1,
    status: 0,
    pid: 0,
    integral: 0,
  };
  return (
    <DrawerForm<API.ToolAdminUserForm>
      layout="horizontal"
      labelCol={{ span: 3 }}
      wrapperCol={{ span: 20 }}
      formRef={restFormRef}
      initialValues={formData}
      title={porps.id ? '编辑' : '添加'}
      autoFocusFirstInput
      isKeyPressSubmit
      trigger={
        porps.id ? (
          <Button type="primary" size="small">
            <FormOutlined />
            编辑
          </Button>
        ) : (
          <Button type="primary">
            <PlusOutlined />
            添加
          </Button>
        )
      }
      drawerProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        render: (props, defaultDoms) => {
          return [
            ...defaultDoms,
            <Button
              loading={loading ? false : true}
              key="extra-reset"
              onClick={async () => {
                const id = props.form?.getFieldValue('id');
                if (id) {
                  await setLoading(false);
                  const res = await edit(id);
                  if (res.status === 20000) {
                    restFormRef.current?.setFieldsValue(res.data);
                    await setLoading(true);
                  }
                } else {
                  props.reset();
                }
              }}
            >
              重置
            </Button>,
          ];
        },
      }}
      onFinish={async (values: any) => {
        const res = await update(porps.id, values);
        if (res.status === 20000) {
          message.success(res.message);
          porps.onConfirm();
          return true;
        }
        return false;
      }}
      onVisibleChange={async (visible) => {
        if (visible && porps.id) {
          await setLoading(false);
          const res = await edit(porps.id);
          if (res.status === 20000) {
            await restFormRef.current?.setFieldsValue(res.data);
            await setLoading(true);
          }
        }
      }}
    >
      <Spin spinning={loading ? false : true}>
        {loading && (
          <>
            <Form.Item label="姓名" name="name">
              <Input allowClear placeholder="请输入姓名" />
            </Form.Item>
            <Form.Item label="电话" name="mobile">
              <Input allowClear placeholder="请输入电话" />
            </Form.Item>
            <Form.Item name="type" label="用户类型">
              <Radio.Group name="type">
                <Radio value={1}>普通用户</Radio>
                <Radio value={2}>业务员</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item name="status" label="顶级业务员">
              <Radio.Group name="status">
                <Radio value={0}>否</Radio>
                <Radio value={1}>是</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item label="上级用户ID" name="pid">
              <InputNumber
                maxLength={11}
                placeholder="请输入上级用户ID"
                min={0}
                style={{ width: '100%' }}
              />
            </Form.Item>
            <Form.Item label="积分" name="integral">
              <InputNumber
                maxLength={11}
                placeholder="请输入积分"
                min={0}
                style={{ width: '100%' }}
              />
            </Form.Item>
          </>
        )}
      </Spin>
    </DrawerForm>
  );
};
export default FormIndex;
