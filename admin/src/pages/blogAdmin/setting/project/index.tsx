import React, { useEffect, useRef, useState } from 'react';
import ProForm from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { Button, message, Form, Input, Radio, Spin, Card, Divider } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import CUpload from '@/components/common/CUpload/index';
import { getInfo, setInfo } from '@/services/blogAdmin/setting/project/api';
import CBraftEditor from '@/components/common/CBraftEditor';
const Index: React.FC = () => {
  const formData: API.BlogAdminProjectForm = {
    name: '',
    logo_id: null,
    ico_id: null,
    url: '',
    description: '',
    keywords: '',
    status: 0,
    gzh_image_id: null,
    wx_image_id: null,
    about: '',
    statement: '',
    network_name: '',
    occupation: '',
    current_residence: '',
    email: '',
    qq: '',
    wx: '',
    reprint_statement: '',
    station_establishment_time: '',
    website_program: '',
    website_program_url: '',
    baidu_propelling_movement_token: '',
    baidu_propelling_movement_site: '',
    baidu_statistics_url: '',
  };

  const { TextArea } = Input;
  const restFormRef = useRef<ProFormInstance<API.BlogAdminProjectForm>>();
  const [loading, setLoading] = useState(true);
  const [submitLoading, setSubmitLoading] = useState(true);
  const fetchApi = async () => {
    await setLoading(false);
    const res = await getInfo();
    if (res.status == 20000) {
      await restFormRef.current?.setFieldsValue(res.data);
      await setLoading(true);
    }
  };
  useEffect(() => {
    fetchApi();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <PageContainer
      fixedHeader={true}
      title={false}
      footer={[
        <Button
          key={1}
          loading={loading ? false : true}
          onClick={async () => {
            await fetchApi();
          }}
        >
          重置
        </Button>,
        <Button
          key={2}
          type="primary"
          loading={submitLoading ? false : true}
          onClick={() => {
            restFormRef.current?.submit();
          }}
        >
          提交
        </Button>,
      ]}
    >
      <Card>
        <ProForm<API.BlogAdminProjectForm>
          layout="horizontal"
          labelCol={{ span: 2 }}
          wrapperCol={{ span: 22 }}
          autoFocusFirstInput
          isKeyPressSubmit
          formRef={restFormRef}
          initialValues={formData}
          submitter={false}
          onFinish={async (values: any) => {
            await setSubmitLoading(false);
            if (!Number.isInteger(values.ico_id)) {
              values.ico_id = values.ico_id && values.ico_id[0].id;
            }
            if (!Number.isInteger(values.logo_id)) {
              values.logo_id = values.logo_id && values.logo_id[0].id;
            }
            if (!Number.isInteger(values.gzh_image_id)) {
              values.gzh_image_id = values.gzh_image_id && values.gzh_image_id[0].id;
            }
            if (!Number.isInteger(values.wx_image_id)) {
              values.wx_image_id = values.wx_image_id && values.wx_image_id[0].id;
            }
            const res = await setInfo(values);
            if (res.status === 20000) {
              message.success(res.message);
            }
            await setSubmitLoading(true);
          }}
        >
          <Spin spinning={loading ? false : true}>
            {loading && (
              <>
                <Form.Item
                  label="项目名称"
                  name="name"
                  rules={[{ required: true, message: '请输入项目名称' }]}
                >
                  <Input maxLength={100} allowClear placeholder="请输入项目名称" />
                </Form.Item>
                <Form.Item
                  name="logo_id"
                  label="logo图"
                  rules={[{ required: true, message: '请选择logo图' }]}
                >
                  <CUpload
                    key="logo_id"
                    imageList={
                      restFormRef.current?.getFieldValue('logo_image')
                        ? [restFormRef.current.getFieldValue('logo_image')]
                        : []
                    }
                  />
                </Form.Item>
                <Form.Item
                  name="ico_id"
                  label="站点标识"
                  rules={[{ required: true, message: '请选择站点标识' }]}
                >
                  <CUpload
                    key="ico_id"
                    imageList={
                      restFormRef.current?.getFieldValue('ico_image')
                        ? [restFormRef.current.getFieldValue('ico_image')]
                        : []
                    }
                  />
                </Form.Item>
                <Form.Item
                  label="项目网址"
                  name="url"
                  rules={[{ required: true, message: '请输入项目网址' }]}
                >
                  <Input maxLength={100} allowClear placeholder="请输入项目网址" />
                </Form.Item>
                <Form.Item
                  label="Title"
                  name="title"
                  rules={[{ required: true, message: '请输入title' }]}
                >
                  <Input maxLength={100} allowClear placeholder="请输入title" />
                </Form.Item>
                <Form.Item
                  label="项目描述"
                  name="description"
                  rules={[{ required: true, message: '请输入项目描述' }]}
                >
                  <TextArea showCount maxLength={100} placeholder="请输入项目描述" />
                </Form.Item>
                <Form.Item
                  label="项目关键词"
                  name="keywords"
                  rules={[{ required: true, message: '请输入项目关键词' }]}
                >
                  <TextArea showCount maxLength={100} placeholder="请输入项目关键词" />
                </Form.Item>
                <Form.Item
                  label="网站备案号"
                  name="icp"
                  rules={[{ required: true, message: '请输入网站备案号' }]}
                >
                  <TextArea showCount maxLength={100} placeholder="请输入网站备案号" />
                </Form.Item>

                <Form.Item label="网名" name="network_name">
                  <Input maxLength={100} allowClear placeholder="请输入网名" />
                </Form.Item>

                <Form.Item label="职业" name="occupation">
                  <Input maxLength={100} allowClear placeholder="请输入职业" />
                </Form.Item>

                <Form.Item label="现居" name="current_residence">
                  <Input maxLength={100} allowClear placeholder="请输入现居" />
                </Form.Item>

                <Form.Item label="Email" name="email">
                  <Input maxLength={100} allowClear placeholder="请输入Email" />
                </Form.Item>

                <Form.Item label="QQ" name="qq">
                  <Input maxLength={100} allowClear placeholder="请输入QQ" />
                </Form.Item>

                <Form.Item label="微信" name="wx">
                  <Input maxLength={100} allowClear placeholder="请输入微信" />
                </Form.Item>

                <Form.Item label="转载声明" name="reprint_statement">
                  <TextArea showCount placeholder="请输入转载声明" />
                </Form.Item>

                <Form.Item label="建站时间" name="station_establishment_time">
                  <Input maxLength={100} allowClear placeholder="请输入建站时间 " />
                </Form.Item>

                <Form.Item label="网站程序" name="website_program">
                  <Input maxLength={100} allowClear placeholder="请输入网站程序 " />
                </Form.Item>

                <Form.Item label="网站程序地址" name="website_program_url">
                  <Input maxLength={100} allowClear placeholder="请输入网站程序地址" />
                </Form.Item>
                <Divider orientation="left">
                  百度配置
                  <a
                    href="https://ziyuan.baidu.com/site/index#/"
                    target="_blank"
                    style={{ paddingLeft: '20px' }}
                  >
                    百度探索资源平台
                  </a>
                  <a
                    href="https://tongji.baidu.com/main/setting/47104197/home/site/index"
                    target="_blank"
                    style={{ paddingLeft: '20px' }}
                  >
                    百度统计
                  </a>
                  <a
                    href="https://union.baidu.com/bqt/modules.html#/"
                    target="_blank"
                    style={{ paddingLeft: '20px' }}
                  >
                    百度联盟
                  </a>
                </Divider>
                <Form.Item label="普通收录token" name="baidu_propelling_movement_token">
                  <Input allowClear placeholder="请输入普通收录token" />
                </Form.Item>
                <Form.Item label="普通收录site" name="baidu_propelling_movement_site">
                  <Input allowClear placeholder="请输入普通收录site" />
                </Form.Item>

                <Form.Item label="百度统计URL" name="baidu_statistics_url">
                  <Input allowClear placeholder="请输入百度统计URL" />
                </Form.Item>
                <Form.Item label="页面头部扩展" name="header">
                  <TextArea showCount placeholder="请输入页面头部扩展" />
                </Form.Item>
                <Form.Item label="页面底部扩展" name="footer">
                  <TextArea showCount placeholder="请输入页面底部扩展" />
                </Form.Item>
                <Form.Item label="普通收录" name="general_embody">
                  <TextArea showCount placeholder="请输入普通收录提交地址" />
                </Form.Item>
                <Form.Item label="快速收录" name="quick_embody">
                  <TextArea showCount placeholder="请输入快速收录提交地址" />
                </Form.Item>
                <Form.Item label="robots设置" name="robots">
                  <TextArea showCount placeholder="请输入robots.txt设置" />
                </Form.Item>
                <Form.Item
                  name="status"
                  label="状态"
                  rules={[{ required: true, message: '请选择状态' }]}
                >
                  <Radio.Group name="status">
                    <Radio value={0}>禁用</Radio>
                    <Radio value={1}>启用</Radio>
                  </Radio.Group>
                </Form.Item>
                <Form.Item
                  name="gzh_image_id"
                  label="公众二维码"
                  rules={[{ required: true, message: '请选择公众二维码' }]}
                >
                  <CUpload
                    key="gzh_image_id"
                    imageList={
                      restFormRef.current?.getFieldValue('gzh_image_to')
                        ? [restFormRef.current.getFieldValue('gzh_image_to')]
                        : []
                    }
                  />
                </Form.Item>
                <Form.Item
                  name="wx_image_id"
                  label="微信二维码"
                  rules={[{ required: true, message: '请选择微信二维码' }]}
                >
                  <CUpload
                    key="wx_image_id"
                    imageList={
                      restFormRef.current?.getFieldValue('wx_image_to')
                        ? [restFormRef.current.getFieldValue('wx_image_to')]
                        : []
                    }
                  />
                </Form.Item>
                <Form.Item
                  label="站点声明"
                  name="statement"
                  rules={[{ required: true, message: '请输入站点声明' }]}
                >
                  <TextArea showCount maxLength={100} placeholder="请输入站点声明" />
                </Form.Item>
                <Form.Item
                  label="关于我们"
                  name="about"
                  rules={[{ required: true, message: '请输入关于我们' }]}
                >
                  <CBraftEditor
                    value={restFormRef.current?.getFieldValue('about')}
                    onChange={async (value: string) => {
                      await restFormRef.current?.setFieldsValue({ about: value });
                    }}
                  />
                </Form.Item>
              </>
            )}
          </Spin>
        </ProForm>
      </Card>
    </PageContainer>
  );
};
export default Index;
