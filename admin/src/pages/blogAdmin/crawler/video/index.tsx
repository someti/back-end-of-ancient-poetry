import React, { useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Input, message, Divider } from 'antd';
import { getVideoUrl } from '@/services/blogAdmin/crawler/api';
import ReactJson from 'react-json-view';
import {
  Player,
  ControlBar,
  PlayToggle, // PlayToggle 播放/暂停按钮 若需禁止加 disabled
  //   ReplayControl, // 后退按钮
  //   ForwardControl, // 前进按钮
  CurrentTimeDisplay,
  TimeDivider,
  PlaybackRateMenuButton, // 倍速播放选项
  VolumeMenuButton,
  BigPlayButton,
} from 'video-react';
import 'video-react/dist/video-react.css';

const Index: React.FC = () => {
  const [url, setUrl] = useState<string>();
  const [player, setPlayer] = useState<any>();
  const [cover, setCover] = useState<string>();
  const [dataInfo, setDataInfo] = useState<any>({});
  return (
    <PageContainer
      fixedHeader={true}
      title={false}
      content={
        <div style={{ textAlign: 'center' }}>
          <Input.Search
            placeholder="请输入url"
            enterButton="搜索"
            size="large"
            onSearch={async (value: string) => {
              if (value == '') {
                // http://pgc.qcdn.xiaodutv.com/1716475222_2111441623_20220213180519.mp4?Cache-Control%3Dmax-age-8640000%26responseExpires%3DTue%2C_24_May_2022_18%3A05%3A23_GMT=&xcode=8247743cd1134f443aeb1eab8d6552c93fe325014010e1a3&time=1645077692&_=1644992974412
                message.error('请输入url');
                return;
              }
              const res = await getVideoUrl({ url: value });
              if (res.status === 20000) {
                await setUrl(res.data.url);
                await setCover(res.data.cover);
                await setDataInfo(JSON.parse(JSON.stringify(res.data)));
                // console.log(player);
                player?.load();
              }
            }}
            style={{ maxWidth: 522, width: '100%' }}
          />
        </div>
      }
    >
      <div style={{ maxWidth: 800, width: '100%', margin: '0 auto' }}>
        <Player
          ref={(c: any) => {
            setPlayer(c);
          }}
          autoPlay
          playsInline
          controls
          name="referrer"
          //   autoPlay="true"
          //   playsInline="true"
          //   controls="true"
          // src={url}
          poster={cover}
        >
          <BigPlayButton position="center" />
          <source src={url} type="video/mp4" />
          <ControlBar autoHide={false} disableDefaultControls={false}>
            {/* <ReplayControl seconds={10} order={1.1} />
          <ForwardControl seconds={30} order={1.2} /> */}
            <PlayToggle />
            <CurrentTimeDisplay order={4.1} />
            <TimeDivider order={4.2} />
            <PlaybackRateMenuButton rates={[5, 2, 1.5, 1, 0.5]} order={7.1} />
            <VolumeMenuButton />
          </ControlBar>
        </Player>
        <Divider orientation="left">返回数据</Divider>
        <ReactJson
          collapsed={false} //是否收起,true为收起
          indentWidth={10} //缩进
          iconStyle="circle"
          src={dataInfo}
          theme="bright"
          collapseStringsAfterLength={10} //字符串多长时用省略号
          enableClipboard={true} //点击向左箭头进行复制
          displayObjectSize={false} //显示有多少个items属性
          displayDataTypes={false} //显示值的类型
          onEdit={(edit) => {
            console.log(edit);
          }} //编辑完成前调用回调函数
          onAdd={(add) => {
            console.log(add);
          }} //添加属性
          defaultValue="malinshu" //添加属性后的默认值
          onDelete={(onDelete) => {
            console.log(onDelete);
          }} //删除属性
          onSelect={(onSelect) => {
            console.log(onSelect);
          }} //单击键值对的值时触发
          sortKeys={true} //键的排序
          quotesOnKeys={false} //是否显示键的引号
          groupArraysAfterLength={5} //数组为多少个的时候被拆分显示
        />
        {/* <ReactJson src={dataInfo} style={{ width: '500px' }} /> */}
      </div>
    </PageContainer>
  );
};
export default Index;
