declare namespace API {
  type BlogAdminArticleTypeList = {
    id: number;
    type: 1 | 2 | 3 | 4;
    name: string;
    image_id: number | null;
    description: string;
    image_to: any;
    status: 1 | 0;
    level: number;
    pid: number;
    sort: number;
    created_at: string;
    updated_at: string | null;
    open_type_value: string;
    attribute_value: string;
  };
  type BlogAdminArticleTypeForm = {
    id: number | null;
    type: 1 | 2 | 3 | 4;
    name: string;
    image_id: number | null;
    description: string;
    status: 1 | 0;
    level: number;
    pid: number;
    sort: number;
    open_type: number;
    attribute: number;
    external_link: string;
    seo_title: string;
    seo_keyword: string;
    seo_description: string;
  };
}
