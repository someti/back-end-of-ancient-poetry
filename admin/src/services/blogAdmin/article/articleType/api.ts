import { request } from '@/utils/Request';

/** 文章分类列表 GET */
export async function getList(data: any) {
  return await request('/blogAdmin/articleType/index', {
    params: data,
  });
}

// 排序
export async function setSorts(id: number, data: API.SortQequest) {
  return await request('/blogAdmin/articleType/sorts/' + id, {
    method: 'put',
    data: data,
  });
}

// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/blogAdmin/articleType/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 添加
export async function add(data: API.BlogAdminArticleTypeForm) {
  return await request('/blogAdmin/articleType/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/blogAdmin/articleType/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.BlogAdminArticleTypeForm) {
  return await request('/blogAdmin/articleType/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/blogAdmin/articleType/del/' + id, {
    method: 'delete',
  });
}
// 获取父级列表
export async function getPidList(data: { id?: number }) {
  return await request('/blogAdmin/articleType/getPidList', {
    params: data,
  });
}
