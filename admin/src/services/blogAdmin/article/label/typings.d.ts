declare namespace API {
  type BlogAdminLabelList = {
    id: number;
    name: string;
    status: 1 | 0;
    sort: number;
    created_at: string;
    updated_at: string | null;
  };
  type BlogAdminLabelForm = {
    id: number | null;
    name: string;
    status: 1 | 0;
    sort: number;
  };
}
