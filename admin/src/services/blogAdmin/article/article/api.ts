import { request } from '@/utils/Request';

/** 文章列表 GET */
export async function getList(data: API.ListQequest) {
  return await request('/blogAdmin/article/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/blogAdmin/article/status/' + id, {
    method: 'put',
    data: data,
  });
}

// 是否推荐
export async function setOpen(id: number, data: any) {
  return await request('/blogAdmin/article/open/' + id, {
    method: 'put',
    data: data,
  });
}

// 排序
export async function setSorts(id: number, data: API.SortQequest) {
  return await request('/blogAdmin/article/sorts/' + id, {
    method: 'put',
    data: data,
  });
}

// 添加
export async function add(data: API.BlogAdminArticleForm) {
  return await request('/blogAdmin/article/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/blogAdmin/article/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.BlogAdminArticleForm) {
  return await request('/blogAdmin/article/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/blogAdmin/article/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/blogAdmin/article/delAll/', {
    method: 'delete',
    data: data,
  });
}
// 获取文章分类列表
export async function getTypeList() {
  return await request('/blogAdmin/article/getTypeList');
}
// 获取文章标签列表
export async function getLabelList(data: { name: string }) {
  return await request('/blogAdmin/article/getLabelList', {
    params: data,
  });
}

// 添加
export async function propellingMovementAll(data: { idArr: any[] }) {
  return await request('/blogAdmin/article/propellingMovementAll', {
    method: 'post',
    data: data,
  });
}
