declare namespace API {
  type AdminOperationLogList = {
    id: number;
    name: string;
    url: string;
    pathname: string;
    method: number;
    ip: number;
    admin_one:
      | {
          id: number;
          username: string;
        }
      | any;
    created_at: string;
    data: string;
    header: string;
  };
}
