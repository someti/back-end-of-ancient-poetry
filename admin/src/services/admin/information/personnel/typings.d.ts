declare namespace API {
  type AdminPersonnelList = {
    id: number;
    name: string;
    status: 1 | 0;
    sort: number;
    department_id: number | null;
    sex: 1 | 0;
    nation: string;
    date_of_birth: string;
    education: number | null;
    marital_status: 1 | 0;
    position: string;
    political_outlook: string;
    personnel_type: string;
    entry_time: string;
    created_at: string;
    updated_at: string | null;
    department_to: any;
  };
  type AdminPersonnelForm = {
    id: number | null;
    name: string;
    department_id: number | null;
    sex: 1 | 0;
    nation: string;
    date_of_birth: string;
    education: number | null;
    marital_status: 1 | 0;
    position: string;
    political_outlook: string;
    personnel_type: string;
    entry_time: string;
    status: 1 | 0;
  };
}
