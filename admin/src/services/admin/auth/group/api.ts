import { request } from '@/utils/Request';

/** 角色列表 GET */
export async function getList(data: API.ListQequest) {
  return await request('/admin/group/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/admin/group/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 添加
export async function add(data: API.AdminProjectForm) {
  return await request('/admin/group/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/admin/group/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.AdminProjectForm) {
  return await request('/admin/group/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/admin/group/del/' + id, {
    method: 'delete',
  });
}
// 权限规则页面
export async function access(id: number) {
  return await request('/admin/group/access/' + id);
}
// 权限规则提交
export async function accessUpdate(id: number, data: { rules: number[] }) {
  return await request('/admin/group/accessUpdate/' + id, {
    method: 'put',
    data: data,
  });
}
