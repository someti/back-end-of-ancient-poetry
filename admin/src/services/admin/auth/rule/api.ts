import { request } from '@/utils/Request';

/** 菜单列表 GET */
export async function getList(data: any) {
  return await request('/admin/rule/index', {
    params: data,
  });
}

// 排序
export async function setSorts(id: number, data: API.SortQequest) {
  return await request('/admin/rule/sorts/' + id, {
    method: 'put',
    data: data,
  });
}

// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/admin/rule/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 是否验证权限
export async function setAuthOpen(id: number, data: API.AdminRuleAuthOpen) {
  return await request('/admin/rule/authOpen/' + id, {
    method: 'put',
    data: data,
  });
}
// 是否固定面板
export async function setAffix(id: number, data: API.AdminRuleAffix) {
  return await request('/admin/rule/affix/' + id, {
    method: 'put',
    data: data,
  });
}
// 添加
export async function add(data: API.AdminProjectForm) {
  return await request('/admin/rule/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/admin/rule/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.AdminProjectForm) {
  return await request('/admin/rule/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/admin/rule/del/' + id, {
    method: 'delete',
  });
}
// 编辑提交
export async function getPidList(data: { id?: number }) {
  return await request('/admin/rule/getPidList', {
    params: data,
  });
}
