declare namespace API {
  type AdminRuleList = {
    id: number;
    path: string;
    url: string;
    redirect: string;
    icon: string;
    name: string;
    type: 1 | 2 | 3;
    status: 1 | 0;
    auth_open: 1 | 0;
    level: number;
    affix: 1 | 0;
    pid: number;
    sort: number;
    created_at: string;
    updated_at: string | null;
  };
  type AdminRuleForm = {
    id: number | null;
    path: string;
    url: string;
    redirect: string;
    icon: string;
    name: string;
    type: 1 | 2 | 3;
    status: 1 | 0;
    auth_open: 1 | 0;
    level: number;
    affix: 1 | 0;
    pid: number;
    sort: number;
  };
  type AdminRuleAuthOpen = {
    auth_open: 0 | 1;
  };
  type AdminRuleAffix = {
    affix: 0 | 1;
  };
}
