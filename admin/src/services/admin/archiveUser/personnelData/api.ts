import { request } from '@/utils/Request';
export async function getList(data: API.ListQequest) {
  return await request('/admin/personnelData/index', {
    params: data,
  });
}
export async function getArchiveList(data: any) {
  return await request('/admin/personnelData/getArchiveList', {
    params: data,
  });
}
