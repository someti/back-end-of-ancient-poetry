declare namespace API {
  type AdminPersonnelDataList = {
    id: number;
    name: string;
    sort: number;
    department_id: number | null;
    sex: 1 | 0;
    nation: string;
    date_of_birth: string;
    education: number | null;
    marital_status: 1 | 0;
    position: string;
    political_outlook: string;
    personnel_type: string;
    entry_time: string;
    created_at: string;
    updated_at: string | null;
    department_to: any;
  };
}
