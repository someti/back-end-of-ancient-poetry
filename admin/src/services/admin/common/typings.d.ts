declare namespace API {
  type LoginType = {
    username?: string;
    password?: string;
  };
  type CurrentUser = {
    username: string;
  };
  type ResponseData = {
    message: string;
    status: number;
    data?: any;
  };
  type ListQequest = {
    page?: number;
    limit?: number;
    status?: number;
    created_at?: [];
    updated_at?: [];
    name?: string;
    title?: string;
    sort?: string;
  };
  type StatusQequest = {
    status: 0 | 1;
  };
  type SortQequest = {
    sort: number;
  };
  type GroupItem = {
    id: number;
    name: string;
  };
  type ProjectItem = {
    id: number;
    name: string;
    type: number;
  };
  type UpdatePwd = {
    y_password: string;
    password: string;
    password_confirmation: string;
  };
  type CurrentConfig = {
    name: string;
    keywords: string;
    description: string;
    logo_image: string;
    logViewerUrl: string;
  };
}
