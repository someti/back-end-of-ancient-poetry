declare namespace API {
  type ToolAdminUserList = {
    id: number;
    nick_name: string;
    avatar_url: string;
    code_url: string;
    login_code_url: string;
    name: string;
    mobile: string;
    type: number;
    status: 1 | 0;
    created_at: string;
    open: number;
    updated_at: string | null;
    gzh_open_id: any;
    integral: number;
  };
  type ToolAdminUserForm = {
    id: number | null;
    name: string;
    mobile: string;
    type: number;
    status: 1 | 0;
    pid: number;
    integral: number;
  };
}
