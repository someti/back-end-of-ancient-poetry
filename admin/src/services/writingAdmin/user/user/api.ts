import { request } from '@/utils/Request';
export async function getList(data: API.ListQequest) {
  return await request('/writingAdmin/user/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/writingAdmin/user/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/writingAdmin/user/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.WritingAdminUserForm) {
  return await request('/writingAdmin/user/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/writingAdmin/user/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/writingAdmin/user/delAll/', {
    method: 'delete',
    data: data,
  });
}
