import { request } from '@/utils/Request';
export async function getList(data: API.ListQequest) {
  return await request('/writingAdmin/anthology/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/writingAdmin/anthology/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 排序
export async function setSorts(id: number, data: API.SortQequest) {
  return await request('/writingAdmin/anthology/sorts/' + id, {
    method: 'put',
    data: data,
  });
}

// 添加
export async function add(data: API.ReadAdminBookForm) {
  return await request('/writingAdmin/anthology/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/writingAdmin/anthology/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.WritingAdminAnthologyForm) {
  return await request('/writingAdmin/anthology/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/writingAdmin/anthology/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/writingAdmin/anthology/delAll/', {
    method: 'delete',
    data: data,
  });
}
