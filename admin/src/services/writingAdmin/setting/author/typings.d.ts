declare namespace API {
  type WritingAdminAuthorList = {
    id: number;
    name: string;
    status: 1 | 0;
    sort: number;
    created_at: string;
    image_to: any;
    updated_at: string | null;
    baidu_wiki: string;
    dynasty_id: number | null;
    dynasty: string;
  };
  type WritingAdminAuthorForm = {
    id: number | null;
    name: string;
    name_tr: string;
    death_year: string;
    birth_year: string;
    desc: string;
    desc_tr: string;
    dynasty_id: number | null;
    dynasty: string;
    dynasty_tr: string;
    baidu_wiki: string;
    works_count: number | null;
    status: 1 | 0;
    sort: number;
  };
}
