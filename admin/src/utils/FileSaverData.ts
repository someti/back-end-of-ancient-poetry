import FileSaver from 'file-saver';

const FileSaverData = async (data: any, name: string) => {
  const blob = new Blob([data], { type: '' });
  FileSaver.saveAs(blob, `${name}.json`);
};
export { FileSaverData };
