import SaveRemotePictures from './SaveRemotePictures';
import GetHtmlView from './GetHtmlView';
import type { IModuleConf } from '@wangeditor/editor';
import { Boot } from '@wangeditor/editor';
// import ctrlEnterModule from '@wangeditor/plugin-ctrl-enter';
import formulaModule from '@wangeditor/plugin-formula';
import markdownModule from '@wangeditor/plugin-md';
import attachmentModule from '@wangeditor/plugin-upload-attachment';
import linkCardModule from '@wangeditor/plugin-link-card';
const saveRemotePicturesConf = {
  key: 'saveRemotePicturesInfo', // 定义 menu key ：要保证唯一、不重复（重要）
  factory() {
    return new SaveRemotePictures();
  },
};
const getHtmlViewConf = {
  key: 'getHtmlView', // 定义 menu key ：要保证唯一、不重复（重要）
  factory() {
    return new GetHtmlView();
  },
  // config: {
  //   onClick: () => {},
  // },
};
const module: Partial<IModuleConf> = {
  menus: [saveRemotePicturesConf, getHtmlViewConf],
};
Boot.registerModule(module);
// Boot.registerModule(ctrlEnterModule);
Boot.registerModule(formulaModule);
Boot.registerModule(markdownModule);
Boot.registerModule(attachmentModule);
Boot.registerModule(linkCardModule);
