import React from 'react';
import { Button } from 'antd';
import { SketchPicker } from 'react-color';
import styles from './index.less';
interface TypeProps {
  onCancel?: any;
  color?: string;
  confirmColor?: string;
  visible?: boolean;
}
class Color extends React.Component<any, TypeProps> {
  constructor(props: TypeProps) {
    super(props);
    const confirmColor = props.color;
    this.state = {
      visible: false,
      confirmColor: confirmColor,
      ...props,
    };
  }
  static defaultProps: TypeProps = {
    color: '#FFFFFF',
  };
  handleChangeComplete = async (color: { hex: any }) => {
    this.setState({ color: color.hex });
  };
  cancelClick = async () => {
    this.setState({ visible: false, color: this.state.confirmColor });
  };
  confirmClick = async () => {
    this.setState({ confirmColor: this.state.color, visible: false });
    if (this.state.onCancel) {
      await this.state.onCancel(this.state.color);
    }
  };
  render() {
    return (
      <div className={styles.infoBox}>
        <div className={styles.colorBox}>
          <div
            className={styles.colorView}
            style={{ backgroundColor: this.state.confirmColor }}
            onClick={() => {
              this.setState({ visible: !this.state.visible });
            }}
          />

          {this.state.visible ? (
            <div className={styles.sketchPickerView}>
              <SketchPicker color={this.state.color} onChangeComplete={this.handleChangeComplete} />
              <div className={styles.operation}>
                <Button size="small" className={styles.operationBtn} onClick={this.cancelClick}>
                  取消
                </Button>
                <Button
                  type="primary"
                  size="small"
                  className={styles.operationBtn}
                  onClick={this.confirmClick}
                >
                  确定
                </Button>
              </div>
            </div>
          ) : (
            <></>
          )}
        </div>
        <div className={styles.colorValue}>{this.state.confirmColor}</div>
      </div>
    );
  }
}
export default Color;
