import React from 'react';
import styles from './index.less';
import { Modal } from 'antd';
interface TypeProps {
  src: string;
  isModalVisibleImageInfo?: boolean;
  onCancel?: any;
}
class CImageInfo extends React.Component<any, TypeProps> {
  constructor(props: TypeProps) {
    super(props);
    let isModalVisibleImageInfo = false;
    if (props.src !== '') {
      isModalVisibleImageInfo = true;
    } else {
      isModalVisibleImageInfo = false;
    }
    this.state = {
      isModalVisibleImageInfo: isModalVisibleImageInfo,
      ...props,
    };
  }
  onClickModalVisibleImageInfo = () => {
    this.setState({ isModalVisibleImageInfo: false });
    this.setState({ src: '' });
    this.state.onCancel();
  };
  render() {
    return (
      <>
        <Modal
          title="图片预览"
          visible={this.state.isModalVisibleImageInfo}
          onCancel={() => {
            this.onClickModalVisibleImageInfo();
          }}
          footer={[]}
        >
          <img src={this.state.src} alt="" className={styles.img} />
        </Modal>
      </>
    );
  }
}
export default CImageInfo;
