import React from 'react';
import { Form, Divider, Input, Select } from 'antd';
import Color from '@/components/common/Color';
import styles from './index.less';
interface TypeProps {
  onCancel?: any;
  options: any;
  modelIndex?: number;
  viewLoading?: boolean;
}
class CModelBox extends React.Component<any, TypeProps> {
  constructor(props: TypeProps) {
    super(props);
    this.state = {
      modelIndex: 4,
      viewLoading: true,
      ...props,
    };
  }
  async boxClick(e: any, i: number) {
    this.setState({ viewLoading: false });
    console.log(e, i);
    e.preventDefault();
    e.stopPropagation();
    this.setState({ modelIndex: i, viewLoading: true });
  }

  async setOptions(value: string, key: string) {
    const options = this.state.options;
    if (value != '') {
      options[key] = value;
    } else {
      delete options[key];
    }
    this.setState({ options: options });
    if (this.state.onCancel) {
      this.state.onCancel(options);
    }
  }
  setView() {
    if (this.state.modelIndex == 1) {
      return (
        <>
          <Divider orientation="left">margin设置</Divider>
          <Form.Item label="top">
            <Input
              placeholder="margin-top"
              value={this.state.options['margin-top'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'margin-top');
              }}
            />
          </Form.Item>
          <Form.Item label="right">
            <Input
              placeholder="margin-right"
              value={this.state.options['margin-right'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'margin-right');
              }}
            />
          </Form.Item>
          <Form.Item label="bottom">
            <Input
              placeholder="margin-bottom"
              value={this.state.options['margin-bottom'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'margin-bottom');
              }}
            />
          </Form.Item>
          <Form.Item label="left">
            <Input
              placeholder="margin-left"
              value={this.state.options['margin-left'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'margin-left');
              }}
            />
          </Form.Item>
        </>
      );
    } else if (this.state.modelIndex == 2) {
      return (
        <>
          <Divider orientation="left">border设置</Divider>
          <Form.Item label="border-color">
            <Color
              color={this.state.options['border-color'] || '#ffffff'}
              onCancel={async (color: any) => {
                this.setOptions(color, 'border-color');
              }}
            />
          </Form.Item>
          <Form.Item label="border-style">
            <Select
              value={this.state.options['border-style'] || null}
              placeholder="border-style"
              onChange={(value) => {
                this.setOptions(value, 'border-style');
              }}
            >
              <Select.Option value="none">none（定义无边框。）</Select.Option>
              <Select.Option value="hidden">
                hidden（与 "none" 相同。不过应用于表时除外，对于表，hidden 用于解决边框冲突。）
              </Select.Option>
              <Select.Option value="dotted">
                dotted（定义点状边框。在大多数浏览器中呈现为实线。）
              </Select.Option>
              <Select.Option value="dashed">
                dashed（定义虚线。在大多数浏览器中呈现为实线。）
              </Select.Option>
              <Select.Option value="solid">solid（定义实线。）</Select.Option>
              <Select.Option value="double">
                double（定义双线。双线的宽度等于 border-width 的值。）
              </Select.Option>
              <Select.Option value="groove">
                groove（定义 3D 凹槽边框。其效果取决于 border-color 的值。）
              </Select.Option>
              <Select.Option value="ridge">
                ridge（定义 3D 垄状边框。其效果取决于 border-color 的值。）
              </Select.Option>
              <Select.Option value="inset">
                inset（定义 3D inset 边框。其效果取决于 border-color 的值。）
              </Select.Option>
              <Select.Option value="outset">
                outset（定义 3D outset 边框。其效果取决于 border-color 的值。）
              </Select.Option>
              <Select.Option value="inherit">
                inherit（规定应该从父元素继承边框样式。）
              </Select.Option>
            </Select>
          </Form.Item>
          <Form.Item label="top">
            <Input
              placeholder="border-top"
              value={this.state.options['border-top'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'border-top');
              }}
            />
          </Form.Item>
          <Form.Item label="right">
            <Input
              placeholder="border-right"
              value={this.state.options['border-right'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'border-right');
              }}
            />
          </Form.Item>
          <Form.Item label="bottom">
            <Input
              placeholder="border-bottom"
              value={this.state.options['border-bottom'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'border-bottom');
              }}
            />
          </Form.Item>
          <Form.Item label="left">
            <Input
              placeholder="border-left"
              value={this.state.options['border-left'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'border-left');
              }}
            />
          </Form.Item>
        </>
      );
    } else if (this.state.modelIndex == 3) {
      return (
        <>
          <Divider orientation="left">padding设置</Divider>
          <Form.Item label="top">
            <Input
              placeholder="padding-top"
              value={this.state.options['padding-top'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'padding-top');
              }}
            />
          </Form.Item>
          <Form.Item label="right">
            <Input
              placeholder="padding-right"
              value={this.state.options['padding-right'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'padding-right');
              }}
            />
          </Form.Item>
          <Form.Item label="bottom">
            <Input
              placeholder="padding-bottom"
              value={this.state.options['padding-bottom'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'padding-bottom');
              }}
            />
          </Form.Item>
          <Form.Item label="left">
            <Input
              placeholder="padding-left"
              value={this.state.options['padding-left'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'padding-left');
              }}
            />
          </Form.Item>
        </>
      );
    } else if (this.state.modelIndex == 4) {
      return (
        <>
          <Divider orientation="left">width/height设置</Divider>
          <Form.Item label="width">
            <Input
              placeholder="width"
              value={this.state.options.width || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'width');
              }}
            />
          </Form.Item>
          <Form.Item label="min-width">
            <Input
              placeholder="min-width"
              value={this.state.options['min-width'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'min-width');
              }}
            />
          </Form.Item>
          <Form.Item label="max-width">
            <Input
              placeholder="max-width"
              value={this.state.options['max-width'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'max-width');
              }}
            />
          </Form.Item>
          <Form.Item label="height">
            <Input
              placeholder="height"
              value={this.state.options.height || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'height');
              }}
            />
          </Form.Item>
          <Form.Item label="min-height">
            <Input
              placeholder="min-height"
              value={this.state.options['min-height'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'min-height');
              }}
            />
          </Form.Item>
          <Form.Item label="max-height">
            <Input
              placeholder="max-height"
              value={this.state.options['max-height'] || ''}
              onChange={(e) => {
                this.setOptions(e.target.value, 'max-height');
              }}
            />
          </Form.Item>
        </>
      );
    } else {
      return <></>;
    }
  }
  render() {
    return (
      <div className={styles.infoBox}>
        <div
          className={styles.marginBox}
          style={{ backgroundColor: this.state.modelIndex == 1 ? '#fff' : '' }}
          onClick={async (e) => {
            await this.boxClick(e, 1);
          }}
        >
          <div className={styles.marginTop}>
            <span className={styles.marginName}>margin</span>
            <span className={styles.marginValue}>{this.state.options['margin-top'] || '-'}</span>
          </div>
          <div className={styles.marginAuto}>
            <div className={styles.marginLeft}>
              <span>{this.state.options['margin-left'] || '-'}</span>
            </div>
            <div className={styles.marginInfo}>
              <div
                className={styles.borderBox}
                style={{ backgroundColor: this.state.modelIndex == 2 ? '#fff' : '' }}
                onClick={async (e) => {
                  await this.boxClick(e, 2);
                }}
              >
                <div className={styles.borderTop}>
                  <span className={styles.borderName}>border</span>
                  <span className={styles.borderValue}>
                    {this.state.options['border-top'] || '-'}
                  </span>
                </div>
                <div className={styles.borderAuto}>
                  <div className={styles.borderLeft}>
                    <span>{this.state.options['border-left'] || '-'}</span>
                  </div>
                  <div className={styles.borderInfo}>
                    <div
                      className={styles.paddingBox}
                      style={{ backgroundColor: this.state.modelIndex == 3 ? '#fff' : '' }}
                      onClick={async (e) => {
                        await this.boxClick(e, 3);
                      }}
                    >
                      <div className={styles.paddingTop}>
                        <span className={styles.paddingName}>padding</span>
                        <span className={styles.paddingValue}>
                          {this.state.options['padding-top'] || '-'}
                        </span>
                      </div>

                      <div className={styles.paddingAuto}>
                        <div className={styles.paddingLeft}>
                          <span>{this.state.options['padding-left'] || '-'}</span>
                        </div>
                        <div className={styles.paddingInfo}>
                          <div
                            className={styles.modelBoxInfo}
                            style={{ backgroundColor: this.state.modelIndex == 4 ? '#fff' : '' }}
                            onClick={async (e) => {
                              await this.boxClick(e, 4);
                            }}
                          >
                            <span>{this.state.options.width || 'auto'}</span>
                            <span>x</span>
                            <span>{this.state.options.height || 'auto'}</span>
                          </div>
                        </div>
                        <div className={styles.paddingRight}>
                          <span>{this.state.options['padding-right'] || '-'}</span>
                        </div>
                      </div>

                      <div className={styles.paddingBottom}>
                        <span>{this.state.options['padding-bottom'] || '-'}</span>
                      </div>
                    </div>
                  </div>
                  <div className={styles.borderRight}>
                    <span>{this.state.options['border-right'] || '-'}</span>
                  </div>
                </div>
                <div className={styles.borderBottom}>
                  <span>{this.state.options['border-bottom'] || '-'}</span>
                </div>
              </div>
            </div>
            <div className={styles.marginRight}>
              <span>{this.state.options['margin-right'] || '-'}</span>
            </div>
          </div>
          <div className={styles.marginBottom}>
            <span>{this.state.options['margin-bottom'] || '-'}</span>
          </div>
        </div>

        {this.state.viewLoading ? this.setView() : <></>}
      </div>
    );
  }
}
export default CModelBox;
