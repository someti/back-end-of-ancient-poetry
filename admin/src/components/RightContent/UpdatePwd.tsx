import React, { useState, useImperativeHandle, forwardRef } from 'react';
import { Button, message, Form, Input, Modal } from 'antd';

import { setUpdatePwd } from '@/services/admin/common/api';

const UpdatePwd: React.FC = (porps: any, ref: any) => {
  const [form] = Form.useForm();
  const [visible, setVisible] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  useImperativeHandle(ref, () => ({
    showModalPwd: async () => {
      await setVisible(true);
    },
  }));
  const closeModal = async () => {
    await setVisible(false);
  };
  const formData: API.UpdatePwd = {
    y_password: '',
    password: '',
    password_confirmation: '',
  };
  return (
    <>
      <Modal
        visible={visible}
        title="修改密码"
        onCancel={closeModal}
        footer={[
          <Button key="back" onClick={closeModal}>
            取消
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={() => {
              form.submit();
            }}
          >
            提交
          </Button>,
          <Button
            key="link"
            type="primary"
            onClick={() => {
              form.resetFields();
            }}
          >
            重置
          </Button>,
        ]}
      >
        <Form
          form={form}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          layout="horizontal"
          autoComplete="off"
          initialValues={formData}
          onFinish={async () => {
            await setLoading(true);
            const res = await setUpdatePwd(form.getFieldsValue(true));
            if (res.status === 20000) {
              message.success(res.message);
              await setVisible(false);
            }
            await setLoading(false);
          }}
        >
          <Form.Item
            label="原密码"
            name="y_password"
            rules={[
              { required: true, message: '请输入原密码' },
              () => ({
                validator(_, value) {
                  const re = /^[a-zA-Z0-9]{4,14}$/;
                  if (re.test(value)) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('原密码必须4到14位的数字或字母'));
                },
              }),
            ]}
          >
            <Input.Password maxLength={14} allowClear placeholder="请输入原密码" />
          </Form.Item>
          <Form.Item
            label="密码"
            name="password"
            rules={[
              { required: true, message: '请输入密码' },
              () => ({
                validator(_, value) {
                  const re = /^[a-zA-Z0-9]{4,14}$/;
                  if (re.test(value)) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('密码必须4到14位的数字或字母'));
                },
              }),
            ]}
          >
            <Input.Password maxLength={14} allowClear placeholder="请输入密码" />
          </Form.Item>
          <Form.Item
            label="确认密码"
            name="password_confirmation"
            rules={[
              { required: true, message: '请输入确认密码' },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('密码和确认密码输入不一致'));
                },
              }),
            ]}
          >
            <Input.Password maxLength={14} allowClear placeholder="请输入确认密码" />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
export default forwardRef(UpdatePwd);
