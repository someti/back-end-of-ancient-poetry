// import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-layout';
const Footer: React.FC = () => {
  const defaultMessage = '咪乐多软件科技有限公司出品';

  const currentYear = new Date().getFullYear();

  return (
    <>
      <DefaultFooter
        copyright={`${currentYear} ${defaultMessage}`}
        links={[
          // {
          //   key: 'LvaCMS 2.0',
          //   title: 'LvaCMS 2.0',
          //   href: 'https://www.lvacms.cn',
          //   blankTarget: true,
          // },
          // {
          //   key: 'github',
          //   title: <GithubOutlined />,
          //   href: 'https://github.com/ant-design/ant-design-pro',
          //   blankTarget: true,
          // },
          {
            key: '开发文档地址',
            title: '开发文档地址',
            href: 'https://www.kancloud.cn/songbo-3_1/lvacms-2/2600255',
            blankTarget: true,
          },
        ]}
      />
    </>
  );
};

export default Footer;
