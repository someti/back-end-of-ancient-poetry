# 优咪乐古诗词开源版   

#### 项目介绍
优咪乐古诗词，是2022年全新推出的一款轻量级、高性能、前后端分离的古诗词管理系统，目前支持微信小程序，前后端源码完全开源，看见及所得，完美支持二次开发，可学习可商用，让您快速搭建个性化独立古诗词应用，内部数据库提供了58w古诗词和3.8w作者。

    如果对您有帮助，您可以点右上角 “Star” 收藏一下 ，获取第一时间更新，谢谢！
   

#### 安装视频
[https://www.bilibili.com/video/BV1XL4y1s76Y/?vd_source=e09fa0cac079e1a1bcdaef6e6433fed3](https://www.bilibili.com/video/BV1XL4y1s76Y/?vd_source=e09fa0cac079e1a1bcdaef6e6433fed3)

#### 文档地址
[https://www.kancloud.cn/songbo-3_1/lvacms-2/2600257](https://www.kancloud.cn/songbo-3_1/lvacms-2/2600257)

#### 技术特点
* 前后端完全分离 (互不依赖 开发效率高)
* 采用PHP7.3 (强类型严格模式)
* laravel8.5（最优雅的PHP开发框架）
* Uni-APP（开发跨平台应用的前端框架）
* Ant Design Pro5.0（企业级中后台产品UI组件库）
* Auth（基于角色的权限控制管理）
* Composer 一键引入三方扩展
* 部署运行的项目体积仅16多MB（真正的轻量化）
* 所有端代码开源 (服务端PHP、后台端react、客户端uniapp)
* 简约高效的编码风格 (可能是最适合二开的源码)
* 源码中清晰中文注释 (小白也能看懂的代码)

#### 系统演示

- 古诗文后台演示：https://testwritingadmin.lvacms.cn
- 用户名和密码：admin 123456
- 下面是公众号和小程序，还可以加微信（有交流群），第一时间获取项目进度。


![公众号](https://img.kancloud.cn/2a/3e/2a3e03cb805b053e5fc69ae441cb4cc8_344x344.jpg)
![小程序](https://img.kancloud.cn/dd/74/dd74f74859f19f3f36257b329adec2de_344x344.jpg)
![讨论可加微信（有交流群）](https://img.kancloud.cn/ae/c1/aec1973e54966b834b5163617f7d110b_300x409.png)

#### 页面展示（小程序）

![首页](https://img.kancloud.cn/7c/b7/7cb788ab247c847692dca5172188986a_400x880.png)
![文库](https://img.kancloud.cn/13/1a/131aa93c581c9608ab755d31e9bd4ae3_400x880.png)
![我的](https://img.kancloud.cn/2e/d9/2ed9a7b9267fb961c6e4d3bf5042bb85_400x880.png)
![古诗文列表](https://img.kancloud.cn/57/5b/575b23b80cbeef96c28e03b87ddd3ab8_400x880.png)
![古诗文详情](https://img.kancloud.cn/a1/77/a177e867d52ea753a33d4a80fb13e1f6_400x880.png)
![生成古诗文海报](https://img.kancloud.cn/68/2b/682b6e780cbf2de73b2b2bd99fe19c62_400x880.png)
![作者列表](https://img.kancloud.cn/e4/e2/e4e2e2ce9d6ea0ede0e57ad939d0f7c6_400x880.png)
![作者详情](https://img.kancloud.cn/2c/09/2c09d0c36d9ce925876271649c270cb6_400x880.png)

#### 页面展示（后台）

![登录页](https://img.kancloud.cn/3b/52/3b52adc2efadbb1635fb6f0de70fb325_1920x961.png)
![系统管理](https://img.kancloud.cn/e6/c8/e6c8aeb7a5794aef3d96b184a0689320_1920x961.png)
![古文管理](https://img.kancloud.cn/d7/6d/d76d40068be22bb7b2f948dcc1573588_1920x961.png)



#### 代码风格

* PHP7强类型严格模式
* 严格遵守MVC设计模式 同时具有service层
* 简约整洁的编码风格 绝不冗余一行代码
* 代码注释完整易读性高 尽量保障初级程序员也可读懂 极大提升二开效率
* 不允许直接调用和使用DB类（破坏封装性）
* 不允许使用原生SQL语句 全部使用链式操作（可维护性强）
* 不允许存在复杂SQL查询语句（可维护性强）
* 所有的CURD操作均通过ORM模型类 并封装方法（扩展性强）
* 数据库设计满足第三范式
* 前端JS编码均采用ES6标准

#### 环境要求和依赖
- CentOS 7.0+
- Nginx 1.10+
- PHP 7.3+  (推荐php7.3)
- MySQL 5.6+ (推荐MySQL 8.0)
- Redis 3.0+
- Composer 2.2.3+
- Node.js 14+
- yarn
- webpack
- eslint
- umi
- react
- [ant-design-pro](https://pro.ant.design/zh-CN/) - Ant Design Of react 实现

#### 环境所需软件和数据库（这里的数据库只是古诗文和作者部分的）下载

----

[https://pan.baidu.com/s/1YfhOk9SiGzv6m-dPFS37lA?pwd=ywu7](https://pan.baidu.com/s/1YfhOk9SiGzv6m-dPFS37lA?pwd=ywu7)

> 请注意，下载到的文件有三个文件夹，代分类数据文件夹中放的是古诗文和作者部分+分类的数据，tool.rar文件夹放的是系统搭建所需要的软件，shici.sql是简化版的古诗文和作者的数据（这个数据可以直接移植到其他的任何项目中）。




#### 服务端安装教程

----

- 切换到服务端根目录
```
cd server
```

- 安装扩展
```
composer install
```

- 生成 env 文件
```
cp .env.example .env
```
- 生成laravel的key
```
php artisan key:generate
```

- 生成jwt-auth的key
```
php artisan jwt:secret
```
- 配置MySql数据库参数
```
#数据库
DB_CONNECTION=mysql
#数据库地址
DB_HOST=127.0.0.1
#数据库端口
DB_PORT=3306
#数据库名称
DB_DATABASE=
#数据库用户名
DB_USERNAME=
#数据库密码
DB_PASSWORD=
#数据表前缀
DB_PREFIX=lv_
# 数据库字符集
DB_CHARSET=utf8mb4
# 数据库排序规则
DB_COLLATION=utf8mb4_unicode_ci
```
- 配置Redis参数
```
# redis地址
REDIS_HOST=127.0.0.1
# redis密码
REDIS_PASSWORD=
# redis端口
REDIS_PORT=6379
```
- 执行数据库迁移文件
```
php artisan module:migrate
```
- 执行数据库填充
```
php artisan module:seed
```
- 创建图片快捷方式
```
php artisan storage:link
```
- 打开phpstudy创建站点并配置伪静态
>提示：由于我们开启了redis，需要在php扩展中开启redis扩展
```
location / {
    try_files $uri $uri/ /index.php?$query_string;
}
```
- 导入古诗文数据库
```
lv_writing.sql
```
#### 后台页面安装教程

----

- 切换到后台页面根目录
```
cd admin
```
- 后台页面端安装
```
tyarn
```
- 使用VsCode打开前端代码并修改config文件夹下的proxy.ts文件
```
dev: {
 '/api/': {
         // 要代理的地址
         target:  '你配置的服务端域名',
         // 配置了这个可以从 http 代理到 https
         // 依赖 origin 的功能可能需要这个，比如 cookie
         changeOrigin:  true,
    },
  },
```
- 启动项目
```
npm run  start:dev
```
- 项目打包
```
npm run build
```
- 配置admin 代理
```
location /api {
	  include  uwsgi_params;
	  proxy_pass  'http://域名/api';
}
proxy_set_header X-Forwarded-For $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
```
#### 小程序端安装教程
- 切换到小程序端根目录
```
打开 writing/config/config.js
修改config.js
export default {
    // 项目ID
    projectId: '9',   
    name:"项目名称",
    coverImgUrl:'https://自己的域名/static/WritingAdmin/coverImgUrl.png',
    // api路径
    requsetUrl:'https://自己的域名/api/v1/writing',
}
```
- 使用HBuilderX运行（这里选择微信小程序）

>注意这里可能运行不成功，如果运行失败，可以关闭微信开发者工具，重新运行一次。