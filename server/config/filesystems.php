<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],
        'sql' => [
            'driver' => 'local',
            'root' => storage_path('app/public/sql'),
            'url' => '/storage/sql',
            'visibility' => 'public',
        ],
        'logs' => [
            'driver' => 'local',
            'root' => storage_path('logs'),
            'url' => '/storage/logs',
            'visibility' => 'public',
        ],
        'DistributionAdmin' => [
            'driver' => 'local',
            'root' => storage_path('DistributionAdmin'),
            'url' => '/storage/DistributionAdmin',
            'visibility' => 'public',
        ],
        'WebsiteAdmin' => [
            'driver' => 'local',
            'root' => storage_path('WebsiteAdmin'),
            'url' => '/storage/WebsiteAdmin',
            'visibility' => 'public',
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public/upload'),
            'url' => '/storage/upload',
            'visibility' => 'public',
        ],
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
            'use_path_style_endpoint' => env('AWS_USE_PATH_STYLE_ENDPOINT', false),
        ],
        'qiniu' => [
            'driver'  => 'qiniu',
            'domains' => [
                'default'   => env('QINIU_DEFAULT','your default'),
                'https'     => env('QINIU_HTTPS','your https'),
                'custom'    => env('QINIU_CUSTOM',''),
            ],
            'access_key'=> env('QINIU_ACCESS_KEY',''),
            'secret_key'=> env('QINIU_SECRET_KEY',''),
            'bucket'    => env('QINIU_BUCKET',''),
            'notify_url'=> env('QINIU_NOTIFY_URL',''),
        ],
        'oss' => [
            'driver'        => 'oss',
            'access_id'     => env('ALIYUN_ACCESS_ID'),
            'access_key'    => env('ALIYUN_ACCESS_KEY'),
            'bucket'        => env('ALIYUN_BUCKET'),
            'cname_bucket'  => env('ALIYUN_CNAME_BUCKET','your cname_bucket'),
            'endpoint'      => env('ALIYUN_ENDPOINT','your endpoint'),
            'cdnDomain'     => env('ALIYUN_CDN_DOMAIN'),
            'ssl'           => env('ALIYUN_SSL'),
            'isCName'       => env('ALIYUN_IS_CNAME'),
            'debug'         => env('ALIYUN_DEBUG')
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage/upload') => storage_path('app/public/upload'),
        public_path('storage/sql') => storage_path('app/public/sql'),
        public_path('storage/logs') => storage_path('logs'),
        public_path('storage/DistributionAdmin') => storage_path('DistributionAdmin'),
        public_path('storage/WebsiteAdmin') => storage_path('WebsiteAdmin'),
    ],
];
