<?php

namespace Modules\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommonSortRequest extends FormRequest
{
    /**
     * php artisan module:make-request AdminRequest Admin
     */

    public function authorize()
    {
        return true;
    }
	public function rules()
    {
        return [
			'sort' 	    => 'required|is_positive_integer',

        ];
    }
	public function messages(){
		return [
			'sort.required' 				    => '缺少参数！',
			'sort.is_positive_integer' 		=> '参数错误！',
		];
	}
}









