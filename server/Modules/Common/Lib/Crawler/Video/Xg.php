<?php


namespace Modules\Common\Lib\Crawler\Video;


use QL\QueryList;

class Xg implements Index
{
    private $status = 1;
    private $message = '西瓜视频提取成功';
    public function index(string $url,string $httpUrl)
    {
        return $this->init($url,$httpUrl);
    }

    private function init(string $url,string $httpUrl){
        $video_url = null;
        $title = null;
        $info = QueryList::get($url,[],[
            'headers' => [
                'Cookie' => 'passport_csrf_token=92947f40e87238ef365651393b8c5255; passport_csrf_token_default=92947f40e87238ef365651393b8c5255; odin_tt=7bef4f9110a75109fa8be86c8ad6eedc32ce3e7852cb143314032e19cec2e0004060676366e4b2383f41c4dc382e723b027af8a883c071c8f18a959b5d072631; passport_auth_status=9ad6a532813ed5dfe8cd671488ad9ffe%2Cf130007850126c40c898faa060347276; passport_auth_status_ss=9ad6a532813ed5dfe8cd671488ad9ffe%2Cf130007850126c40c898faa060347276; sid_guard=e5db7cb5bdb93c05c8edb89486c5dd2f%7C1692601793%7C5184001%7CFri%2C+20-Oct-2023+07%3A09%3A54+GMT; uid_tt=041593be638adb028b02f06c7b747614; uid_tt_ss=041593be638adb028b02f06c7b747614; sid_tt=e5db7cb5bdb93c05c8edb89486c5dd2f; sessionid=e5db7cb5bdb93c05c8edb89486c5dd2f; sessionid_ss=e5db7cb5bdb93c05c8edb89486c5dd2f; sid_ucp_v1=1.0.0-KDAyNjNkMzFlZTZkMDhjN2QzNDY3MjNhY2FlNjgzZGNiY2QzYjQxZmEKGQi0pcCEzfT_BRDBm4ynBhjoDSAMOAJA8QcaAmxxIiBlNWRiN2NiNWJkYjkzYzA1YzhlZGI4OTQ4NmM1ZGQyZg; ssid_ucp_v1=1.0.0-KDAyNjNkMzFlZTZkMDhjN2QzNDY3MjNhY2FlNjgzZGNiY2QzYjQxZmEKGQi0pcCEzfT_BRDBm4ynBhjoDSAMOAJA8QcaAmxxIiBlNWRiN2NiNWJkYjkzYzA1YzhlZGI4OTQ4NmM1ZGQyZg; _ga=GA1.1.540510114.1688367355; _ga_SQMSS7G7R9=GS1.1.1692601871.2.1.1692602181.0.0.0; support_webp=true; support_avif=true; _tea_utm_cache_3586={%22utm_source%22:%22copy_link%22%2C%22utm_medium%22:%22android%22%2C%22utm_campaign%22:%22client_share%22}; _tea_utm_cache_1300={%22utm_source%22:%22copy_link%22%2C%22utm_medium%22:%22android%22%2C%22utm_campaign%22:%22client_share%22}; ttwid=1%7CMTkO4bSr94meEH8E_3flDpDTdJmoI4T7px5-AlZWg7E%7C1692949771%7C7cb06b7d7bb445a23ee058726367e62a0892dc27b8debedb26c46eb1d25d9120; msToken=SDryOZo803d45oqoCYymUHvHeiU9wtEkPW6TEgmh-NR2mgUXB04nqjLtV0LBDpiCkBDHb8eZNWQI5U8Rpgl3RD1qQye-SFp0KAYhIV3M',

            ]
        ])->getHtml();
        preg_match_all("/window._SSR_HYDRATED_DATA=(.*?)<\/script>/",$info,$info);

        if(!isset($info[1][0]) || !$info[1][0]){
            $this->message = "你要观看的视频不存在";
            $this->status = 0;
        }else{
            $info = $info[1][0];
            $info = str_replace('undefined','""',$info);
            $info = str_replace('""",','",',$info);
            $info = str_replace('"""','"',$info);
            $info = json_decode($info,true);
            if(!isset($info['anyVideo']['gidInformation']['packerData']['video']['title']) || !isset($info['anyVideo']['gidInformation']['packerData']['video']['videoResource']['dash']['dynamic_video']['dynamic_video_list'][3]['main_url'])){
                $this->message = "你要观看的视频不存在";
                $this->status = 0;
            }
            $title = $info['anyVideo']['gidInformation']['packerData']['video']['title'];
            $video_url = base64_decode($info['anyVideo']['gidInformation']['packerData']['video']['videoResource']['dash']['dynamic_video']['dynamic_video_list'][3]['main_url']);
        }
        return [
            'status'=>$this->status,
            'data'=>[
                'video_url'=>$video_url,
                'title'=>$title
            ],
            'message'=>$this->message
        ];
    }


    private function httpRequest($url, $data = null, $cookieFile = false, $headers = false, $proxy = false, $outTime = 5)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url.'?wid_try=1');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            if (is_array($data)) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            } else {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
        }
        curl_setopt($curl, CURLOPT_REFERER, $url.'?wid_try=1');
        if ($cookieFile) {
            curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
            curl_setopt($curl, CURLOPT_COOKIEFILE, $cookieFile);
        }
        if (is_array($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        if (is_array($proxy)) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy['ip']);
            curl_setopt($curl, CURLOPT_PROXYPORT, $proxy['port']);
        } else if (is_string($proxy)) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy);
        }
        curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, $outTime);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($curl);
        $curlInfo = curl_getinfo($curl);
        $header = substr($response, 0, $curlInfo['header_size']); // 根据头大小获取头信息
        $body = substr($response, $curlInfo['header_size']);

        curl_close($curl);
        return [
            'header' => $header,
            'body' => $body,
            'request_header' => $curlInfo['request_header']
        ];
    }
}
