<?php


namespace Modules\Common\Lib\Crawler\Video;


interface Index
{
    public function index(string $url,string $httpUrl);
}
