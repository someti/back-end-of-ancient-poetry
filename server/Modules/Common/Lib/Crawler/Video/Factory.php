<?php


namespace Modules\Common\Lib\Crawler\Video;


class Factory
{
    static private $urlList = [
        "https://v.douyin.com"=>"\Modules\Common\Lib\Crawler\Video\Dy",
        "https://www.ixigua.com"=>"\Modules\Common\Lib\Crawler\Video\Xg",
        "https://m.ixigua.com"=>"\Modules\Common\Lib\Crawler\Video\Xg",
        "https://b23.tv"=>"\Modules\Common\Lib\Crawler\Video\Bz",
        "https://www.bilibili.com"=>"\Modules\Common\Lib\Crawler\Video\Bz1",
    ];
    static function getVideo(string $url){
        $url = trim($url);
        $name = null;
        $httpUrl = self::getHttpUrlStr($url);
        if(!isset(self::$urlList[$httpUrl])){
            return [
              'status'=>0,
              'message'=>'请输入正确的url'
            ];
        }
        $newClass = new self::$urlList[$httpUrl]();
        return $newClass->index($url,$httpUrl);
    }
    private static function getHttpUrlStr(string $url){
        $urlArr = parse_url($url);
        if(isset($urlArr['scheme']) && isset($urlArr['host'])){
            return $urlArr['scheme'] . '://' . $urlArr['host'];
        }
        return '';
    }
}
