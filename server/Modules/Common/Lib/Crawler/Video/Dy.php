<?php


namespace Modules\Common\Lib\Crawler\Video;


class Dy implements Index
{
    private $status = 1;
    private $message = '抖音视频提取成功';
    public function index(string $url,string $httpUrl)
    {
        return $this->init($url,$httpUrl);
    }

    private function init(string $url,string $httpUrl){
        $video_url = '';
        $cover = '';
        $music = '';
        $ext = [];
        $header = get_headers($url,1);
        $new_url = $header['Location'][0];
        $item_ids = explode("/",parse_url($new_url)['path'])[3];
        $res = $this->httpRequest("https://www.iesdouyin.com/web/api/v2/aweme/iteminfo/?item_ids=".$item_ids);
        $data = json_decode($res['body'],true);
        if(!count($data['item_list'])){
            $this->message = "你要观看的视频不存在";
            $this->status = 0;
        }else{
            // 封面图片
            $cover = $data['item_list'][0]['video']['cover']['url_list'][0];
// 音频
            $music = $data['item_list'][0]['music']['play_url']['uri'];
// 其他信息
            $ext = $data['item_list'][0]['share_info'];

// vid
            $vid = json_decode($res['body'],true)['item_list'][0]['video']['vid'];
            $res = $this->httpRequest("https://aweme.snssdk.com/aweme/v1/play/?video_id=".$vid."&ratio=720p&line=0",null,false,[
                "user-agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.81 Safari/537.36"
            ]);
// 视频地址
            $regex = '/https?\:\/\/[^\\r]+/i';
            preg_match_all($regex, $res['header'], $matches);
            if(isset($matches[0]) && isset($matches[0][0])){
                $video_url = $matches[0][0];
            }
        }
        return [
            'status'=>$this->status,
            'data'=>[
                'url'=>$video_url,
                'cover'=>$cover,
                'music'=>$music,
                'ext'=>$ext
            ],
            'message'=>$this->message
        ];

    }


    private function httpRequest($url, $data = null, $cookieFile = false, $headers = false, $proxy = false, $outTime = 5)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            if (is_array($data)) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            } else {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
        }
        if ($cookieFile) {
            curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
            curl_setopt($curl, CURLOPT_COOKIEFILE, $cookieFile);
        }
        if (is_array($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        if (is_array($proxy)) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy['ip']);
            curl_setopt($curl, CURLOPT_PROXYPORT, $proxy['port']);
        } else if (is_string($proxy)) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy);
        }
        curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, $outTime);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($curl);
        $curlInfo = curl_getinfo($curl);
        $header = substr($response, 0, $curlInfo['header_size']); // 根据头大小获取头信息
        $body = substr($response, $curlInfo['header_size']);

        curl_close($curl);
        return [
            'header' => $header,
            'body' => $body,
            'request_header' => $curlInfo['request_header']
        ];
    }
}
