<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2022/1/26 17:25
 */

namespace Modules\BlogApi\Services\Picture;
use Modules\BlogApi\Models\BlogPicture;
use Modules\BlogApi\Services\BaseApiServices;
class PictureServices extends BaseApiServices
{
    public function list($project_id){
        $list['banner'] = BlogPicture::query()->select('id','type','image_id','content','url')
            ->where([
                "status"=>1,
                "project_id"=>$project_id,
                'type'=>1
            ])
            ->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')
            ->get()->toArray();
        $list['banner_two'] = BlogPicture::query()->select('id','type','image_id','content','url')
            ->where([
                "status"=>1,
                "project_id"=>$project_id,
                'type'=>2
            ])
            ->with([
                'imageTo'=>function($query){
                    $query->select('id','url','open');
                }
            ])->orderBy('sort','asc')->orderBy('id','desc')
            ->get()->toArray();
        return $this->apiSuccess('',$list);
    }
}
