<?php

namespace Modules\BlogApi\Http\Requests\Common;

use Illuminate\Foundation\Http\FormRequest;

class ProjectIdRequest extends FormRequest
{
    /**
     * php artisan module:make-request AdminRequest Admin
     */

    public function authorize()
    {
        return true;
    }
	public function rules()
    {
        return [
			'id' 	            => 'required|is_positive_integer',
            "project_id"            =>'required|is_positive_integer',
        ];
    }
	public function messages(){
		return [
			'limit.required' 				    => '缺少参数id！',
			'limit.is_positive_integer' 		=> '参数错误id！',
            'project_id.required'                 =>'缺少参数project_id',
            'project_id.is_positive_integer'      =>'project_id参数格式错误',
		];
	}
}









