<?php


use Illuminate\Support\Facades\Route;
Route::group(["prefix"=>"v1/blogApi"],function (){
    // 网站配置信息
    Route::post('/config/info', 'v1\ConfigController@info');


    // 网页分类
    Route::post('/articleType/list', 'v1\ArticleTypeController@list');


    // 图片列表
    Route::post('/picture/list', 'v1\PictureController@list');

    // 标签列表
    Route::post('/label/list', 'v1\LabelController@list');


    // 友情链接列表
    Route::post('/link/list', 'v1\LinkController@list');

    // 文章列表
    Route::post('/article/list', 'v1\ArticleController@list');


    // 文章详情
    Route::post('/article/details', 'v1\ArticleController@details');

});
