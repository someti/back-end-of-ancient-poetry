<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */
namespace Modules\WritingAdmin\Services\Author;
use Modules\WritingAdmin\Models\WritingAuthor;
use Modules\WritingAdmin\Models\WritingCollectionAuthor;
use Modules\WritingAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class AuthorServices extends BaseApiServices
{
    public function index(array $data)
    {
        $model = WritingAuthor::query();
        $model = $this->setWhereQueryProject($model);
        $model = $this->queryCondition($model, $data, "name");
        if (isset($data['sort'])) {
            $model = $this->querySort($model, $data['sort']);
        }
        if(isset($data['dynasty_id']) && $data['dynasty_id'] > 0){
            $model = $model->where('dynasty_id',$data['dynasty_id']);
        }
        $list = $model->orderBy('id', 'desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('', [
            'list' => $list['data'],
            'total' => $list['total']
        ]);
    }
    public function add(array $data)
    {
        $data = $this->getCommonId($data);
        return $this->commonCreate(WritingAuthor::query(),$data);
    }
    public function edit(int $id)
    {
        $data = WritingAuthor::query()->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }
    public function update(int $id,array $data)
    {
        return $this->commonUpdate(WritingAuthor::query(),$id,$data);
    }
    public function status(int $id,array $data)
    {
        return $this->commonStatusUpdate(WritingAuthor::query(),$id,$data);
    }
    public function sorts(int $id,array $data)
    {
        return $this->commonSortsUpdate(WritingAuthor::query(),$id,$data);
    }
    public function del(int $id)
    {
        return $this->commonDestroy(WritingAuthor::query(),['id'=>$id]);
    }
    public function delAll(array $idArr)
    {
        return $this->commonDestroy(WritingAuthor::query(),$idArr);
    }

    public function getAuthorList($data){
        $model = WritingAuthor::query()->select('id','name','name_tr','dynasty','dynasty_tr');
        $model = $model->where(['project_id'=>$data['project_id'],'status'=>1]);
        if (!empty($data['key'])){
            $model = $model->where('name','like','%' . $data['key'] . '%');
            $model = $model->orWhere('name_tr','like','%' . $data['key'] . '%');
        }
        if(isset($data['dynasty_id'])){
            $model = $model->whereIn('dynasty_id',explode(',',$data['dynasty_id']));
        }
        $list = $model->orderBy('sort','asc')
            ->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('', [
            'list' => $list['data'],
            'total' => $list['total']
        ]);
    }

    public function getAuthorDetails($data){
        if(!isset($data['id']) || $data['id'] <= 0){
            $this->apiError('缺少或错误参数id');
        }
        $info = WritingAuthor::query()->select('id','name','name_tr','death_year','birth_year','desc','desc_tr','dynasty','dynasty_tr')->where(['project_id'=>$data['project_id'],'status'=>1,'id'=>$data['id']])->first();
        if(!$info){
            $this->apiError('暂无数据');
        }
        $info = $info->toArray();
        $info['collection_status'] = false;
        if(isset($data['user_id']) && $data['user_id'] > 0) {
            $id = WritingCollectionAuthor::query()->where(['user_id' => $data['user_id'],'author_id'=>$data['id']])->value('id');
            if ($id) {
                $info['collection_status'] = true;
            }
        }
        return $this->apiSuccess('',$info);
    }
}
