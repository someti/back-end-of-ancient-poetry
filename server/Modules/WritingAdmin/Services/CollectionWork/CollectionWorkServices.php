<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */
namespace Modules\WritingAdmin\Services\CollectionWork;
use Modules\WritingAdmin\Models\WritingCollectionWork;
use Modules\WritingAdmin\Models\WritingWork;
use Modules\WritingAdmin\Services\BaseApiServices;

class CollectionWorkServices extends BaseApiServices
{
    public function setWorks($data){
        if(!isset($data['id']) || $data['id']<=0 || !isset($data['user_id']) || $data['user_id']<=0){
            $this->apiError('网络错误');
        }
        if(!WritingWork::query()->where(['id'=>$data['id']])->value('id')){
            $this->apiError('古诗文不存在');
        }
        $id = WritingCollectionWork::query()->where([
            "project_id"=>$data['project_id'],
            'admin_id'=>1,
            'user_id'=>$data['user_id'],
            'work_id'=>$data['id']
        ])->value('id');
        if($id){
            $res = WritingCollectionWork::query()->where(['id'=>$id])->delete();
        }else{
            $res = WritingCollectionWork::query()->insert([
                "project_id"=>$data['project_id'],
                'admin_id'=>1,
                'user_id'=>$data['user_id'],
                'work_id'=>$data['id'],
                'created_at'=> date('Y-m-d H:i:s')
            ]);
        }
        if(!$res){
            $this->apiError('网络错误，请重试');
        }
        if($id){
            return $this->apiSuccess('取消收藏成功');
        }else{
            return $this->apiSuccess('收藏成功');
        }
    }
    public function getWorksList($data){
        $model = WritingCollectionWork::query()->select('id','work_id','created_at')
            ->where(['user_id'=>$data['user_id'],"project_id"=>$data['project_id']])
            ->whereHas('work_one',function($quest)use($data){
                $quest->where(['status'=>1]);
            })
            ->with(['work_one'=>function($quest){
                $quest->select('id','title','title_tr','author','author_tr','dynasty','dynasty_tr','content','content_tr','quote','quote_tr');
            }]);
        if(isset($data['textbook_id'])){
            $model = $model->whereHas('work_one.textbook_one',function($quest)use($data){
                $quest->whereIn('textbook_id',explode(',',$data['textbook_id']));
            });
        }
        if(isset($data['solar_term_id']) && $data['solar_term_id'] > 0){
            $model = $model->whereHas('work_one',function($quest)use($data){
                $quest->whereIn('solar_term_id',explode(',',$data['solar_term_id']));
            });
        }
        if(isset($data['dynasty_id']) && $data['dynasty_id'] > 0){
            $model = $model->whereHas('work_one',function($quest)use($data){
                $quest->whereIn('dynasty_id',explode(',',$data['dynasty_id']));
            });
        }
        $list = $model->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('', [
            'list' => $list['data'],
            'total' => $list['total']
        ]);
    }
}
