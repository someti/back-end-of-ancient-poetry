<?php


namespace Modules\WritingAdmin\Services\Command;
use Modules\WritingAdmin\Jobs\Creategetdxcn10ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn11ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn12ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn13ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn14ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn15ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn16ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn17ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn2ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn3ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn4ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn5ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn6ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn7ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn8ItemQueue;
use Modules\WritingAdmin\Jobs\Creategetdxcn9ItemQueue;
use Modules\WritingAdmin\Models\WritingAllusion;
use Modules\WritingAdmin\Models\WritingAnthology;
use Modules\WritingAdmin\Models\WritingBrand;
use Modules\WritingAdmin\Models\WritingCitysMountain;
use Modules\WritingAdmin\Models\WritingDescribeSceneryMountain;
use Modules\WritingAdmin\Models\WritingFamousMountain;
use Modules\WritingAdmin\Models\WritingFestivalsMountain;
use Modules\WritingAdmin\Models\WritingFlowersPlant;
use Modules\WritingAdmin\Models\WritingGeography;
use Modules\WritingAdmin\Models\WritingSeason;
use Modules\WritingAdmin\Models\WritingSolarTerm;
use Modules\WritingAdmin\Models\WritingTextbook;
use Modules\WritingAdmin\Models\WritingTheme;
use Modules\WritingAdmin\Models\WritingTimeMsMountain;
use Modules\WritingAdmin\Models\WritingWork;
use Modules\WritingAdmin\Models\WritingWorkTextbook;
use Modules\WritingAdmin\Services\BaseApiServices;
use Modules\WritingAdmin\Jobs\Creategetdxcn1ItemQueue;
use Modules\WritingAdmin\Models\WritingAuthor;
use Modules\WritingAdmin\Models\WritingDynasty;

class Readable1Services extends BaseApiServices
{
    public function index($data){
        set_time_limit(0);
        $funcName = 'type'.$data['type'];
        return $this->$funcName($data);
    }
    // 作者
    private function type1($data){
        for($i=1;$i<=400;$i++){
//            $this->getDetails1Job($i,100,$data);
            Creategetdxcn1ItemQueue::dispatch(['page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn1ItemQueue');
        }
        return $this->apiSuccess();
    }
    public function getDetails1Job($page,$perPage,$data){
        $res = $this->httpRequest('https://avoscloud.com/1.1/call/getHotAuthorsIncludeCountByLikers', ['page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        if(!is_array($data['result']['authors'])){
            Creategetdxcn1ItemQueue::dispatch(['page'=>$page,'perPage'=>$perPage,'data'=>$data])->delay(now()->addSecond($page*20))->onQueue('WritingAdminCreategetdxcn1ItemQueue');
            return;
        }
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result']['authors'] as $k=>$v){
            if(!isset($v['name'])){
                continue;
            }
            if(!WritingAuthor::query()->where(['project_id'=>9,'admin_id'=>1,'name'=>$v['name']])->value('id')){
                $dynasty_id = 0;
                if(isset($v['dynasty'])){
                    $dynasty_id = WritingDynasty::query()->where('name',$v['dynasty'])->value('id');
                }
                try{
                    WritingAuthor::query()->insert([
                        'project_id'=>9,
                        'admin_id'=>1,
                        'name'=>$v['name'],
                        'name_tr'=>isset($v['nameTr'])?$v['nameTr']:'',
                        'death_year'=>$v['deathYear'],
                        'birth_year'=>$v['birthYear'],
                        'desc'=>isset($v['desc'])?$v['desc']:'',
                        'desc_tr'=>isset($v['descTr'])?$v['descTr']:'',
                        'dynasty_id'=>$dynasty_id,
                        'dynasty'=>isset($v['dynasty'])?$v['dynasty']:'',
                        'dynasty_tr'=>isset($v['dynastyTr'])?$v['dynastyTr']:'',
                        'baidu_wiki'=>isset($v['baiduWiki'])?$v['baiduWiki']:'',
                        'status'=>1,
                        'sort'=> (($page - 1)*$perPage) + $k + 1,
                        'created_at'=>$newDate
                    ]);
                }catch(\Exception $e){
                    continue;
                }

            }
        }
    }
    // 作品
    private function type2($data){
        for($i=1;$i<=75;$i++){
//            $this->getDetails2Job($i,1000,$data);
            Creategetdxcn2ItemQueue::dispatch(['page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn2ItemQueue');
        }
        return $this->apiSuccess();
    }
    public function getDetails2Job($page,$perPage,$data){
        $res = $this->httpRequest('https://avoscloud.com/1.1/call/getWorksIncludeCountByGenreKind', ['page'=>$page,'perPage'=>$perPage,'kind'=>'wen'], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        if(!is_array($data['result']['works'])){
            Creategetdxcn2ItemQueue::dispatch(['page'=>$page,'perPage'=>$perPage,'data'=>$data])->delay(now()->addSecond($page*20))->onQueue('WritingAdminCreategetdxcn2ItemQueue');
            return;
        }
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result']['works'] as $k=>$v){
            if(!isset($v['title'])){
                continue;
            }
            if(!WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'title'=>$v['title']])->value('id')){
                $dynasty_id = 0;
                if(isset($v['dynasty'])){
                    $dynasty_id = WritingDynasty::query()->where('name',$v['dynasty'])->value('id');
                }
                $author_id = 0;
                if(isset($v['authorName'])){
                    $author_id = WritingAuthor::query()->where('name',$v['authorName'])->value('id');
                }
                try{
                    WritingWork::query()->insert([
                        'project_id'=>9,
                        'admin_id'=>1,
                        'title'=>isset($v['title'])?$v['title']:'',
                        'title_tr'=>isset($v['titleTr'])?$v['titleTr']:'',
                        'author_id'=>$author_id,
                        'author'=>isset($v['authorName'])?$v['authorName']:'',
                        'author_tr'=>isset($v['authorNameTr'])?$v['authorNameTr']:'',
                        'dynasty_id'=>$dynasty_id,
                        'dynasty'=>isset($v['dynasty'])?$v['dynasty']:'',
                        'dynasty_tr'=>isset($v['dynastyTr'])?$v['dynastyTr']:'',
                        'kind'=>isset($v['kind'])?$v['kind']:'',
                        'kind_cn'=>isset($v['kindCN'])?$v['kindCN']:'',
                        'kind_cn_tr'=>isset($v['kindCNTr'])?$v['kindCNTr']:'',
                        'baidu_wiki'=>isset($v['baiduWiki'])?$v['baiduWiki']:'',
                        'content'=>isset($v['content'])?$v['content']:'',
                        'content_tr'=>isset($v['contentTr'])?$v['contentTr']:'',
                        'intro'=>isset($v['intro'])?$v['intro']:'',
                        'intro_tr'=>isset($v['introTr'])?$v['introTr']:'',
                        'annotation'=>isset($v['annotation'])?$v['annotation']:'',
                        'annotation_tr'=>isset($v['annotationTr'])?$v['annotationTr']:'',
                        'translation'=>isset($v['translation'])?$v['translation']:'',
                        'translation_tr'=>isset($v['translationTr'])?$v['translationTr']:'',
                        'master_comment'=>isset($v['masterComment'])?$v['masterComment']:'',
                        'master_comment_tr'=>isset($v['masterCommentTr'])?$v['masterCommentTr']:'',
                        'status'=>1,
                        'sort'=> (($page - 1)*$perPage) + $k + 1,
                        'created_at'=>$newDate
                    ]);
                }catch(\Exception $e){
                    continue;
                }
            }
        }
    }

    // 名句
    private function type3($data){
        for($i=1;$i<=43;$i++){
//            $this->getDetails3Job($i,100,$data);
            Creategetdxcn3ItemQueue::dispatch(['page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn3ItemQueue');
        }
        return $this->apiSuccess();
    }
    public function getDetails3Job($page,$perPage,$data){

        $res = $this->httpRequest('https://avoscloud.com/1.1/call/getQuotesIncludeCount', ['page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result']['quotes'] as $k=>$v){
            try{
                $id = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['work']['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($id){
                    WritingWork::query()->where('id',$id)->update([
                        'quote_Tr'=>$v['quoteTr'],
                        'quote'=>$v['quote'],
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }

    // 用典
    private function type4($data){
        $list = WritingAllusion::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails4Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn4ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn4ItemQueue');
            }
        }
        return $this->apiSuccess();

    }
    public function getDetails4Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'allusion_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }


    // 选集
    private function type5($data){
        $list = WritingAnthology::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails5Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn5ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn5ItemQueue');
            }
        }
        return $this->apiSuccess();

    }
    public function getDetails5Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'anthology_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }

    // 主题
    private function type6($data){
        $list = WritingTheme::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails6Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn6ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn6ItemQueue');
            }
        }
        return $this->apiSuccess();

    }
    public function getDetails6Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'theme_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }
    // 词牌
    private function type7($data){
        $list = WritingBrand::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails7Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn7ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn7ItemQueue');
            }
        }
        return $this->apiSuccess();

    }
    public function getDetails7Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'brand_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }



    // 地理
    private function type8($data){
        $list = WritingGeography::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails8Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn8ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn8ItemQueue');
            }
        }
        return $this->apiSuccess();

    }
    public function getDetails8Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'geography_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }


    // 名山
    private function type9($data){
        $list = WritingFamousMountain::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails9Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn9ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn9ItemQueue');
            }
        }
        return $this->apiSuccess();

    }
    public function getDetails9Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'famous_mountain_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }

    // 城市
    private function type10($data){
        $list = WritingCitysMountain::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails10Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn10ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn10ItemQueue');
            }
        }
        return $this->apiSuccess();

    }
    public function getDetails10Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'city_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }
    // 时间
    private function type11($data){
        $list = WritingTimeMsMountain::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails11Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn11ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn11ItemQueue');
            }
        }
        return $this->apiSuccess();
    }
    public function getDetails11Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'time_m_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }


    // 写景
    private function type12($data){
        $list = WritingDescribeSceneryMountain::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails12Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn12ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn12ItemQueue');
            }
        }
        return $this->apiSuccess();
    }
    public function getDetails12Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'describe_scenery_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }

    // 节日
    private function type13($data){
        $list = WritingFestivalsMountain::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails13Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn13ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn13ItemQueue');
            }
        }
        return $this->apiSuccess();
    }
    public function getDetails13Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'festival_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }

    // 节气
    private function type14($data){
        $list = WritingSolarTerm::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails14Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn14ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn14ItemQueue');
            }
        }
        return $this->apiSuccess();
    }
    public function getDetails14Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'solar_term_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }

    // 时令
    private function type15($data){
        $list = WritingSeason::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails15Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn15ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn15ItemQueue');
            }
        }
        return $this->apiSuccess();
    }
    public function getDetails15Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'season_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }

    // 花卉
    private function type16($data){
        $list = WritingFlowersPlant::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails16Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn16ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn16ItemQueue');
            }
        }
        return $this->apiSuccess();
    }
    public function getDetails16Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId){
                    WritingWork::query()->where('id',$workId)->update([
                        'flowers_plant_id'=>$id,
                        'updated_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }

    // 花卉
    private function type17($data){
        $list = WritingTextbook::query()->get()->toArray();
        foreach ($list as $k=>$v){
            $urlArr = explode('?',$v['baidu_wiki']);
            $collectionId = explode('=',$urlArr[1])[1];
            $url = $urlArr[0];
            for ($i=1;$i<=10;$i++){
//                $this->getDetails17Job($v['id'],$url,$collectionId,$i,100,$data);
                Creategetdxcn17ItemQueue::dispatch(['id'=>$v['id'],'url'=>$url,'collectionId'=>$collectionId,'page'=>$i,'perPage'=>100,'data'=>$data])->delay(now()->addSecond($i*20))->onQueue('WritingAdminCreategetdxcn17ItemQueue');
            }
        }
        return $this->apiSuccess();
    }
    public function getDetails17Job($id,$url,$collectionId,$page,$perPage,$data){
        $res = $this->httpRequest($url, ['collectionId'=>$collectionId,'page'=>$page,'perPage'=>$perPage], $cookieFile = false, [
            "x-lc-id:".$data['x-lc-id'],
            "x-lc-sign:".$data['x-lc-sign']
        ]);
        $data = json_decode($res['body'],true);
        $newDate = date('Y-m-d H:i:s');
        foreach ($data['result'] as $k=>$v){
//            dd($v);
            try{
                $workId = WritingWork::query()->where(['project_id'=>9,'admin_id'=>1,'author'=>$v['authorName'],'title'=>$v['title'],'dynasty'=>$v['dynasty']])->value('id');
                if($workId && !WritingWorkTextbook::query()->where([
                        'work_id'=>$workId,
                        'textbook_id'=>$id
                    ])->value('id')){
                    WritingWorkTextbook::query()->insert([
                        'work_id'=>$workId,
                        'textbook_id'=>$id,
                        'created_at'=>$newDate
                    ]);
                }
            }catch(\Exception $e){
                continue;
            }
        }
    }


    private function httpRequest($url, $data = null, $cookieFile = false, $headers = false, $proxy = false, $outTime = 5)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            if (is_array($data)) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            } else {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
        }
        if ($cookieFile) {
            curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
            curl_setopt($curl, CURLOPT_COOKIEFILE, $cookieFile);
        }
        if (is_array($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        if (is_array($proxy)) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy['ip']);
            curl_setopt($curl, CURLOPT_PROXYPORT, $proxy['port']);
        } else if (is_string($proxy)) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy);
        }
        curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, $outTime);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($curl);
        $curlInfo = curl_getinfo($curl);
        $header = substr($response, 0, $curlInfo['header_size']); // 根据头大小获取头信息
        $body = substr($response, $curlInfo['header_size']);

        curl_close($curl);
        return [
            'header' => $header,
            'body' => $body,
//            'request_header' => $curlInfo['request_header'],
            'url'=>$url
        ];
    }


    public function setGenerateAudio($fileUrl){
        $url = "https://fanyi.sogou.com/reventondc/synthesis?text=你好&speed=1.0&lang=zh-CHS&from=translateweb&speaker=2";
        $data = $this->httpRequest($url, $data = null, $cookieFile = false, [
            "Accept: */*",
            "Accept-Encoding: identity;q=1, *;q=0",
            "Accept-Language: zh-CN,zh;q=0.9",
            "Connection: keep-alive",
            "Cookie: ABTEST=0|1669710973|v17; IPLOC=CN6101; SUID=FD2F91DB8586A20A000000006385C47D; SNUID=D71643C7191DF7802B4B8369193B7A58; wuid=1669712347110; FQV=99a7131898981e54048db8704e525c30; translate.sess=99fa8102-13f4-4c3e-b8e1-c9643051bbc7; SUV=1669712348047; SGINPUT_UPSCREEN=1669712348062",
            "Host: fanyi.sogou.com",
            "Range: bytes=0-",
            "Referer: ".$url,
            'sec-ch-ua: "Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
            'sec-ch-ua-mobile: ?0',
            'sec-ch-ua-platform: "Windows"',
            'Sec-Fetch-Dest: audio',
            'Sec-Fetch-Mode: no-cors',
            'Sec-Fetch-Site: same-origin',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
        ]);
//        $rawdata = $data['body'];
//        $file_path = $fileUrl . '/' . time() . '.mp3';
//        $fp = fopen($file_path, 'w');
//        fwrite($fp, $rawdata);
//        fclose($fp);
        dd($data['body']);
//        $ch = curl_init($url);
//        curl_setopt($ch, CURLOPT_HEADER, 0);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);


    }
}
