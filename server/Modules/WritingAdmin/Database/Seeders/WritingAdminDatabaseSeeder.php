<?php

namespace Modules\WritingAdmin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class WritingAdminDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(WritingTableSeederTableSeeder::class);
        $this->call(WritingTableListTableSeeder::class);
    }
}
