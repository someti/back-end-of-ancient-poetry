<?php

namespace Modules\WritingAdmin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class WritingTableListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*******************************************************************************用典********************************************************************************************************/
        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/1.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'敬民篇',
            'name_tr'=>'敬民篇',
            'image_id'=>$image_id,
            'content'=>'天地之大，黎元为先。',
            'content_tr'=>'天地之大，黎元為先。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1bd667f35600660b26ab',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/2.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'治理篇',
            'name_tr'=>'治理篇',
            'image_id'=>$image_id,
            'content'=>'大道之行，天下为公。',
            'content_tr'=>'大道之行，天下為公。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1be51579a3005f6c0465',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/3.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'修身篇',
            'name_tr'=>'修身篇',
            'image_id'=>$image_id,
            'content'=>'人而无信，不知其可也。',
            'content_tr'=>'人而無信，不知其可也。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1bf3303f39005f1f4cf9',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/4.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'法治篇',
            'name_tr'=>'法治篇',
            'image_id'=>$image_id,
            'content'=>'立善法于天下，则天下治；立善法于一国，则一国治。',
            'content_tr'=>'立善法於天下，則天下治；立善法於一國，則一國治。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c3e9f545404749f8ecb',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/5.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'辩证篇',
            'name_tr'=>'辯證篇',
            'image_id'=>$image_id,
            'content'=>'变化者，乃天地之自然。',
            'content_tr'=>'變化者，乃天地之自然。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c48808ca40072c8b0ac',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/6.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'历史篇',
            'name_tr'=>'歷史篇',
            'image_id'=>$image_id,
            'content'=>'疑今者，察之古；不知来者，视之往。',
            'content_tr'=>'疑今者，察之古；不知來者，視之往。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c569f54540066593b21',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/7.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'文学篇',
            'name_tr'=>'文學篇',
            'image_id'=>$image_id,
            'content'=>'感人心者，莫先乎情。',
            'content_tr'=>'感人心者，莫先乎情。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c5e808ca40072c8b1b6',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/8.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'任贤篇',
            'name_tr'=>'任賢篇',
            'image_id'=>$image_id,
            'content'=>'盖有非常之功，必待非常之人。',
            'content_tr'=>'蓋有非常之功，必待非常之人。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c7544d904005fc0ecba',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/9.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'信念篇',
            'name_tr'=>'信念篇',
            'image_id'=>$image_id,
            'content'=>'天行健，君子以自强不息。',
            'content_tr'=>'天行健，君子以自強不息。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c869f545404749f928b',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/10.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'笃行篇',
            'name_tr'=>'篤行篇',
            'image_id'=>$image_id,
            'content'=>'图难于其易，为大于其细。天下难事，必作于易；天下大事，必作于细。',
            'content_tr'=>'圖難於其易，為大於其細。天下難事，必作於易；天下大事，必作於細。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c069f545400665936e9',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/11.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'劝学篇',
            'name_tr'=>'勸學篇',
            'image_id'=>$image_id,
            'content'=>'吾生也有涯，而知也无涯。',
            'content_tr'=>'吾生也有涯，而知也無涯。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c169f545404749f8cae',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/12.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'天下篇',
            'name_tr'=>'天下篇',
            'image_id'=>$image_id,
            'content'=>'立天地之正位，行天下之大道。',
            'content_tr'=>'立天地之正位，行天下之大道。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c26303f39005f1f4f7e',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/13.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'廉政篇',
            'name_tr'=>'廉政篇',
            'image_id'=>$image_id,
            'content'=>'一丝一粒，我之名节；一厘一毫，民之脂膏。',
            'content_tr'=>'一絲一粒，我之名節；一厘一毫，民之脂膏。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c3544d904005fc0e97a',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/14.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'立德篇',
            'name_tr'=>'立德篇',
            'image_id'=>$image_id,
            'content'=>'修其心治其身，而后可以为政于天下。',
            'content_tr'=>'修其心治其身，而後可以為政於天下。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a1c69808ca40072c8b228',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/15.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'创新篇',
            'name_tr'=>'創新篇',
            'image_id'=>$image_id,
            'content'=>'工欲善其事必先利其器。',
            'content_tr'=>'工欲善其事必先利其器。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1b16ff808ca40073273680',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/allusion/16.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_allusions')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'为政篇',
            'name_tr'=>'為政篇',
            'image_id'=>$image_id,
            'content'=>'其身正，不令而行；其身不正，虽令不从。',
            'content_tr'=>'其身正，不令而行；其身不正，雖令不從。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1caaae808ca4565c88538c',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /*******************************************************************************选集********************************************************************************************************/


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'诗经全集',
            'name_tr'=>'詩經全集',
            'image_id'=>$image_id,
            'content'=>'中国最古老的诗歌总集。',
            'content_tr'=>'中國最古老的詩歌總集。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d4da2f600060e88946',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'楚辞全集',
            'name_tr'=>'楚辭全集',
            'image_id'=>$image_id,
            'content'=>'首部浪漫主义诗歌总集。',
            'content_tr'=>'首部浪漫主義詩歌總集。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e5a0bb9f00585c94d3',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'道德经',
            'name_tr'=>'道德經',
            'image_id'=>$image_id,
            'content'=>'道家思想哲学经典。',
            'content_tr'=>'道家思想哲學經典。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=58ad3ac28fd9c500670175f4',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'千家诗',
            'name_tr'=>'千家詩',
            'image_id'=>$image_id,
            'content'=>'启蒙性质的诗歌选本。',
            'content_tr'=>'啟蒙性質的詩歌選本。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a2a00bc9f54547de0eb6d92',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'唐诗三百首',
            'name_tr'=>'唐詩三百首',
            'image_id'=>$image_id,
            'content'=>'蘅塘退士编撰的唐诗选集。',
            'content_tr'=>'蘅塘退士編撰的唐詩選集。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d38ac247005be5621c',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'宋词三百首',
            'name_tr'=>'宋詞三百首',
            'image_id'=>$image_id,
            'content'=>'朱孝臧编撰的宋词选本。',
            'content_tr'=>'朱孝臧編撰的宋詞選本。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d30bd1d0005b1a8fb2',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'元曲三百首',
            'name_tr'=>'元曲三百首',
            'image_id'=>$image_id,
            'content'=>'元曲是中国古代文化的瑰宝。',
            'content_tr'=>'元曲是中國古代文化的瑰寶。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aa9e25c9f545448cf1ce43d',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'给孩子的诗',
            'name_tr'=>'給孩子的詩',
            'image_id'=>$image_id,
            'content'=>'适合孩子诵读的古诗词选本。',
            'content_tr'=>'適合孩子誦讀的古詩詞選本。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d3bf22ec00588688db',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'古诗十九首',
            'name_tr'=>'古詩十九首',
            'image_id'=>$image_id,
            'content'=>'南朝萧统选录的古诗集。',
            'content_tr'=>'南朝蕭統選錄的古詩集。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d5a34131006252a4e5',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'乐府诗集',
            'name_tr'=>'樂府詩集',
            'image_id'=>$image_id,
            'content'=>'古代乐府诗歌集。',
            'content_tr'=>'古代樂府詩歌集。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e4c4c971005f84e1aa',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'古文观止',
            'name_tr'=>'古文觀止',
            'image_id'=>$image_id,
            'content'=>'文言文至此观止矣。',
            'content_tr'=>'文言文至此觀止矣。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=599a4f93570c35356fe401b0',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'人间词话',
            'name_tr'=>'人間詞話',
            'image_id'=>$image_id,
            'content'=>'王国维所著的一部文学批评著作。',
            'content_tr'=>'王國維所著的一部文學批評著作。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab22447ac502e57c933edb2',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'周易',
            'name_tr'=>'周易',
            'image_id'=>$image_id,
            'content'=>'群经之首，大道之源。',
            'content_tr'=>'群經之首，大道之源。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=58ad3f8d1b69e6006c1a9492',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/14.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'中庸',
            'name_tr'=>'中庸',
            'image_id'=>$image_id,
            'content'=>'儒家论述人生修养境界的一部道德哲学专著。',
            'content_tr'=>'儒家論述人生修養境界的一部道德哲學專著。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bd6ba069f54540066bf4707',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/15.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'论语',
            'name_tr'=>'論語',
            'image_id'=>$image_id,
            'content'=>'孔子及其弟子的语录结集。',
            'content_tr'=>'孔子及其弟子的語錄結集。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bc052a3fb4ffecf3b8bdb48',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/16.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'孟子',
            'name_tr'=>'孟子',
            'image_id'=>$image_id,
            'content'=>'中国儒家典籍中的一部。',
            'content_tr'=>'中國儒家典籍中的一部。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cd5262730863b00702d2e14',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/17.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'庄子',
            'name_tr'=>'莊子',
            'image_id'=>$image_id,
            'content'=>'哲学、文学、审美学上的寓言杰作典范。',
            'content_tr'=>'哲學、文學、審美學上的寓言傑作典範。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae281072f301e003dd7ac07',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/18.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'史记',
            'name_tr'=>'史記',
            'image_id'=>$image_id,
            'content'=>'中国历史上第一部纪传体通史。',
            'content_tr'=>'中國歷史上第一部紀傳體通史。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae70fcb9f54540040196541',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/19.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'左传',
            'name_tr'=>'左傳',
            'image_id'=>$image_id,
            'content'=>'为《春秋》做注解的一部史书。',
            'content_tr'=>'為《春秋》做註解的一部史書。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ce4ef5730863b0069b04bda',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/20.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'汉书',
            'name_tr'=>'漢書',
            'image_id'=>$image_id,
            'content'=>'中国第一部纪传体断代史。',
            'content_tr'=>'中國第一部紀傳體斷代史。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cd23da743e78c006579650b',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/21.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'四十二章经',
            'name_tr'=>'四十二章經',
            'image_id'=>$image_id,
            'content'=>'《佛遗教三经》之一。',
            'content_tr'=>'《佛遺教三經》之一。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aad3f389f545417b4c1ef69',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/22.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'妙法莲华经',
            'name_tr'=>'妙法蓮華經',
            'image_id'=>$image_id,
            'content'=>'大乘佛教初期经典之一。',
            'content_tr'=>'大乘佛教初期經典之一。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5afe91e4fb4ffe005d552203',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/23.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'地藏经',
            'name_tr'=>'地藏經',
            'image_id'=>$image_id,
            'content'=>'本经叙说地藏菩萨之本愿功德及本生誓愿。',
            'content_tr'=>'本經敘說地藏菩薩之本願功德及本生誓願。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5acdc8837f6fd3003fc17cbf',
            'status'=>1,
            'sort'=>23,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/24.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'金刚经',
            'name_tr'=>'金剛經',
            'image_id'=>$image_id,
            'content'=>'金刚般若波罗蜜经。',
            'content_tr'=>'金剛般若波羅蜜經。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5836c5a6c4c9710054ac1e57',
            'status'=>1,
            'sort'=>24,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/25.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'圆觉经',
            'name_tr'=>'圓覺經',
            'image_id'=>$image_id,
            'content'=>'華嚴宗、禪宗等宗派盛行講習的經典。',
            'content_tr'=>'華嚴宗、禪宗等宗派盛行講習的經典。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5abe5fc89f54543495c05916',
            'status'=>1,
            'sort'=>25,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/26.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'六祖坛经',
            'name_tr'=>'六祖壇經',
            'image_id'=>$image_id,
            'content'=>'佛教禅宗典籍。',
            'content_tr'=>'佛教禪宗典籍。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ac72c122f301e00651b42b4',
            'status'=>1,
            'sort'=>26,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/27.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'维摩诘经',
            'name_tr'=>'維摩詰經',
            'image_id'=>$image_id,
            'content'=>'佛教大乘经典。',
            'content_tr'=>'佛教大乘經典。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b023f69fb4ffe005d6a2723',
            'status'=>1,
            'sort'=>27,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/28.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'白香词谱',
            'name_tr'=>'白香詞譜',
            'image_id'=>$image_id,
            'content'=>'一部浅易的、简明的词学入门选集。',
            'content_tr'=>'一部淺易的、簡明的詞學入門選集。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aa8c33d9f5454006a20a32c',
            'status'=>1,
            'sort'=>28,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/29.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'声律启蒙',
            'name_tr'=>'聲律啟蒙',
            'image_id'=>$image_id,
            'content'=>'掌握声韵格律的儿童启蒙读物。',
            'content_tr'=>'掌握聲韻格律的兒童啟蒙讀物。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aae71f22f301e003670ea0d',
            'status'=>1,
            'sort'=>29,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/30.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'笠翁对韵',
            'name_tr'=>'笠翁對韻',
            'image_id'=>$image_id,
            'content'=>'学习写作近体诗、词启蒙读物。',
            'content_tr'=>'學習寫作近體詩、詞啟蒙讀物。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5adfeeccac502e2713c9819f',
            'status'=>1,
            'sort'=>30,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/31.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'山海经',
            'name_tr'=>'山海經',
            'image_id'=>$image_id,
            'content'=>'中国志怪古籍。',
            'content_tr'=>'中國誌怪古籍。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5abf2a399f54541656d8bc08',
            'status'=>1,
            'sort'=>31,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/32.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'茶经',
            'name_tr'=>'茶經',
            'image_id'=>$image_id,
            'content'=>'中国茶道奠基人陆羽所著茶学专著。',
            'content_tr'=>'中國茶道奠基人陸羽所著茶學專著。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ac6088a4773f7005d690c6c',
            'status'=>1,
            'sort'=>32,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/33.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'鬼谷子',
            'name_tr'=>'鬼谷子',
            'image_id'=>$image_id,
            'content'=>'该书侧重于权谋策略及言谈辩论技巧。',
            'content_tr'=>'該書側重於權謀策略及言談辯論技巧。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ac1d948ee920a004511b421',
            'status'=>1,
            'sort'=>33,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/34.gif',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'孙子兵法',
            'name_tr'=>'孫子兵法',
            'image_id'=>$image_id,
            'content'=>'中国古典军事文化著作。',
            'content_tr'=>'中國古典軍事文化著作。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ac484797565710045c14a74',
            'status'=>1,
            'sort'=>34,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/35.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'三十六计',
            'name_tr'=>'三十六計',
            'image_id'=>$image_id,
            'content'=>'中国古代三十六个兵法策略。',
            'content_tr'=>'中國古代三十六個兵法策略。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aaa4302a22b9d0045a94f53',
            'status'=>1,
            'sort'=>35,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/36.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'文心雕龙',
            'name_tr'=>'文心雕龍',
            'image_id'=>$image_id,
            'content'=>'中国第一部系统文艺理论巨著。',
            'content_tr'=>'中國第一部系統文藝理論巨著。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5af94f31a22b9d004484589e',
            'status'=>1,
            'sort'=>36,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/37.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'红楼梦',
            'name_tr'=>'紅樓夢',
            'image_id'=>$image_id,
            'content'=>'中国古代章回体长篇小说。',
            'content_tr'=>'中國古代章回體長篇小說。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b0e703fee920a0044a3c66d',
            'status'=>1,
            'sort'=>37,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/38.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'三国演义',
            'name_tr'=>'三國演義',
            'image_id'=>$image_id,
            'content'=>'第一部长篇章回体历史演义小说。',
            'content_tr'=>'第一部長篇章回體歷史演義小說。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b98c7d4808ca43cd206d18c',
            'status'=>1,
            'sort'=>38,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/39.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'水浒传',
            'name_tr'=>'水滸傳',
            'image_id'=>$image_id,
            'content'=>'北宋梁山泊聚义的故事。',
            'content_tr'=>'北宋梁山泊聚義的故事。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b9b3465fb4ffe005cad21bc',
            'status'=>1,
            'sort'=>39,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/40.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'西游记',
            'name_tr'=>'西遊記',
            'image_id'=>$image_id,
            'content'=>'中国古代第一部浪漫主义章回体长篇神魔小说。',
            'content_tr'=>'中國古代第一部浪漫主義章回體長篇神魔小說。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b96258467f356005c5752fe',
            'status'=>1,
            'sort'=>40,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/41.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'世说新语',
            'name_tr'=>'世說新語',
            'image_id'=>$image_id,
            'content'=>'魏晋南北朝时期“笔记小说”的代表作。',
            'content_tr'=>'魏晉南北朝時期“筆記小說”的代表作。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b8612299f54540031fbee81',
            'status'=>1,
            'sort'=>41,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/42.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'三国志',
            'name_tr'=>'三國誌',
            'image_id'=>$image_id,
            'content'=>'记载三国时期的魏、蜀、吴纪传体国别史。',
            'content_tr'=>'記載三國時期的魏、蜀、吳紀傳體國別史。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bd1882617d0090062d9edf7',
            'status'=>1,
            'sort'=>42,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/43.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'传习录',
            'name_tr'=>'傳習錄',
            'image_id'=>$image_id,
            'content'=>'记载了王阳明的语录和论学书信。',
            'content_tr'=>'記載了王陽明的語錄和論學書信。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ba358b89f5454006f2fdddb',
            'status'=>1,
            'sort'=>43,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/44.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'晏子春秋',
            'name_tr'=>'晏子春秋',
            'image_id'=>$image_id,
            'content'=>'记叙春秋政治家晏婴言行的书。',
            'content_tr'=>'記敘春秋政治家晏嬰言行的書。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1a5f419f545400665ce507',
            'status'=>1,
            'sort'=>44,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/45.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'四时幽赏录',
            'name_tr'=>'四時幽賞錄',
            'image_id'=>$image_id,
            'content'=>'描述了杭州四季景色流转及赏景闲事。',
            'content_tr'=>'描述了杭州四季景色流轉及賞景閑事。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae70fe12f301e003df21737',
            'status'=>1,
            'sort'=>45,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/46.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'随园食单',
            'name_tr'=>'隨園食單',
            'image_id'=>$image_id,
            'content'=>'古代中国烹饪著作。',
            'content_tr'=>'古代中國烹飪著作。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bcec29c808ca400729fd271',
            'status'=>1,
            'sort'=>46,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/47.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'西湖梦寻',
            'name_tr'=>'西湖夢尋',
            'image_id'=>$image_id,
            'content'=>'追记往日西湖之胜，以寄亡明遗老故国哀思。',
            'content_tr'=>'追記往日西湖之勝，以寄亡明遺老故國哀思。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bd7ff570b6160006617c283',
            'status'=>1,
            'sort'=>47,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/48.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'陶庵梦忆',
            'name_tr'=>'陶庵夢憶',
            'image_id'=>$image_id,
            'content'=>'《陶庵梦忆》，明代散文集。为明朝散文家张岱所著。该书共八卷，成书于甲申明亡（1644年）之后，直至乾隆四十年（1775年）才初版行世。其中所记大多是作者亲身经历过的杂事，将种种世相展现在人们面前。',
            'content_tr'=>'《陶庵夢憶》，明代散文集。為明朝散文家張岱所著。該書共八卷，成書於甲申明亡（1644年）之後，直至乾隆四十年（1775年）才初版行世。其中所記大多是作者親身經歷過的雜事，將種種世相展現在人們面前。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e6ba89656207100772ba97b',
            'status'=>1,
            'sort'=>48,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/49.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'幽梦影',
            'name_tr'=>'幽夢影',
            'image_id'=>$image_id,
            'content'=>'中国随笔体格言小品文集。',
            'content_tr'=>'中國隨筆體格言小品文集。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cbe77d3f884af0065bffad7',
            'status'=>1,
            'sort'=>49,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/50.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'菜根谭',
            'name_tr'=>'菜根譚',
            'image_id'=>$image_id,
            'content'=>'《菜根谭》采用语录体，揉合了儒家的中庸思想、道家的无为思想和佛家的出世思想的人生处世哲学。',
            'content_tr'=>'《菜根譚》采用語錄體，揉合了儒家的中庸思想、道家的無為思想和佛家的出世思想的人生處世哲學。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e8956ec4a6d2c0076b73ef9',
            'status'=>1,
            'sort'=>50,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/51.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'淮南子',
            'name_tr'=>'淮南子',
            'image_id'=>$image_id,
            'content'=>'对后世研究秦汉时期文化起到了不可替代的作用。',
            'content_tr'=>'對後世研究秦漢時期文化起到了不可替代的作用。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ce4ef69a673f50068d4450f',
            'status'=>1,
            'sort'=>51,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/52.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'老残游记',
            'name_tr'=>'老殘遊記',
            'image_id'=>$image_id,
            'content'=>'清末文学家刘鹗的代表作。',
            'content_tr'=>'清末文學家劉鶚的代表作。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cf09589ba39c80068b16aeb',
            'status'=>1,
            'sort'=>52,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/53.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'西厢记',
            'name_tr'=>'西廂記',
            'image_id'=>$image_id,
            'content'=>'愿天下有情人终成眷属。',
            'content_tr'=>'願天下有情人終成眷屬。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5dbd2083eaa375006c489580',
            'status'=>1,
            'sort'=>53,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/54.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'月令七十二候集解',
            'name_tr'=>'月令七十二候集解',
            'image_id'=>$image_id,
            'content'=>'旧本题“元吴澄撰”。其书以七十二候分属于二十四气，各训释其所以然。考《礼记·月令》，本无七十二候之说。疑好事者为之，托名于澄也。（四库全书总目·经部·礼类存目）。',
            'content_tr'=>'舊本題“元吳澄撰”。其書以七十二候分屬於二十四氣，各訓釋其所以然。考《禮記·月令》，本無七十二候之說。疑好事者為之，托名於澄也。（四庫全書總目·經部·禮類存目）。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5db2a38eba39c800738a7116',
            'status'=>1,
            'sort'=>54,
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/anthology/55.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_anthologys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'墨子',
            'name_tr'=>'墨子',
            'image_id'=>$image_id,
            'content'=>'墨家思想著作。',
            'content_tr'=>'墨家思想著作。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=606ac56ce76ff77738cb625e',
            'status'=>1,
            'sort'=>55,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /*******************************************************************************主题********************************************************************************************************/

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'爱',
            'name_tr'=>'愛',
            'image_id'=>$image_id,
            'content'=>'愿得一心人，白头不相离。',
            'content_tr'=>'願得一心人，白頭不相離。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d4bf22ec00588688de',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'禅',
            'name_tr'=>'禪',
            'image_id'=>$image_id,
            'content'=>'溪花与禅意，相对亦忘言。',
            'content_tr'=>'溪花與禪意，相對亦忘言。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d50e3dd90058377b6e',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'茶',
            'name_tr'=>'茶',
            'image_id'=>$image_id,
            'content'=>'休对故人思故国，且将新火试新茶。',
            'content_tr'=>'休對故人思故國，且將新火試新茶。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dd0e3dd90058377ba8',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'酒',
            'name_tr'=>'酒',
            'image_id'=>$image_id,
            'content'=>'且将新火试新茶，诗酒趁年华。',
            'content_tr'=>'且將新火試新茶，詩酒趁年華。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5836c5a7c59e0d0057795476',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'战争',
            'name_tr'=>'戰爭',
            'image_id'=>$image_id,
            'content'=>'马作的卢飞快，弓如霹雳弦惊。',
            'content_tr'=>'馬作的盧飛快，弓如霹靂弦驚。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d70e3dd90058377b80',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'离别',
            'name_tr'=>'離別',
            'image_id'=>$image_id,
            'content'=>'海内存知己，天涯若比邻。',
            'content_tr'=>'海內存知己，天涯若比鄰。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81db0bd1d0005b1a9003',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'悼亡',
            'name_tr'=>'悼亡',
            'image_id'=>$image_id,
            'content'=>'赌书消得泼茶香，当时只道是寻常。',
            'content_tr'=>'賭書消得潑茶香，當時只道是尋常。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d4816dfa005eff4c35',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'思乡',
            'name_tr'=>'思鄉',
            'image_id'=>$image_id,
            'content'=>'露从今夜白，月是故乡明。',
            'content_tr'=>'露從今夜白，月是故鄉明。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d5da2f600060e8894a',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'孤独',
            'name_tr'=>'孤獨',
            'image_id'=>$image_id,
            'content'=>'拣尽寒枝不肯栖，寂寞沙洲冷。',
            'content_tr'=>'揀盡寒枝不肯棲，寂寞沙洲冷。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dc816dfa005eff4c7b',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'壮志',
            'name_tr'=>'壯誌',
            'image_id'=>$image_id,
            'content'=>'会当临绝顶，一览众山小。',
            'content_tr'=>'會當臨絕頂，一覽眾山小。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dc8ac247005be5626e',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'田园',
            'name_tr'=>'田園',
            'image_id'=>$image_id,
            'content'=>'稻花香里说丰年，听取蛙声一片。',
            'content_tr'=>'稻花香裏說豐年，聽取蛙聲一片。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dcc4c971005f84e136',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'边塞',
            'name_tr'=>'邊塞',
            'image_id'=>$image_id,
            'content'=>'征蓬出汉塞，归雁入胡天。',
            'content_tr'=>'征蓬出漢塞，歸雁入胡天。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81ddd2030900695e3f26',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'友情',
            'name_tr'=>'友情',
            'image_id'=>$image_id,
            'content'=>'春草明年绿，王孙归不归？',
            'content_tr'=>'春草明年綠，王孫歸不歸？',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5836c5a6c4c9710054ac1e59',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/14.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'羁旅',
            'name_tr'=>'羈旅',
            'image_id'=>$image_id,
            'content'=>'聒碎乡心梦不成，故园无此声。',
            'content_tr'=>'聒碎鄉心夢不成，故園無此聲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5836c5a667f3560065f530da',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/15.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'哲理',
            'name_tr'=>'哲理',
            'image_id'=>$image_id,
            'content'=>'不识庐山真面目，只缘身在此山中。',
            'content_tr'=>'不識廬山真面目，只緣身在此山中。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5836c5a7ac502e006c0691ad',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/16.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'怀古',
            'name_tr'=>'懷古',
            'image_id'=>$image_id,
            'content'=>'古今多少事，都付笑谈中。',
            'content_tr'=>'古今多少事，都付笑談中。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dbbf22ec005886892f',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/17.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'母亲',
            'name_tr'=>'母親',
            'image_id'=>$image_id,
            'content'=>'临行密密缝，意恐迟迟归。',
            'content_tr'=>'臨行密密縫，意恐遲遲歸。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bdaba7dfb4ffe00669aa200',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/18.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'爱国',
            'name_tr'=>'愛國',
            'image_id'=>$image_id,
            'content'=>'经典爱国诗词。',
            'content_tr'=>'經典愛國詩詞。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cb996dda673f50068691a93',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/19.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'佛',
            'name_tr'=>'佛',
            'image_id'=>$image_id,
            'content'=>'身是菩提树，心如明镜台。',
            'content_tr'=>'身是菩提樹，心如明鏡臺。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa32f6d60e700083a3277',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/20.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'僧',
            'name_tr'=>'僧',
            'image_id'=>$image_id,
            'content'=>'修身了悟如来旨，学道需参最上乘。',
            'content_tr'=>'修身了悟如來旨，學道需參最上乘。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa33599ba270008f101a6',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/21.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'仙',
            'name_tr'=>'仙',
            'image_id'=>$image_id,
            'content'=>'仙人骑白鹿，发短耳何长。',
            'content_tr'=>'仙人騎白鹿，發短耳何長。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa36dfe7b0b0008f12287',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/theme/22.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_themes')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'道',
            'name_tr'=>'道',
            'image_id'=>$image_id,
            'content'=>'素手掬青霭，罗衣曳紫烟。',
            'content_tr'=>'素手掬青靄，羅衣曳紫煙。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa38694d48b0006bb3429',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /*******************************************************************************词牌********************************************************************************************************/
        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/1.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'如梦令',
            'name_tr'=>'如夢令',
            'image_id'=>$image_id,
            'content'=>'应是绿肥红瘦。',
            'content_tr'=>'應是綠肥紅瘦。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28ef911579a30062cebd3e',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/2.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'醉花阴',
            'name_tr'=>'醉花陰',
            'image_id'=>$image_id,
            'content'=>'人比黄花瘦。',
            'content_tr'=>'人比黃花瘦。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28f13c2f301e0063205fe5',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/3.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'虞美人',
            'name_tr'=>'虞美人',
            'image_id'=>$image_id,
            'content'=>'春花秋月何时了。',
            'content_tr'=>'春花秋月何時了。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28f25aee920a00447afcaa',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/4.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'定风波',
            'name_tr'=>'定風波',
            'image_id'=>$image_id,
            'content'=>'莫听穿林打叶声。',
            'content_tr'=>'莫聽穿林打葉聲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28eb54fe88c20062523fff',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/5.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'洞仙歌',
            'name_tr'=>'洞仙歌',
            'image_id'=>$image_id,
            'content'=>'时见疏星度河汉。',
            'content_tr'=>'時見疏星度河漢。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28eea3128fe10046e4e69d',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/6.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雨霖铃',
            'name_tr'=>'雨霖鈴',
            'image_id'=>$image_id,
            'content'=>'多情自古伤离别。',
            'content_tr'=>'多情自古傷離別。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28f348128fe100694c833d',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/7.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'长相思',
            'name_tr'=>'長相思',
            'image_id'=>$image_id,
            'content'=>'浅情人不知。',
            'content_tr'=>'淺情人不知。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28f78c1579a30062cef459',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/8.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'念奴娇',
            'name_tr'=>'念奴嬌',
            'image_id'=>$image_id,
            'content'=>'一樽还酹江月。',
            'content_tr'=>'一樽還酹江月。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28f92b128fe100694cad9e',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/9.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'六州歌头',
            'name_tr'=>'六州歌頭',
            'image_id'=>$image_id,
            'content'=>'一诺千金重。',
            'content_tr'=>'一諾千金重。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a28f5779f54540045fe96ff',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/10.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'忆江南',
            'name_tr'=>'憶江南',
            'image_id'=>$image_id,
            'content'=>'能不忆江南。',
            'content_tr'=>'能不憶江南。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a2a41e617d0090063e55180',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/11.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'鹤冲天',
            'name_tr'=>'鶴沖天',
            'image_id'=>$image_id,
            'content'=>'自是白衣卿相。',
            'content_tr'=>'自是白衣卿相。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a2a4505ee920a7072fdd359',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/12.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'临江仙',
            'name_tr'=>'臨江仙',
            'image_id'=>$image_id,
            'content'=>'是非成败转头空。',
            'content_tr'=>'是非成敗轉頭空。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5a2a468e8d6d810062cf2aa1',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'浣溪沙',
            'name_tr'=>'浣溪沙',
            'image_id'=>$image_id,
            'content'=>'我是人间惆怅客。',
            'content_tr'=>'我是人間惆悵客。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b0cfb29a22b9d00326690fe',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/14.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'水调歌头',
            'name_tr'=>'水調歌頭',
            'image_id'=>$image_id,
            'content'=>'明月几时有。',
            'content_tr'=>'明月幾時有。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b0d06abfb4ffe005b091f62',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/15.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'菩萨蛮',
            'name_tr'=>'菩薩蠻',
            'image_id'=>$image_id,
            'content'=>'西北望长安。',
            'content_tr'=>'西北望長安。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b0e66bf67f356003b82f75b',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/16.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'鹧鸪天',
            'name_tr'=>'鷓鴣天',
            'image_id'=>$image_id,
            'content'=>'歌尽桃花扇底风。',
            'content_tr'=>'歌盡桃花扇底風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b0f9d529f5454617c3f8c7c',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/17.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'满江红',
            'name_tr'=>'滿江紅',
            'image_id'=>$image_id,
            'content'=>'笑谈渴饮匈奴血。',
            'content_tr'=>'笑談渴飲匈奴血。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b10b6909f5454617c45670b',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/18.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'蝶恋花',
            'name_tr'=>'蝶戀花',
            'image_id'=>$image_id,
            'content'=>'山长水阔知何处。',
            'content_tr'=>'山長水闊知何處。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b149a302f301e003849ad3c',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/19.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'西江月',
            'name_tr'=>'西江月',
            'image_id'=>$image_id,
            'content'=>'人生几度秋凉？',
            'content_tr'=>'人生幾度秋涼？',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b15ec29ee920a253fd544e6',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/20.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'减字木兰花',
            'name_tr'=>'減字木蘭花',
            'image_id'=>$image_id,
            'content'=>'过尽飞鸿字字愁。',
            'content_tr'=>'過盡飛鴻字字愁。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b17a288a22b9d0032a1f581',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/21.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'沁园春',
            'name_tr'=>'沁園春',
            'image_id'=>$image_id,
            'content'=>'欲与天公试比高。',
            'content_tr'=>'欲與天公試比高。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b18dfefd50eee0031dcd635',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/22.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'点绛唇',
            'name_tr'=>'點絳唇',
            'image_id'=>$image_id,
            'content'=>'却把青梅嗅。',
            'content_tr'=>'卻把青梅嗅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b19edc744d9040034833179',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/23.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'贺新郎',
            'name_tr'=>'賀新郎',
            'image_id'=>$image_id,
            'content'=>'知我者，二三子。',
            'content_tr'=>'知我者，二三子。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b19f46467f35600318acce2',
            'status'=>1,
            'sort'=>23,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/24.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'清平乐',
            'name_tr'=>'清平樂',
            'image_id'=>$image_id,
            'content'=>'拂了一身还满。',
            'content_tr'=>'拂了一身還滿。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b1e31abac502e003d6bf1d6',
            'status'=>1,
            'sort'=>24,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/25.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'满庭芳',
            'name_tr'=>'滿庭芳',
            'image_id'=>$image_id,
            'content'=>'山抹微云。',
            'content_tr'=>'山抹微雲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b1e35b59f5454003b0b36dc',
            'status'=>1,
            'sort'=>25,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/26.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'水龙吟',
            'name_tr'=>'水龍吟',
            'image_id'=>$image_id,
            'content'=>'无人会，登临意。',
            'content_tr'=>'無人會，登臨意。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b1f85a244d904003b3f9748',
            'status'=>1,
            'sort'=>26,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/27.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'好事近',
            'name_tr'=>'好事近',
            'image_id'=>$image_id,
            'content'=>'酒阑歌罢玉尊空。',
            'content_tr'=>'酒闌歌罷玉尊空。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b1f8a61a22b9d003a480685',
            'status'=>1,
            'sort'=>27,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/28.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'渔家傲',
            'name_tr'=>'漁家傲',
            'image_id'=>$image_id,
            'content'=>'羌管悠悠霜满地。',
            'content_tr'=>'羌管悠悠霜滿地。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b20c621fe88c200348e84de',
            'status'=>1,
            'sort'=>28,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/29.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'朝中措',
            'name_tr'=>'朝中措',
            'image_id'=>$image_id,
            'content'=>'别来几度春风？',
            'content_tr'=>'別來幾度春風？',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b2218adfe88c20034984a3c',
            'status'=>1,
            'sort'=>29,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/30.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'卜算子',
            'name_tr'=>'蔔算子',
            'image_id'=>$image_id,
            'content'=>'她在丛中笑。',
            'content_tr'=>'她在叢中笑。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b221daf67f3560034fcbd5e',
            'status'=>1,
            'sort'=>30,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/31.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'谒金门',
            'name_tr'=>'謁金門',
            'image_id'=>$image_id,
            'content'=>'吹皱一池春水。',
            'content_tr'=>'吹皺一池春水。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b2326769f54540035d3565b',
            'status'=>1,
            'sort'=>31,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/32.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'南乡子',
            'name_tr'=>'南鄉子',
            'image_id'=>$image_id,
            'content'=>'醉笑陪公三万场。',
            'content_tr'=>'醉笑陪公三萬場。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b232a26fe88c200349eb4d7',
            'status'=>1,
            'sort'=>32,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/33.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'玉楼春',
            'name_tr'=>'玉樓春',
            'image_id'=>$image_id,
            'content'=>'此恨不关风与月。',
            'content_tr'=>'此恨不關風與月。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b27ad0cac502e0031980887',
            'status'=>1,
            'sort'=>33,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/34.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'浪淘沙令',
            'name_tr'=>'浪淘沙令',
            'image_id'=>$image_id,
            'content'=>'把酒祝东风。',
            'content_tr'=>'把酒祝東風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b27b3a067f35600341fba14',
            'status'=>1,
            'sort'=>34,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/35.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'踏莎行',
            'name_tr'=>'踏莎行',
            'image_id'=>$image_id,
            'content'=>'杜鹃声里斜阳暮。',
            'content_tr'=>'杜鵑聲裏斜陽暮。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b28c0eb44d9040030bc4277',
            'status'=>1,
            'sort'=>35,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/36.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'南歌子',
            'name_tr'=>'南歌子',
            'image_id'=>$image_id,
            'content'=>'人间帘幕垂。',
            'content_tr'=>'人間簾幕垂。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b28c924ee920a003ba6eb59',
            'status'=>1,
            'sort'=>36,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/37.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'生查子',
            'name_tr'=>'生查子',
            'image_id'=>$image_id,
            'content'=>'记得绿罗裙。',
            'content_tr'=>'記得綠羅裙。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b29fe0ba22b9d003a6bce6c',
            'status'=>1,
            'sort'=>37,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/38.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'柳梢青',
            'name_tr'=>'柳梢青',
            'image_id'=>$image_id,
            'content'=>'行人一棹天涯。',
            'content_tr'=>'行人一棹天涯。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b2a0399ac502e0031a73e62',
            'status'=>1,
            'sort'=>38,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/39.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'苏幕遮',
            'name_tr'=>'蘇幕遮',
            'image_id'=>$image_id,
            'content'=>'明月楼高休独倚。',
            'content_tr'=>'明月樓高休獨倚。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b2b1a5c9f54540035af1c0f',
            'status'=>1,
            'sort'=>39,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/40.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'望江南',
            'name_tr'=>'望江南',
            'image_id'=>$image_id,
            'content'=>'诗酒趁年华。',
            'content_tr'=>'詩酒趁年華。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b2b50502f301e0035340489',
            'status'=>1,
            'sort'=>40,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/41.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'鹊桥仙',
            'name_tr'=>'鵲橋仙',
            'image_id'=>$image_id,
            'content'=>'金风玉露一相逢。',
            'content_tr'=>'金風玉露一相逢。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b2ca3652f301e00353ceba6',
            'status'=>1,
            'sort'=>41,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/42.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'蓦山溪',
            'name_tr'=>'驀山溪',
            'image_id'=>$image_id,
            'content'=>'风月任招呼。',
            'content_tr'=>'風月任招呼。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b2cae479f5454003510bf1a',
            'status'=>1,
            'sort'=>42,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/43.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'诉衷情令',
            'name_tr'=>'訴衷情令',
            'image_id'=>$image_id,
            'content'=>'梦远不成归。',
            'content_tr'=>'夢遠不成歸。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b30a1bf44d9040030ef3064',
            'status'=>1,
            'sort'=>43,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/44.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'木兰花慢',
            'name_tr'=>'木蘭花慢',
            'image_id'=>$image_id,
            'content'=>'可怜今夕月。',
            'content_tr'=>'可憐今夕月。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b30a5269f54540035d33a28',
            'status'=>1,
            'sort'=>44,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/45.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'阮郎归',
            'name_tr'=>'阮郎歸',
            'image_id'=>$image_id,
            'content'=>'欲将沉醉换悲凉。',
            'content_tr'=>'欲將沈醉換悲涼。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b31e402ee920a003b350b5b',
            'status'=>1,
            'sort'=>45,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/46.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'江城子',
            'name_tr'=>'江城子',
            'image_id'=>$image_id,
            'content'=>'十年生死两茫茫。',
            'content_tr'=>'十年生死兩茫茫。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b333f1e756571003ccad256',
            'status'=>1,
            'sort'=>46,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/47.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'青玉案',
            'name_tr'=>'青玉案',
            'image_id'=>$image_id,
            'content'=>'东风夜放花千树。',
            'content_tr'=>'東風夜放花千樹。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3447a4ee920a003a2b29b5',
            'status'=>1,
            'sort'=>47,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/48.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'醉落魄',
            'name_tr'=>'醉落魄',
            'image_id'=>$image_id,
            'content'=>'人间多少闲狐兔。',
            'content_tr'=>'人間多少閑狐兔。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3451872f301e0060079759',
            'status'=>1,
            'sort'=>48,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/49.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'摸鱼儿',
            'name_tr'=>'摸魚兒',
            'image_id'=>$image_id,
            'content'=>'脉脉此情谁诉？',
            'content_tr'=>'脈脈此情誰訴？',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b35e1a2ee920a003d135857',
            'status'=>1,
            'sort'=>49,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/50.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'渔父',
            'name_tr'=>'漁父',
            'image_id'=>$image_id,
            'content'=>'世上如侬有几人。',
            'content_tr'=>'世上如儂有幾人。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b35e6139f5454003b4702a5',
            'status'=>1,
            'sort'=>50,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/51.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'瑞鹤仙',
            'name_tr'=>'瑞鶴仙',
            'image_id'=>$image_id,
            'content'=>'湿云粘雁影。',
            'content_tr'=>'濕雲粘雁影。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b398784756571003a678062',
            'status'=>1,
            'sort'=>51,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/52.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'小重山',
            'name_tr'=>'小重山',
            'image_id'=>$image_id,
            'content'=>'弦断有谁听。',
            'content_tr'=>'弦斷有誰聽。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b398ad167f35600381409cb',
            'status'=>1,
            'sort'=>52,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/53.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'八声甘州',
            'name_tr'=>'八聲甘州',
            'image_id'=>$image_id,
            'content'=>'羡青山有思。',
            'content_tr'=>'羨青山有思。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3aeb849f5454003b696a2c',
            'status'=>1,
            'sort'=>53,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/54.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'喜迁莺',
            'name_tr'=>'喜遷鶯',
            'image_id'=>$image_id,
            'content'=>'岁华向晚愁思。',
            'content_tr'=>'歲華向晚愁思。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3aeed2ee920a003a31e271',
            'status'=>1,
            'sort'=>54,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/55.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'感皇恩',
            'name_tr'=>'感皇恩',
            'image_id'=>$image_id,
            'content'=>'一个小园儿。',
            'content_tr'=>'一個小園兒。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3c7f4a2f301e005f7911a2',
            'status'=>1,
            'sort'=>55,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/56.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'采桑子',
            'name_tr'=>'采桑子',
            'image_id'=>$image_id,
            'content'=>'不是人间富贵花。',
            'content_tr'=>'不是人間富貴花。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3c842b2f301e005f79495a',
            'status'=>1,
            'sort'=>56,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/57.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'醉蓬莱',
            'name_tr'=>'醉蓬萊',
            'image_id'=>$image_id,
            'content'=>'笑劳生一梦。',
            'content_tr'=>'笑勞生一夢。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3d894c67f356003a4b7073',
            'status'=>1,
            'sort'=>57,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/58.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'忆秦娥',
            'name_tr'=>'憶秦娥',
            'image_id'=>$image_id,
            'content'=>'秦娥梦断秦楼月。',
            'content_tr'=>'秦娥夢斷秦樓月。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3d8c70ee920a003bfe54f7',
            'status'=>1,
            'sort'=>58,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/59.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'齐天乐',
            'name_tr'=>'齊天樂',
            'image_id'=>$image_id,
            'content'=>'消得斜阳几度？',
            'content_tr'=>'消得斜陽幾度？',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b3ed5ae0b6160003c0d9164',
            'status'=>1,
            'sort'=>59,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/60.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'少年游',
            'name_tr'=>'少年遊',
            'image_id'=>$image_id,
            'content'=>'那堪疏雨滴黄昏。',
            'content_tr'=>'那堪疏雨滴黃昏。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b432669d50eee003ae42188',
            'status'=>1,
            'sort'=>60,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/61.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'眼儿媚',
            'name_tr'=>'眼兒媚',
            'image_id'=>$image_id,
            'content'=>'迟迟春日弄轻柔。',
            'content_tr'=>'遲遲春日弄輕柔。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b432eba9f5454003b65a1a6',
            'status'=>1,
            'sort'=>61,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/62.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'霜天晓角',
            'name_tr'=>'霜天曉角',
            'image_id'=>$image_id,
            'content'=>'云来去、数枝雪。',
            'content_tr'=>'雲來去、數枝雪。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b44768fd50eee003aed50b4',
            'status'=>1,
            'sort'=>62,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/63.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'永遇乐',
            'name_tr'=>'永遇樂',
            'image_id'=>$image_id,
            'content'=>'尚能饭否？',
            'content_tr'=>'尚能飯否？',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b45b6abfe88c2003507678a',
            'status'=>1,
            'sort'=>63,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/64.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'千秋岁',
            'name_tr'=>'千秋歲',
            'image_id'=>$image_id,
            'content'=>'东窗未白凝残月。',
            'content_tr'=>'東窗未白凝殘月。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b47361c67f3560035067bc5',
            'status'=>1,
            'sort'=>64,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/65.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'风入松',
            'name_tr'=>'風入松',
            'image_id'=>$image_id,
            'content'=>'银桥不到深山。',
            'content_tr'=>'銀橋不到深山。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b473beb9f54540031d53231',
            'status'=>1,
            'sort'=>65,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/66.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'汉宫春',
            'name_tr'=>'漢宮春',
            'image_id'=>$image_id,
            'content'=>'风流不在人知。',
            'content_tr'=>'風流不在人知。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b4863c99f5454003d83375a',
            'status'=>1,
            'sort'=>66,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/67.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'更漏子',
            'name_tr'=>'更漏子',
            'image_id'=>$image_id,
            'content'=>'梦长君不知。',
            'content_tr'=>'夢長君不知。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b4c5da30b6160003107ba04',
            'status'=>1,
            'sort'=>67,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/68.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'乌夜啼',
            'name_tr'=>'烏夜啼',
            'image_id'=>$image_id,
            'content'=>'青衫记得章台月。',
            'content_tr'=>'青衫記得章臺月。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b4c62299f54540031f876de',
            'status'=>1,
            'sort'=>68,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/69.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'祝英台近',
            'name_tr'=>'祝英臺近',
            'image_id'=>$image_id,
            'content'=>'是他春带愁来。',
            'content_tr'=>'是他春帶愁來。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b4d91f62f301e003ba689ec',
            'status'=>1,
            'sort'=>69,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/70.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'声声慢',
            'name_tr'=>'聲聲慢',
            'image_id'=>$image_id,
            'content'=>'梧桐更兼细雨。',
            'content_tr'=>'梧桐更兼細雨。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b4d956fa22b9d003c9618a5',
            'status'=>1,
            'sort'=>70,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/71.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'行香子',
            'name_tr'=>'行香子',
            'image_id'=>$image_id,
            'content'=>'一番雨，一番凉。',
            'content_tr'=>'一番雨，一番涼。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b4f01909f5454003db1dcc8',
            'status'=>1,
            'sort'=>71,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/72.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雨中花慢',
            'name_tr'=>'雨中花慢',
            'image_id'=>$image_id,
            'content'=>'睡起酒阑花谢。',
            'content_tr'=>'睡起酒闌花謝。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b4f063e9f5454003db20b45',
            'status'=>1,
            'sort'=>72,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/73.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'瑞鹧鸪',
            'name_tr'=>'瑞鷓鴣',
            'image_id'=>$image_id,
            'content'=>'玉骨冰肌未肯枯。',
            'content_tr'=>'玉骨冰肌未肯枯。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b5056399f5454003dbb7b08',
            'status'=>1,
            'sort'=>73,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/74.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'风流子',
            'name_tr'=>'風流子',
            'image_id'=>$image_id,
            'content'=>'人生须行乐。',
            'content_tr'=>'人生須行樂。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b51a32e2f301e003905f863',
            'status'=>1,
            'sort'=>74,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/75.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'武陵春',
            'name_tr'=>'武陵春',
            'image_id'=>$image_id,
            'content'=>'风住尘香花已尽。',
            'content_tr'=>'風住塵香花已盡。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b51a856808ca4006f7be5ce',
            'status'=>1,
            'sort'=>75,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/76.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'烛影摇红',
            'name_tr'=>'燭影搖紅',
            'image_id'=>$image_id,
            'content'=>'云卧衣裳冷。',
            'content_tr'=>'雲臥衣裳冷。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b55a2be808ca4006f986321',
            'status'=>1,
            'sort'=>76,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/77.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桃源忆故人',
            'name_tr'=>'桃源憶故人',
            'image_id'=>$image_id,
            'content'=>'清夜悠悠谁共？',
            'content_tr'=>'清夜悠悠誰共？',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b55bac3808ca4006f9b1988',
            'status'=>1,
            'sort'=>77,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/78.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'昭君怨',
            'name_tr'=>'昭君怨',
            'image_id'=>$image_id,
            'content'=>'道是花来春未。',
            'content_tr'=>'道是花來春未。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b56ced0808ca4006fa2a453',
            'status'=>1,
            'sort'=>78,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/79.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'酒泉子',
            'name_tr'=>'酒泉子',
            'image_id'=>$image_id,
            'content'=>'灯欲落，雁还飞。',
            'content_tr'=>'燈欲落，雁還飛。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b56d4870b616000310d912f',
            'status'=>1,
            'sort'=>79,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/80.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'夜行船',
            'name_tr'=>'夜行船',
            'image_id'=>$image_id,
            'content'=>'檀板未终人去去。',
            'content_tr'=>'檀板未終人去去。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b57e760808ca4006faaa029',
            'status'=>1,
            'sort'=>80,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/81.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'杏花天',
            'name_tr'=>'杏花天',
            'image_id'=>$image_id,
            'content'=>'又将愁眼与春风。',
            'content_tr'=>'又將愁眼與春風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b57efc7808ca4003c53b67d',
            'status'=>1,
            'sort'=>81,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/82.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'一落索',
            'name_tr'=>'一落索',
            'image_id'=>$image_id,
            'content'=>'酒不到、刘伶墓。',
            'content_tr'=>'酒不到、劉伶墓。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b5990ab128fe1002f81c6a9',
            'status'=>1,
            'sort'=>82,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/83.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'应天长',
            'name_tr'=>'應天長',
            'image_id'=>$image_id,
            'content'=>'兰棹今宵何处？',
            'content_tr'=>'蘭棹今宵何處？',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b5995f4128fe1002f81fd81',
            'status'=>1,
            'sort'=>83,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/84.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'画堂春',
            'name_tr'=>'畫堂春',
            'image_id'=>$image_id,
            'content'=>'一生一代一双人。',
            'content_tr'=>'一生一代一雙人。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b5ad8c70b616000312d906e',
            'status'=>1,
            'sort'=>84,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/85.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'最高楼',
            'name_tr'=>'最高樓',
            'image_id'=>$image_id,
            'content'=>'云出早，鸟归迟。',
            'content_tr'=>'雲出早，鳥歸遲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b5ed687ee920a003c1835a0',
            'status'=>1,
            'sort'=>85,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/86.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'调笑令',
            'name_tr'=>'調笑令',
            'image_id'=>$image_id,
            'content'=>'边草尽来兵老。',
            'content_tr'=>'邊草盡來兵老。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b6028af9f5454003c48a45e',
            'status'=>1,
            'sort'=>86,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/87.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'杨柳枝',
            'name_tr'=>'楊柳枝',
            'image_id'=>$image_id,
            'content'=>'听唱新翻杨柳枝。',
            'content_tr'=>'聽唱新翻楊柳枝。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b616ab2808ca4006ff43e93',
            'status'=>1,
            'sort'=>87,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/88.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'花心动',
            'name_tr'=>'花心動',
            'image_id'=>$image_id,
            'content'=>'夜长更漏传声远。',
            'content_tr'=>'夜長更漏傳聲遠。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b61749c2f301e00397d2e40',
            'status'=>1,
            'sort'=>88,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/89.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'思佳客',
            'name_tr'=>'思佳客',
            'image_id'=>$image_id,
            'content'=>'衣懒换，酒难赊。',
            'content_tr'=>'衣懶換，酒難賒。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b62c05c808ca40070006c68',
            'status'=>1,
            'sort'=>89,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/90.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'天仙子',
            'name_tr'=>'天仙子',
            'image_id'=>$image_id,
            'content'=>'云破月来花弄影。',
            'content_tr'=>'雲破月來花弄影。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b62c44f2f301e0039874406',
            'status'=>1,
            'sort'=>90,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/91.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'兰陵王',
            'name_tr'=>'蘭陵王',
            'image_id'=>$image_id,
            'content'=>'春去人间无路。',
            'content_tr'=>'春去人間無路。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b63fcb19f54540035b530a3',
            'status'=>1,
            'sort'=>91,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/92.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'忆王孙',
            'name_tr'=>'憶王孫',
            'image_id'=>$image_id,
            'content'=>'萋萋芳草忆王孙。',
            'content_tr'=>'萋萋芳草憶王孫。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b640269808ca4003c6711f1',
            'status'=>1,
            'sort'=>92,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/93.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桂枝香',
            'name_tr'=>'桂枝香',
            'image_id'=>$image_id,
            'content'=>'六朝旧事随流水。',
            'content_tr'=>'六朝舊事隨流水。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b6908629f54540035e95ae9',
            'status'=>1,
            'sort'=>93,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/94.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'探春令',
            'name_tr'=>'探春令',
            'image_id'=>$image_id,
            'content'=>'清歌妙舞从头按。',
            'content_tr'=>'清歌妙舞從頭按。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b6a989167f3560035992fd8',
            'status'=>1,
            'sort'=>94,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/95.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'一剪梅',
            'name_tr'=>'一剪梅',
            'image_id'=>$image_id,
            'content'=>'流光容易把人抛。',
            'content_tr'=>'流光容易把人拋。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b6aa01efe88c2005b459893',
            'status'=>1,
            'sort'=>95,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/96.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'望海潮',
            'name_tr'=>'望海潮',
            'image_id'=>$image_id,
            'content'=>'钱塘自古繁华。',
            'content_tr'=>'錢塘自古繁華。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b6bff5d9f54540035f048b3',
            'status'=>1,
            'sort'=>96,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/97.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'高阳台',
            'name_tr'=>'高陽臺',
            'image_id'=>$image_id,
            'content'=>'无心再续笙歌梦。',
            'content_tr'=>'無心再續笙歌夢。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b6c0543d50eee0031384d95',
            'status'=>1,
            'sort'=>97,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/98.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'出塞',
            'name_tr'=>'出塞',
            'image_id'=>$image_id,
            'content'=>'花谢东风扫。',
            'content_tr'=>'花謝東風掃。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b6d3dc69f54540035f8fc85',
            'status'=>1,
            'sort'=>98,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/99.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'忆旧游',
            'name_tr'=>'憶舊遊',
            'image_id'=>$image_id,
            'content'=>'山冷不生云。',
            'content_tr'=>'山冷不生雲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7141d9fe88c2005b746d51',
            'status'=>1,
            'sort'=>99,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/100.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'品令',
            'name_tr'=>'品令',
            'image_id'=>$image_id,
            'content'=>'往事总归眉际恨。',
            'content_tr'=>'往事總歸眉際恨。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b71475eee920a003b353d1d',
            'status'=>1,
            'sort'=>100,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/101.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'相见欢',
            'name_tr'=>'相見歡',
            'image_id'=>$image_id,
            'content'=>'林花谢了春红。',
            'content_tr'=>'林花謝了春紅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b72436ad50eee003163ec36',
            'status'=>1,
            'sort'=>101,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/102.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'惜分飞',
            'name_tr'=>'惜分飛',
            'image_id'=>$image_id,
            'content'=>'起看天共青山老。',
            'content_tr'=>'起看天共青山老。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b73999967f3560035d946f2',
            'status'=>1,
            'sort'=>102,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/103.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'河传',
            'name_tr'=>'河傳',
            'image_id'=>$image_id,
            'content'=>'几回邀约雁来时。',
            'content_tr'=>'幾回邀約雁來時。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b739f5efe88c2005b85be02',
            'status'=>1,
            'sort'=>103,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/104.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'玉蝴蝶',
            'name_tr'=>'玉蝴蝶',
            'image_id'=>$image_id,
            'content'=>'小桥依旧燕飞忙。',
            'content_tr'=>'小橋依舊燕飛忙。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7534f7756571003bc353bf',
            'status'=>1,
            'sort'=>104,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/105.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'解连环',
            'name_tr'=>'解連環',
            'image_id'=>$image_id,
            'content'=>'谁怜旅愁荏苒。',
            'content_tr'=>'誰憐旅愁荏苒。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b753b1167f356005f722dc7',
            'status'=>1,
            'sort'=>105,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/106.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'燕归梁',
            'name_tr'=>'燕歸梁',
            'image_id'=>$image_id,
            'content'=>'我梦唐宫春昼迟。',
            'content_tr'=>'我夢唐宮春晝遲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b767c3b9f54540031727a4a',
            'status'=>1,
            'sort'=>106,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/107.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'夜游宫',
            'name_tr'=>'夜遊宮',
            'image_id'=>$image_id,
            'content'=>'鬓虽残，心未死。',
            'content_tr'=>'鬢雖殘，心未死。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b76824f808ca40064e95b26',
            'status'=>1,
            'sort'=>107,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/108.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'御街行',
            'name_tr'=>'禦街行',
            'image_id'=>$image_id,
            'content'=>'天淡银河垂地。',
            'content_tr'=>'天淡銀河垂地。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7a8009808ca40064054fdb',
            'status'=>1,
            'sort'=>108,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/109.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'大酺',
            'name_tr'=>'大酺',
            'image_id'=>$image_id,
            'content'=>'夜游共谁秉烛。',
            'content_tr'=>'夜遊共誰秉燭。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7a8887fb4ffe0058b4121e',
            'status'=>1,
            'sort'=>109,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/110.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'木兰花令',
            'name_tr'=>'木蘭花令',
            'image_id'=>$image_id,
            'content'=>'不见莺啼花落处。',
            'content_tr'=>'不見鶯啼花落處。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7bcac1ee920a003d241a46',
            'status'=>1,
            'sort'=>110,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/111.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'女冠子',
            'name_tr'=>'女冠子',
            'image_id'=>$image_id,
            'content'=>'而今灯漫挂。',
            'content_tr'=>'而今燈漫掛。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7bd3439f54540031c4356a',
            'status'=>1,
            'sort'=>111,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/112.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'壶中天',
            'name_tr'=>'壺中天',
            'image_id'=>$image_id,
            'content'=>'月光长照歌席。',
            'content_tr'=>'月光長照歌席。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7d0b65ee920a003d2d2896',
            'status'=>1,
            'sort'=>112,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/113.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'西河',
            'name_tr'=>'西河',
            'image_id'=>$image_id,
            'content'=>'关河万里寂无烟。',
            'content_tr'=>'關河萬裏寂無煙。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7d16ae4773f7005cdfa601',
            'status'=>1,
            'sort'=>113,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/114.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'疏影',
            'name_tr'=>'疏影',
            'image_id'=>$image_id,
            'content'=>'还教一片随波去。',
            'content_tr'=>'還教一片隨波去。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7e7b4f808ca40070d4c89c',
            'status'=>1,
            'sort'=>114,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/115.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'玉漏迟',
            'name_tr'=>'玉漏遲',
            'image_id'=>$image_id,
            'content'=>'瑶台梦回人远。',
            'content_tr'=>'瑤臺夢回人遠。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7e82c1a22b9d00317b82b4',
            'status'=>1,
            'sort'=>115,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/116.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'扫花游',
            'name_tr'=>'掃花遊',
            'image_id'=>$image_id,
            'content'=>'付与朝钟暮鼓。',
            'content_tr'=>'付與朝鐘暮鼓。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7f762a9f545400319eb4b5',
            'status'=>1,
            'sort'=>116,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/117.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'宴清都',
            'name_tr'=>'宴清都',
            'image_id'=>$image_id,
            'content'=>'吟鞭又指孤店。',
            'content_tr'=>'吟鞭又指孤店。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b7faf699f54540031a0f0ab',
            'status'=>1,
            'sort'=>117,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/118.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'步蟾宫',
            'name_tr'=>'步蟾宮',
            'image_id'=>$image_id,
            'content'=>'归来沉醉月朦胧。',
            'content_tr'=>'歸來沈醉月朦朧。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b83b5ae808ca4003d7be327',
            'status'=>1,
            'sort'=>118,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/119.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'唐多令',
            'name_tr'=>'唐多令',
            'image_id'=>$image_id,
            'content'=>'欲买桂花同载酒。',
            'content_tr'=>'欲買桂花同載酒。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b850b5c570c3500382ef57c',
            'status'=>1,
            'sort'=>119,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/120.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'婆罗门令',
            'name_tr'=>'婆羅門令',
            'image_id'=>$image_id,
            'content'=>'漫天絮影飘残后。',
            'content_tr'=>'漫天絮影飄殘後。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b8510ec0b6160006211ce15',
            'status'=>1,
            'sort'=>120,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/121.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'一丛花',
            'name_tr'=>'一叢花',
            'image_id'=>$image_id,
            'content'=>'犹解嫁东风。',
            'content_tr'=>'猶解嫁東風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b8f867d9f545400319e5fe5',
            'status'=>1,
            'sort'=>121,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/122.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'天香',
            'name_tr'=>'天香',
            'image_id'=>$image_id,
            'content'=>'银河开遍碧芙蓉。',
            'content_tr'=>'銀河開遍碧芙蓉。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5b8f908c9f545400319f2758',
            'status'=>1,
            'sort'=>122,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/123.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'秋霁',
            'name_tr'=>'秋霽',
            'image_id'=>$image_id,
            'content'=>'时有惊鱼掷浪声。',
            'content_tr'=>'時有驚魚擲浪聲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bdc082867f3560062df0bc0',
            'status'=>1,
            'sort'=>123,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/124.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'太常引',
            'name_tr'=>'太常引',
            'image_id'=>$image_id,
            'content'=>'光景自长新。',
            'content_tr'=>'光景自長新。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bdc0c427565710067345075',
            'status'=>1,
            'sort'=>124,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/125.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'摊破浣溪沙',
            'name_tr'=>'攤破浣溪沙',
            'image_id'=>$image_id,
            'content'=>'西风愁起绿波间。',
            'content_tr'=>'西風愁起綠波間。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bdffc03fb4ffeff82dadcf4',
            'status'=>1,
            'sort'=>125,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/126.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'婆罗门引',
            'name_tr'=>'婆羅門引',
            'image_id'=>$image_id,
            'content'=>'一枝花影送黄昏。',
            'content_tr'=>'一枝花影送黃昏。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be004368d6d81005ea9b707',
            'status'=>1,
            'sort'=>126,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/127.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'人月圆',
            'name_tr'=>'人月圓',
            'image_id'=>$image_id,
            'content'=>'诗眼倦天涯。',
            'content_tr'=>'詩眼倦天涯。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be13f938d6d81005eba4c8a',
            'status'=>1,
            'sort'=>127,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/128.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'台城路',
            'name_tr'=>'臺城路',
            'image_id'=>$image_id,
            'content'=>'此时终是倦游了。',
            'content_tr'=>'此時終是倦遊了。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be29f838d6d81005ecaaa74',
            'status'=>1,
            'sort'=>128,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/129.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'渔歌子',
            'name_tr'=>'漁歌子',
            'image_id'=>$image_id,
            'content'=>'斜风细雨不须归。',
            'content_tr'=>'斜風細雨不須歸。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be2a4d44773f70066176777',
            'status'=>1,
            'sort'=>129,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/130.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'渡江雲',
            'name_tr'=>'渡江雲',
            'image_id'=>$image_id,
            'content'=>'明朝事与孤烟冷。',
            'content_tr'=>'明朝事與孤煙冷。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be3e8459f54540070df7f28',
            'status'=>1,
            'sort'=>130,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/131.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'真珠帘',
            'name_tr'=>'真珠簾',
            'image_id'=>$image_id,
            'content'=>'飞鸟遗音云出岫。',
            'content_tr'=>'飛鳥遺音雲出岫。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be3ee729f54540070c625f4',
            'status'=>1,
            'sort'=>131,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/132.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'贺圣朝',
            'name_tr'=>'賀聖朝',
            'image_id'=>$image_id,
            'content'=>'且高歌休诉。',
            'content_tr'=>'且高歌休訴。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be5467e8d6d81005eec8a13',
            'status'=>1,
            'sort'=>132,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/133.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'秋蕊香',
            'name_tr'=>'秋蕊香',
            'image_id'=>$image_id,
            'content'=>'云雨人间未了。',
            'content_tr'=>'雲雨人間未了。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be54b1d4773f70066393161',
            'status'=>1,
            'sort'=>133,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/134.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'宝鼎现',
            'name_tr'=>'寶鼎現',
            'image_id'=>$image_id,
            'content'=>'天上人间梦里。',
            'content_tr'=>'天上人間夢裏。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be93d384773f7006670d89b',
            'status'=>1,
            'sort'=>134,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/135.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'春光好',
            'name_tr'=>'春光好',
            'image_id'=>$image_id,
            'content'=>'红粉相随南浦晚。',
            'content_tr'=>'紅粉相隨南浦晚。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5be942d74773f70066712770',
            'status'=>1,
            'sort'=>135,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/136.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'破阵子',
            'name_tr'=>'破陣子',
            'image_id'=>$image_id,
            'content'=>'醉里挑灯看剑。',
            'content_tr'=>'醉裏挑燈看劍。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bea8ce58d6d81005e3292d2',
            'status'=>1,
            'sort'=>136,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/137.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'绛都春',
            'name_tr'=>'絳都春',
            'image_id'=>$image_id,
            'content'=>'街马冲尘东风细。',
            'content_tr'=>'街馬沖塵東風細。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bea916dfb4ffe006918d0a4',
            'status'=>1,
            'sort'=>137,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/138.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'哨遍',
            'name_tr'=>'哨遍',
            'image_id'=>$image_id,
            'content'=>'我今忘我兼忘世。',
            'content_tr'=>'我今忘我兼忘世。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bebdbbe9f54540070050ad6',
            'status'=>1,
            'sort'=>138,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/139.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'倾杯乐',
            'name_tr'=>'傾杯樂',
            'image_id'=>$image_id,
            'content'=>'何人月下临风处。',
            'content_tr'=>'何人月下臨風處。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bebdfe7756571006739f689',
            'status'=>1,
            'sort'=>139,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/140.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'二郎神',
            'name_tr'=>'二郎神',
            'image_id'=>$image_id,
            'content'=>'情远窃香年少。',
            'content_tr'=>'情遠竊香年少。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bece795fb4ffe006a4fac11',
            'status'=>1,
            'sort'=>140,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/141.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'一萼红',
            'name_tr'=>'一萼紅',
            'image_id'=>$image_id,
            'content'=>'且论笛里平生。',
            'content_tr'=>'且論笛裏平生。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5beceb2c9f54540070113de7',
            'status'=>1,
            'sort'=>141,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/142.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'南浦',
            'name_tr'=>'南浦',
            'image_id'=>$image_id,
            'content'=>'柳阴撑出扁舟小。',
            'content_tr'=>'柳陰撐出扁舟小。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bee67c58d6d81005e30063e',
            'status'=>1,
            'sort'=>142,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/143.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'多丽',
            'name_tr'=>'多麗',
            'image_id'=>$image_id,
            'content'=>'甑堕懒回头。',
            'content_tr'=>'甑墮懶回頭。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bee6e2644d9040061cf5902',
            'status'=>1,
            'sort'=>143,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/144.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'六幺令',
            'name_tr'=>'六幺令',
            'image_id'=>$image_id,
            'content'=>'细写茶经煮香雪。',
            'content_tr'=>'細寫茶經煮香雪。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf2300f8d6d810068ef67f6',
            'status'=>1,
            'sort'=>144,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/145.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'凤凰台上忆吹箫',
            'name_tr'=>'鳳凰臺上憶吹簫',
            'image_id'=>$image_id,
            'content'=>'起来慵自梳头。',
            'content_tr'=>'起來慵自梳頭。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf233bd8d6d810068ef9522',
            'status'=>1,
            'sort'=>145,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/146.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'满路花',
            'name_tr'=>'滿路花',
            'image_id'=>$image_id,
            'content'=>'三缕明霞照晚。',
            'content_tr'=>'三縷明霞照晚。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf3837544d904006106ea0a',
            'status'=>1,
            'sort'=>146,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/147.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雨中花令',
            'name_tr'=>'雨中花令',
            'image_id'=>$image_id,
            'content'=>'剪翠妆红欲就。',
            'content_tr'=>'剪翠妝紅欲就。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf3ae4f303f390062201f0b',
            'status'=>1,
            'sort'=>147,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/148.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'开元乐',
            'name_tr'=>'開元樂',
            'image_id'=>$image_id,
            'content'=>'一片红云闹处。',
            'content_tr'=>'一片紅雲鬧處。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf4c9bdfb4ffe006aa7c671',
            'status'=>1,
            'sort'=>148,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/149.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'芳草',
            'name_tr'=>'芳草',
            'image_id'=>$image_id,
            'content'=>'年年纵有春风便。',
            'content_tr'=>'年年縱有春風便。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf4ccf575657100679e180d',
            'status'=>1,
            'sort'=>149,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/150.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'绮罗香',
            'name_tr'=>'綺羅香',
            'image_id'=>$image_id,
            'content'=>'剪灯深夜语。',
            'content_tr'=>'剪燈深夜語。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf6166d756571006af6250b',
            'status'=>1,
            'sort'=>150,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/151.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'南楼令',
            'name_tr'=>'南樓令',
            'image_id'=>$image_id,
            'content'=>'正风雨下南楼。',
            'content_tr'=>'正風雨下南樓。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf619ec44d904005fcb27f7',
            'status'=>1,
            'sort'=>151,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/152.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'忆仙姿',
            'name_tr'=>'憶仙姿',
            'image_id'=>$image_id,
            'content'=>'楼外一江烟雨。',
            'content_tr'=>'樓外一江煙雨。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf75d36ee920a0068fd3c20',
            'status'=>1,
            'sort'=>152,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/153.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'百字令',
            'name_tr'=>'百字令',
            'image_id'=>$image_id,
            'content'=>'帆影摇空绿。',
            'content_tr'=>'帆影搖空綠。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bf762d567f356005f7771af',
            'status'=>1,
            'sort'=>153,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/154.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'塞翁吟',
            'name_tr'=>'塞翁吟',
            'image_id'=>$image_id,
            'content'=>'千桃过眼春如梦。',
            'content_tr'=>'千桃過眼春如夢。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bfb5fd6756571006a372671',
            'status'=>1,
            'sort'=>154,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/155.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'玲珑四犯',
            'name_tr'=>'玲瓏四犯',
            'image_id'=>$image_id,
            'content'=>'即今卧得云衣冷。',
            'content_tr'=>'即今臥得雲衣冷。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bfb65dd0b6160006752ec62',
            'status'=>1,
            'sort'=>155,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/156.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'减字浣溪沙',
            'name_tr'=>'減字浣溪沙',
            'image_id'=>$image_id,
            'content'=>'淡黄杨柳暗栖鸦。',
            'content_tr'=>'淡黃楊柳暗棲鴉。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bfcac670b61600067627330',
            'status'=>1,
            'sort'=>156,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/157.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'巫山一段云',
            'name_tr'=>'巫山一段雲',
            'image_id'=>$image_id,
            'content'=>'水声山色锁妆楼。',
            'content_tr'=>'水聲山色鎖妝樓。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bfcb1a4756571006a4757f2',
            'status'=>1,
            'sort'=>157,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/158.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'潇湘神',
            'name_tr'=>'瀟湘神',
            'image_id'=>$image_id,
            'content'=>'潇湘深夜月明时。',
            'content_tr'=>'瀟湘深夜月明時。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bfe1051ee920a006888762f',
            'status'=>1,
            'sort'=>158,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/159.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'庆清朝',
            'name_tr'=>'慶清朝',
            'image_id'=>$image_id,
            'content'=>'枝头色比舞裙同。',
            'content_tr'=>'枝頭色比舞裙同。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bff5577808ca400728d8a92',
            'status'=>1,
            'sort'=>159,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/160.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'归朝欢',
            'name_tr'=>'歸朝歡',
            'image_id'=>$image_id,
            'content'=>'岁华都瞬息。',
            'content_tr'=>'歲華都瞬息。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bff5a61fb4ffe0069aa8b3d',
            'status'=>1,
            'sort'=>160,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/161.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'竹枝',
            'name_tr'=>'竹枝',
            'image_id'=>$image_id,
            'content'=>'道是无晴却有晴。',
            'content_tr'=>'道是無晴卻有晴。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c009c9744d904005f9313eb',
            'status'=>1,
            'sort'=>161,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/162.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'相思引',
            'name_tr'=>'相思引',
            'image_id'=>$image_id,
            'content'=>'赠我不须长夜饮。',
            'content_tr'=>'贈我不須長夜飲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c00a47a9f54540066e30162',
            'status'=>1,
            'sort'=>162,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/163.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'暗香',
            'name_tr'=>'暗香',
            'image_id'=>$image_id,
            'content'=>'天际疏星趁马。',
            'content_tr'=>'天際疏星趁馬。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c049f0e67f35600660db777',
            'status'=>1,
            'sort'=>163,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/164.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'河满子',
            'name_tr'=>'河滿子',
            'image_id'=>$image_id,
            'content'=>'却爱蓝罗裙子。',
            'content_tr'=>'卻愛藍羅裙子。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c061c5f9f5454006624b61a',
            'status'=>1,
            'sort'=>164,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/165.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'菊花新',
            'name_tr'=>'菊花新',
            'image_id'=>$image_id,
            'content'=>'树色烟光相紫翠。',
            'content_tr'=>'樹色煙光相紫翠。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c06241567f35600662046de',
            'status'=>1,
            'sort'=>165,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/166.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'琐窗寒',
            'name_tr'=>'瑣窗寒',
            'image_id'=>$image_id,
            'content'=>'故人剪烛西窗语。',
            'content_tr'=>'故人剪燭西窗語。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c0734bc1579a3005f7e56d4',
            'status'=>1,
            'sort'=>166,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/brand/167.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_brands')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'倦寻芳',
            'name_tr'=>'倦尋芳',
            'image_id'=>$image_id,
            'content'=>'流水行云天四远。',
            'content_tr'=>'流水行雲天四遠。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c073a31303f39005f3ddea3',
            'status'=>1,
            'sort'=>167,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'黄鹤楼',
            'name_tr'=>'黃鶴樓',
            'image_id'=>$image_id,
            'content'=>'黄鹤一去不复返，白云千载空悠悠。',
            'content_tr'=>'黃鶴一去不復返，白雲千載空悠悠。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cac0d84a91c932819c08c05',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'西湖',
            'name_tr'=>'西湖',
            'image_id'=>$image_id,
            'content'=>'欲把西湖比西子，淡妆浓抹总相宜。',
            'content_tr'=>'欲把西湖比西子，淡妝濃抹總相宜。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cad56aba91c93158a485eec',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'洞庭湖',
            'name_tr'=>'洞庭湖',
            'image_id'=>$image_id,
            'content'=>'觉来不语到明坐，一夜洞庭湖水声。',
            'content_tr'=>'覺來不語到明坐，一夜洞庭湖水聲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5caea558c05a800073126666',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'黄河',
            'name_tr'=>'黃河',
            'image_id'=>$image_id,
            'content'=>'九曲黄河万里沙，浪淘风簸自天涯。',
            'content_tr'=>'九曲黃河萬裏沙，浪淘風簸自天涯。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5caffc82eaa375007457b615',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'平山堂',
            'name_tr'=>'平山堂',
            'image_id'=>$image_id,
            'content'=>'三过平山堂下，半生弹指声中。',
            'content_tr'=>'三過平山堂下，半生彈指聲中。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cb3e8dcc8959c0075918553',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'长江',
            'name_tr'=>'長江',
            'image_id'=>$image_id,
            'content'=>'古今多少事，都付笑谈中。',
            'content_tr'=>'古今多少事，都付笑談中。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cb68a667b968a00751b9ed6',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'长江',
            'name_tr'=>'長江',
            'image_id'=>$image_id,
            'content'=>'古今多少事，都付笑谈中。',
            'content_tr'=>'古今多少事，都付笑談中。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cb68a667b968a00751b9ed6',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/8.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'鹳雀楼',
            'name_tr'=>'鸛雀樓',
            'image_id'=>$image_id,
            'content'=>'高楼怀古动悲歌，鹳雀今无野燕过。',
            'content_tr'=>'高樓懷古動悲歌，鸛雀今無野燕過。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cb933c0a673f5006862884b',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'赤壁',
            'name_tr'=>'赤壁',
            'image_id'=>$image_id,
            'content'=>'人生如梦，一尊还酹江月。',
            'content_tr'=>'人生如夢，一尊還酹江月。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cbd3552c8959c00751ad865',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'寒山寺',
            'name_tr'=>'寒山寺',
            'image_id'=>$image_id,
            'content'=>'姑苏城外寒山寺，夜半钟声到客船。',
            'content_tr'=>'姑蘇城外寒山寺，夜半鐘聲到客船。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cb7e381c8959c0075cc52c8',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'滕王阁',
            'name_tr'=>'滕王閣',
            'image_id'=>$image_id,
            'content'=>'落霞与孤鹜齐飞，秋水共长天一色。',
            'content_tr'=>'落霞與孤鶩齊飛，秋水共長天一色。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cbd3819a673f500689db568',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/geography/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_geographys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'骊山',
            'name_tr'=>'驪山',
            'image_id'=>$image_id,
            'content'=>'至今清夜月，依前过缭墙。',
            'content_tr'=>'至今清夜月，依前過繚墻。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e8b3931974f710008ae19c5',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'黄山',
            'name_tr'=>'黃山',
            'image_id'=>$image_id,
            'content'=>'五岳归来不看山，黄山归来不看岳。',
            'content_tr'=>'五嶽歸來不看山，黃山歸來不看嶽。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5c1b4252fb4ffe005f0f4b0a',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'泰山',
            'name_tr'=>'泰山',
            'image_id'=>$image_id,
            'content'=>'造化钟神秀，阴阳割昏晓。',
            'content_tr'=>'造化鐘神秀，陰陽割昏曉。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5caff3afc8959c0073e54a51',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'衡山',
            'name_tr'=>'衡山',
            'image_id'=>$image_id,
            'content'=>'衡山苍苍入紫冥，下看南极老人星。',
            'content_tr'=>'衡山蒼蒼入紫冥，下看南極老人星。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa614cd056c0008a5e11f',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'华山',
            'name_tr'=>'華山',
            'image_id'=>$image_id,
            'content'=>'只有天在上，更无山与齐。',
            'content_tr'=>'只有天在上，更無山與齊。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa60bd322a50009d35e59',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'恒山',
            'name_tr'=>'恒山',
            'image_id'=>$image_id,
            'content'=>'恒山北临岱，秀崿东跨幽。',
            'content_tr'=>'恒山北臨岱，秀崿東跨幽。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa61899ba270008f10eb6',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'嵩山',
            'name_tr'=>'嵩山',
            'image_id'=>$image_id,
            'content'=>'此时不合人间有，尽入嵩山静夜看。',
            'content_tr'=>'此時不合人間有，盡入嵩山靜夜看。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa60666beda0006dbb6d8',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/7.jpeg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'庐山',
            'name_tr'=>'廬山',
            'image_id'=>$image_id,
            'content'=>'不识庐山真面目，只缘身在此山中。',
            'content_tr'=>'不識廬山真面目，只緣身在此山中。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cb93048a673f500686249b0',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'五台山',
            'name_tr'=>'五臺山',
            'image_id'=>$image_id,
            'content'=>'诗阁晓窗藏雪岭，画堂秋水接蓝溪。',
            'content_tr'=>'詩閣曉窗藏雪嶺，畫堂秋水接藍溪。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f865b03e8fb933539490e29',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'普陀山',
            'name_tr'=>'普陀山',
            'image_id'=>$image_id,
            'content'=>'千年普济香烟袅，百尺莲台佛像巍。',
            'content_tr'=>'千年普濟香煙裊，百尺蓮臺佛像巍。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa659d322a50009d35fe4',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'峨眉山',
            'name_tr'=>'峨眉山',
            'image_id'=>$image_id,
            'content'=>'娥眉山月半轮秋，影入平羌江水流。',
            'content_tr'=>'娥眉山月半輪秋，影入平羌江水流。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f865b175dbff53816bf3caa',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'九华山',
            'name_tr'=>'九華山',
            'image_id'=>$image_id,
            'content'=>'寺隔数峰犹未到，禅灯几点翠微明。',
            'content_tr'=>'寺隔數峰猶未到，禪燈幾點翠微明。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa654d322a50009d35fc1',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'武当山',
            'name_tr'=>'武當山',
            'image_id'=>$image_id,
            'content'=>'皇极殿中龙虎静，武当云外钟鼓清。',
            'content_tr'=>'皇極殿中龍虎靜，武當雲外鐘鼓清。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f865a7ead39200b72be7559',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'武夷山',
            'name_tr'=>'武夷山',
            'image_id'=>$image_id,
            'content'=>'溪边奇茗冠天下, 武夷仙人从古栽。',
            'content_tr'=>'溪邊奇茗冠天下, 武夷仙人從古栽。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa651d14faa00083c945a',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/14.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'终南山',
            'name_tr'=>'終南山',
            'image_id'=>$image_id,
            'content'=>'君言不得意，归卧南山陲。',
            'content_tr'=>'君言不得意，歸臥南山陲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa619b3ef60000649cf8e',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/15.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'青城山',
            'name_tr'=>'青城山',
            'image_id'=>$image_id,
            'content'=>'青城山中云茫茫，龙车问道来轩皇。',
            'content_tr'=>'青城山中雲茫茫，龍車問道來軒皇。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f865ae1f03105754381efd1',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/16.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'昆仑山',
            'name_tr'=>'昆侖山',
            'image_id'=>$image_id,
            'content'=>'昆仑山上玉楼前，五色祥光混紫烟。',
            'content_tr'=>'昆侖山上玉樓前，五色祥光混紫煙。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f865bbb2462281f9feda7cf',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/17.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'太白山',
            'name_tr'=>'太白山',
            'image_id'=>$image_id,
            'content'=>'太白与我语，为我开天关。',
            'content_tr'=>'太白與我語，為我開天關。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f865c05f03105754381f30d',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/18.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'长白山',
            'name_tr'=>'長白山',
            'image_id'=>$image_id,
            'content'=>'长白山初出，青云路欲飞。',
            'content_tr'=>'長白山初出，青雲路欲飛。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f865c6bd23a0f1e2b7be907',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/19.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'敬亭山',
            'name_tr'=>'敬亭山',
            'image_id'=>$image_id,
            'content'=>'相看两不厌，只有敬亭山。',
            'content_tr'=>'相看兩不厭，只有敬亭山。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f865d352460cb29dde728ea',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/20.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'崆峒山',
            'name_tr'=>'崆峒山',
            'image_id'=>$image_id,
            'content'=>'斗星高被众峰吞，莽荡山河剑气昏。',
            'content_tr'=>'鬥星高被眾峰吞，莽蕩山河劍氣昏。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f86601a1842b9644e3c9bd4',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/21.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'武陵源',
            'name_tr'=>'武陵源',
            'image_id'=>$image_id,
            'content'=>'仙境何处有，武陵源上寻。',
            'content_tr'=>'仙境何處有，武陵源上尋。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f866037d23a0f1e2b7bf639',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/22.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'白云山',
            'name_tr'=>'白雲山',
            'image_id'=>$image_id,
            'content'=>'白云山下春光早，少年冶游风景好。',
            'content_tr'=>'白雲山下春光早，少年冶遊風景好。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f86608e2460cb29dde7346a',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/23.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雁荡山',
            'name_tr'=>'雁蕩山',
            'image_id'=>$image_id,
            'content'=>'雁荡经行云漠漠，龙湫宴坐雨蒙蒙。',
            'content_tr'=>'雁蕩經行雲漠漠，龍湫宴坐雨蒙蒙。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8661b62462281f9fedbdad',
            'status'=>1,
            'sort'=>23,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/24.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'龙虎山',
            'name_tr'=>'龍虎山',
            'image_id'=>$image_id,
            'content'=>'细思便合从君去，龙虎山中作道人。',
            'content_tr'=>'細思便合從君去，龍虎山中作道人。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f86630aad39200b72be91f1',
            'status'=>1,
            'sort'=>24,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/famous_mountain/25.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_famous_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'齐云山',
            'name_tr'=>'齊雲山',
            'image_id'=>$image_id,
            'content'=>'齐云山与壁云齐，四顾青山座座低。',
            'content_tr'=>'齊雲山與壁雲齊，四顧青山座座低。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8663354ff95f453836b85a',
            'status'=>1,
            'sort'=>25,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'洛阳',
            'name_tr'=>'洛陽',
            'image_id'=>$image_id,
            'content'=>'洛阳城里见秋风，欲作家书意万重。',
            'content_tr'=>'洛陽城裏見秋風，欲作家書意萬重。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e4006c42a6bfd0075ec969d',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'长安',
            'name_tr'=>'長安',
            'image_id'=>$image_id,
            'content'=>'春风得意马蹄疾，一日看尽长安花。',
            'content_tr'=>'春風得意馬蹄疾，一日看盡長安花。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5cb7e172a91c9377aa51db82',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'北京',
            'name_tr'=>'北京',
            'image_id'=>$image_id,
            'content'=>'前不见古人，后不见来者。',
            'content_tr'=>'前不見古人，後不見來者。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bdab0aafb4ffe00669a0252',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'南京',
            'name_tr'=>'南京',
            'image_id'=>$image_id,
            'content'=>'江南佳丽地，金陵帝王州。',
            'content_tr'=>'江南佳麗地，金陵帝王州。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5bdab53d7565710067204ac0',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'杭州',
            'name_tr'=>'杭州',
            'image_id'=>$image_id,
            'content'=>'谁把杭州曲子讴，荷花十里桂三秋。',
            'content_tr'=>'誰把杭州曲子謳，荷花十裏桂三秋。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8683f12462281f9fee3a90',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'苏州',
            'name_tr'=>'蘇州',
            'image_id'=>$image_id,
            'content'=>'姑苏城外寒山寺，夜半钟声到客船。',
            'content_tr'=>'姑蘇城外寒山寺，夜半鐘聲到客船。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8684046e464d074d36645e',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'扬州',
            'name_tr'=>'揚州',
            'image_id'=>$image_id,
            'content'=>'故人西辞黄鹤楼，烟花三月下扬州。',
            'content_tr'=>'故人西辭黃鶴樓，煙花三月下揚州。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8684116950876a796b35a9',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'安阳',
            'name_tr'=>'安陽',
            'image_id'=>$image_id,
            'content'=>'芳草自生宫殿处，牧童谁识帝王城。',
            'content_tr'=>'芳草自生宮殿處，牧童誰識帝王城。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f86862f5fa4a03f7d05163a',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'凉州',
            'name_tr'=>'涼州',
            'image_id'=>$image_id,
            'content'=>'葡萄美酒夜光杯，欲饮琵琶马上催。',
            'content_tr'=>'葡萄美酒夜光杯，欲飲琵琶馬上催。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8bb40720e0c41dbecf53c0',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'敦煌',
            'name_tr'=>'敦煌',
            'image_id'=>$image_id,
            'content'=>'莫高千窟列鸣沙，崖壁纷披五色霞。',
            'content_tr'=>'莫高千窟列鳴沙，崖壁紛披五色霞。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8bb4cee4b49a0442e14094',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'银川',
            'name_tr'=>'銀川',
            'image_id'=>$image_id,
            'content'=>'大漠孤烟直，长河落日圆。',
            'content_tr'=>'大漠孤煙直，長河落日圓。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8bb552fefae07b8adc6eee',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'榆林',
            'name_tr'=>'榆林',
            'image_id'=>$image_id,
            'content'=>'黄龙戍上游侠儿，愁逢汉使不相识。',
            'content_tr'=>'黃龍戍上遊俠兒，愁逢漢使不相識。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f8bb528f21f8d657798a4d0',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/citys_mountain/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_citys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'山东',
            'name_tr'=>'山東',
            'image_id'=>$image_id,
            'content'=>'岱宗夫如何，齐鲁青未了。',
            'content_tr'=>'岱宗夫如何，齊魯青未了。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=606ec25a8b555e6e97ef94c3',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'正月',
            'name_tr'=>'正月',
            'image_id'=>$image_id,
            'content'=>'正月晴和风气新，纷纷已有醉游人。',
            'content_tr'=>'正月晴和風氣新，紛紛已有醉遊人。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa8cf673fde0009069d55',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'二月',
            'name_tr'=>'二月',
            'image_id'=>$image_id,
            'content'=>'新年都未有芳华，二月初惊见草芽。',
            'content_tr'=>'新年都未有芳華，二月初驚見草芽。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fab30cd056c0008a5f634',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'三月',
            'name_tr'=>'三月',
            'image_id'=>$image_id,
            'content'=>'三月残花落更开，小檐日日燕飞来。',
            'content_tr'=>'三月殘花落更開，小檐日日燕飛來。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fac3efe7b0b0008f14824',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'四月',
            'name_tr'=>'四月',
            'image_id'=>$image_id,
            'content'=>'四月清和雨乍晴，南山当户转分明。',
            'content_tr'=>'四月清和雨乍晴，南山當戶轉分明。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2faefa166d7f00081b60cf',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'五月',
            'name_tr'=>'五月',
            'image_id'=>$image_id,
            'content'=>'五月虽热麦风清，檐头索索缲车鸣。',
            'content_tr'=>'五月雖熱麥風清，檐頭索索繰車鳴。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb079cd056c0008a60d0a',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'六月',
            'name_tr'=>'六月',
            'image_id'=>$image_id,
            'content'=>'毕竟西湖六月中，风光不与四时同。',
            'content_tr'=>'畢竟西湖六月中，風光不與四時同。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb161b3ef6000064a0100',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'七月',
            'name_tr'=>'七月',
            'image_id'=>$image_id,
            'content'=>'七月七日长生殿，夜半无人私语时。',
            'content_tr'=>'七月七日長生殿，夜半無人私語時。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb22db3ef6000064a03fc',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'八月',
            'name_tr'=>'八月',
            'image_id'=>$image_id,
            'content'=>'今夜月明人尽望，不知秋思落谁家。',
            'content_tr'=>'今夜月明人盡望，不知秋思落誰家。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb3f699ba270008f148f3',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'九月',
            'name_tr'=>'九月',
            'image_id'=>$image_id,
            'content'=>'可怜九月初三夜，露似真珠月似弓。',
            'content_tr'=>'可憐九月初三夜，露似真珠月似弓。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb4a2e416aa0006af2d58',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'十月',
            'name_tr'=>'十月',
            'image_id'=>$image_id,
            'content'=>'十月江南天气好，可怜冬景似春华。',
            'content_tr'=>'十月江南天氣好，可憐冬景似春華。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb61ffe7b0b0008f173b2',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'十一月',
            'name_tr'=>'十一月',
            'image_id'=>$image_id,
            'content'=>'夜阑卧听风吹雨，铁马冰河入梦来。',
            'content_tr'=>'夜闌臥聽風吹雨，鐵馬冰河入夢來。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb711166d7f00081b8428',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/time_ms_mountain/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_time_ms_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'十二月',
            'name_tr'=>'十二月',
            'image_id'=>$image_id,
            'content'=>'吹灯窗更明，月照一天雪。',
            'content_tr'=>'吹燈窗更明，月照一天雪。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb76b94d48b0006bb8aeb',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'春',
            'name_tr'=>'春',
            'image_id'=>$image_id,
            'content'=>'夜来风雨声，花落知多少。',
            'content_tr'=>'夜來風雨聲，花落知多少。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d82e958a0054554de2',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'夏',
            'name_tr'=>'夏',
            'image_id'=>$image_id,
            'content'=>'连雨不知春去，一晴方觉夏深。',
            'content_tr'=>'連雨不知春去，一晴方覺夏深。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d8c4c971005f84e101',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'秋',
            'name_tr'=>'秋',
            'image_id'=>$image_id,
            'content'=>'自古逢秋悲寂寥，我言秋日胜春朝。',
            'content_tr'=>'自古逢秋悲寂寥，我言秋日勝春朝。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d8a34131006252a505',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'冬',
            'name_tr'=>'冬',
            'image_id'=>$image_id,
            'content'=>'柴门闻犬吠，风雪夜归人。',
            'content_tr'=>'柴門聞犬吠，風雪夜歸人。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d95bbb50005d71f9f3',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'山',
            'name_tr'=>'山',
            'image_id'=>$image_id,
            'content'=>'江碧鸟逾白，山青花欲燃。',
            'content_tr'=>'江碧鳥逾白，山青花欲燃。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81da0bd1d0005b1a8ff5',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'水',
            'name_tr'=>'水',
            'image_id'=>$image_id,
            'content'=>'枯藤老树昏鸦，小桥流水人家。',
            'content_tr'=>'枯藤老樹昏鴉，小橋流水人家。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81da5bbb50005d71f9fb',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'日',
            'name_tr'=>'日',
            'image_id'=>$image_id,
            'content'=>'大漠孤烟直，长河落日圆。',
            'content_tr'=>'大漠孤煙直，長河落日圓。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=58c66f992f301e006bc96dd9',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'月',
            'name_tr'=>'月',
            'image_id'=>$image_id,
            'content'=>'举杯邀明月，对影成三人。',
            'content_tr'=>'舉杯邀明月，對影成三人。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d9128fe10057219a79',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'风',
            'name_tr'=>'风',
            'image_id'=>$image_id,
            'content'=>'二月春风似剪刀。',
            'content_tr'=>'二月春風似剪刀。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=58b7f470ac502e006c00613c',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雲',
            'name_tr'=>'雲',
            'image_id'=>$image_id,
            'content'=>'云自无心水自闲。',
            'content_tr'=>'雲自無心水自閑。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=58b7e862a22b9d005ed09f16',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雨',
            'name_tr'=>'雨',
            'image_id'=>$image_id,
            'content'=>'好雨知时节，当春乃发生。',
            'content_tr'=>'好雨知時節，當春乃發生。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d9a22b9d006167e9fa',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雪',
            'name_tr'=>'雪',
            'image_id'=>$image_id,
            'content'=>'孤舟蓑笠翁，独钓寒江雪。',
            'content_tr'=>'孤舟蓑笠翁，獨釣寒江雪。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dbbf22ec005886892d',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'梅',
            'name_tr'=>'梅',
            'image_id'=>$image_id,
            'content'=>'零落成泥碾作尘，只有香如故。',
            'content_tr'=>'零落成泥碾作塵，只有香如故。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d9128fe10057219a76',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/14.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'兰',
            'name_tr'=>'蘭',
            'image_id'=>$image_id,
            'content'=>'幽谷自流芳，九畹风偏动。',
            'content_tr'=>'幽谷自流芳，九畹風偏動。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e4bdec41358aa006adc9647',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/15.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'竹',
            'name_tr'=>'竹',
            'image_id'=>$image_id,
            'content'=>'竹影和诗瘦，梅花入梦香。',
            'content_tr'=>'竹影和詩瘦，梅花入夢香。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5caab87dc05a800073d98245',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/16.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'菊',
            'name_tr'=>'菊',
            'image_id'=>$image_id,
            'content'=>'采菊东篱下，悠然见南山。',
            'content_tr'=>'采菊東籬下，悠然見南山。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81daa34131006252a522',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/17.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桃',
            'name_tr'=>'桃',
            'image_id'=>$image_id,
            'content'=>'桃之夭夭，灼灼其华。',
            'content_tr'=>'桃之夭夭，灼灼其華。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=58c66db3da2f605dc5ac0a5b',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/18.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'梨',
            'name_tr'=>'梨',
            'image_id'=>$image_id,
            'content'=>'带月归来仙骨冷，梦魂全不到梨花。',
            'content_tr'=>'帶月歸來仙骨冷，夢魂全不到梨花。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e8d40c43b51690006514aa8',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/19.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'荷',
            'name_tr'=>'荷',
            'image_id'=>$image_id,
            'content'=>'水面清圆，一一风荷举。',
            'content_tr'=>'水面清圓，一一風荷舉。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81da0bd1d0005b1a8ffa',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/20.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桂',
            'name_tr'=>'桂',
            'image_id'=>$image_id,
            'content'=>'暗淡轻黄体性柔，情疏迹远只香留。',
            'content_tr'=>'暗淡輕黃體性柔，情疏跡遠只香留。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e5b69e991db280076059581',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/21.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'柳',
            'name_tr'=>'柳',
            'image_id'=>$image_id,
            'content'=>'似花还似非花，也无人惜从教坠。',
            'content_tr'=>'似花還似非花，也無人惜從教墜。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e5b57648a84ab00762af780',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/22.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'松',
            'name_tr'=>'松',
            'image_id'=>$image_id,
            'content'=>'大雪压青松，青松挺且直。',
            'content_tr'=>'大雪壓青松，青松挺且直。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e4be0c6562071007732817b',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/23.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'苔',
            'name_tr'=>'苔',
            'image_id'=>$image_id,
            'content'=>'苔花如米小，也学牡丹开。',
            'content_tr'=>'苔花如米小，也學牡丹開。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e8b387221b47e006f5b26da',
            'status'=>1,
            'sort'=>23,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/24.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'草',
            'name_tr'=>'草',
            'image_id'=>$image_id,
            'content'=>'离离原上草，一岁一枯荣。',
            'content_tr'=>'離離原上草，一歲一枯榮。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e60bfda8a84ab00766ba2d4',
            'status'=>1,
            'sort'=>24,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/25.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'晷',
            'name_tr'=>'晷',
            'image_id'=>$image_id,
            'content'=>'君心不似天经纬，日日归垣定不移。',
            'content_tr'=>'君心不似天經緯，日日歸垣定不移。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa3df9ddf340006ad7eff',
            'status'=>1,
            'sort'=>25,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/26.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'星',
            'name_tr'=>'星',
            'image_id'=>$image_id,
            'content'=>'朗月并众星，日出擅其明。',
            'content_tr'=>'朗月並眾星，日出擅其明。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa3e05227870009079473',
            'status'=>1,
            'sort'=>26,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/27.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'云',
            'name_tr'=>'雲',
            'image_id'=>$image_id,
            'content'=>'浮云舒五色，玛瑙应霜天。',
            'content_tr'=>'浮雲舒五色，瑪瑙應霜天。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4165227870009079567',
            'status'=>1,
            'sort'=>27,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/28.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雷',
            'name_tr'=>'雷',
            'image_id'=>$image_id,
            'content'=>'惊雷奋兮震万里。',
            'content_tr'=>'驚雷奮兮震萬裏。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa430673fde0009068ac9',
            'status'=>1,
            'sort'=>28,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/29.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'电',
            'name_tr'=>'電',
            'image_id'=>$image_id,
            'content'=>'行过断桥沙路黑，忽从电影得前村。',
            'content_tr'=>'行過斷橋沙路黑，忽從電影得前村。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa43252278700090795f4',
            'status'=>1,
            'sort'=>29,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/30.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雹',
            'name_tr'=>'雹',
            'image_id'=>$image_id,
            'content'=>'雨脚初来杂鸣雹，雷驱电挟声沨沨。',
            'content_tr'=>'雨腳初來雜鳴雹，雷驅電挾聲沨沨。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa432d14faa00083c8c2f',
            'status'=>1,
            'sort'=>30,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/31.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'露',
            'name_tr'=>'露',
            'image_id'=>$image_id,
            'content'=>'秋荷一滴露，清夜坠玄天。',
            'content_tr'=>'秋荷一滴露，清夜墜玄天。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa434cd056c0008a5d915',
            'status'=>1,
            'sort'=>31,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/32.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'霜',
            'name_tr'=>'霜',
            'image_id'=>$image_id,
            'content'=>'蒹葭苍苍，白露为霜。',
            'content_tr'=>'蒹葭蒼蒼，白露為霜。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa43b9ddf340006ad805d',
            'status'=>1,
            'sort'=>32,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/33.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雾',
            'name_tr'=>'霧',
            'image_id'=>$image_id,
            'content'=>'涿鹿妖氛静，丹山霁色明。',
            'content_tr'=>'涿鹿妖氛靜，丹山霽色明。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa43fd14faa00083c8c70',
            'status'=>1,
            'sort'=>33,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/34.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'霞',
            'name_tr'=>'霞',
            'image_id'=>$image_id,
            'content'=>'日落西南第几峰，断霞千里抹残红。',
            'content_tr'=>'日落西南第幾峰，斷霞千裏抹殘紅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa445b3ef60000649c83e',
            'status'=>1,
            'sort'=>34,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/35.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'晴',
            'name_tr'=>'晴',
            'image_id'=>$image_id,
            'content'=>'清气朗山壑，千里遥相见。',
            'content_tr'=>'清氣朗山壑，千裏遙相見。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4495227870009079654',
            'status'=>1,
            'sort'=>35,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/36.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'岩',
            'name_tr'=>'巖',
            'image_id'=>$image_id,
            'content'=>'不知涧寺晴时雨，何似溪亭落处风。',
            'content_tr'=>'不知澗寺晴時雨，何似溪亭落處風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4749ddf340006ad8157',
            'status'=>1,
            'sort'=>36,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/37.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'洞',
            'name_tr'=>'洞',
            'image_id'=>$image_id,
            'content'=>'紫云白鹤去不返，唯有桃花溪水流。',
            'content_tr'=>'紫雲白鶴去不返，唯有桃花溪水流。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa47ad322a50009d357a0',
            'status'=>1,
            'sort'=>37,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/38.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'石',
            'name_tr'=>'石',
            'image_id'=>$image_id,
            'content'=>'烟翠三秋色，波涛万古痕。',
            'content_tr'=>'烟翠三秋色，波涛万古痕。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa481e416aa0006aee99f',
            'status'=>1,
            'sort'=>38,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/39.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'泉',
            'name_tr'=>'泉',
            'image_id'=>$image_id,
            'content'=>'出泉枯柳根，汲引岁月古。',
            'content_tr'=>'出泉枯柳根，汲引歲月古。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa48e35432a0006622277',
            'status'=>1,
            'sort'=>39,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/40.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'冰',
            'name_tr'=>'冰',
            'image_id'=>$image_id,
            'content'=>'日华照冰彩，灼烁自相明。',
            'content_tr'=>'日華照冰彩，灼爍自相明。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa49bd14faa00083c8e01',
            'status'=>1,
            'sort'=>40,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/41.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'湖',
            'name_tr'=>'湖',
            'image_id'=>$image_id,
            'content'=>'湖上春来似画图，乱峰围绕水平铺。',
            'content_tr'=>'湖上春來似畫圖，亂峰圍繞水平鋪。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa49ed322a50009d3583f',
            'status'=>1,
            'sort'=>41,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/42.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'池',
            'name_tr'=>'池',
            'image_id'=>$image_id,
            'content'=>'池开天汉分黄道，龙向天门入紫微。',
            'content_tr'=>'池開天漢分黃道，龍向天門入紫微。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4b366beda0006dbb145',
            'status'=>1,
            'sort'=>42,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/43.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'溪',
            'name_tr'=>'溪',
            'image_id'=>$image_id,
            'content'=>'言入黄花川，每逐青溪水。',
            'content_tr'=>'言入黃花川，每逐青溪水。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4ca35432a0006622380',
            'status'=>1,
            'sort'=>43,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/44.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'谷',
            'name_tr'=>'谷',
            'image_id'=>$image_id,
            'content'=>'虚名随振鹭，安得久栖林。',
            'content_tr'=>'虛名隨振鷺，安得久棲林。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4dbb3ef60000649ca8f',
            'status'=>1,
            'sort'=>44,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/45.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'涧',
            'name_tr'=>'澗',
            'image_id'=>$image_id,
            'content'=>'涧流急易转，溪竹暗难开。',
            'content_tr'=>'澗流急易轉，溪竹暗難開。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4e052278700090798c7',
            'status'=>1,
            'sort'=>45,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/46.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'海',
            'name_tr'=>'海',
            'image_id'=>$image_id,
            'content'=>'东临碣石，以观沧海。',
            'content_tr'=>'東臨碣石，以觀滄海。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4e266beda0006dbb21c',
            'status'=>1,
            'sort'=>46,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/47.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'河',
            'name_tr'=>'河',
            'image_id'=>$image_id,
            'content'=>'河洲多青草，朝暮增客愁。',
            'content_tr'=>'河洲多青草，朝暮增客愁。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4e7feb0380006823c01',
            'status'=>1,
            'sort'=>47,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/48.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'江',
            'name_tr'=>'江',
            'image_id'=>$image_id,
            'content'=>'一道残阳铺水中，半江瑟瑟半江红。',
            'content_tr'=>'一道殘陽鋪水中，半江瑟瑟半江紅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4ed99ba270008f1098c',
            'status'=>1,
            'sort'=>48,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/49.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'堤岸',
            'name_tr'=>'堤岸',
            'image_id'=>$image_id,
            'content'=>'鹊飞山月曙，蝉噪野风秋。',
            'content_tr'=>'鵲飛山月曙，蟬噪野風秋。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa4fe99ba270008f109ea',
            'status'=>1,
            'sort'=>49,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/50.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'川',
            'name_tr'=>'川',
            'image_id'=>$image_id,
            'content'=>'谁无泉石趣，朝下少同过。',
            'content_tr'=>'誰無泉石趣，朝下少同過。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa502feb0380006823c78',
            'status'=>1,
            'sort'=>50,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/51.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'峰',
            'name_tr'=>'峰',
            'image_id'=>$image_id,
            'content'=>'举手可近月，前行若无山。',
            'content_tr'=>'舉手可近月，前行若無山。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa504673fde0009068e22',
            'status'=>1,
            'sort'=>51,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/52.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'岭',
            'name_tr'=>'嶺',
            'image_id'=>$image_id,
            'content'=>'孤峰夕阳后，翠岭秋天外。',
            'content_tr'=>'孤峰夕陽後，翠嶺秋天外。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa50eb3ef60000649cb6f',
            'status'=>1,
            'sort'=>52,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/53.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'石壁',
            'name_tr'=>'石壁',
            'image_id'=>$image_id,
            'content'=>'石生铭字长，山久谷神虚。',
            'content_tr'=>'石生銘字長，山久谷神虛。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa516b3ef60000649cb8b',
            'status'=>1,
            'sort'=>53,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/54.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'渚',
            'name_tr'=>'渚',
            'image_id'=>$image_id,
            'content'=>'不夜楚帆落，避风湘渚间。',
            'content_tr'=>'不夜楚帆落，避風湘渚間。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa518e416aa0006aeebe8',
            'status'=>1,
            'sort'=>54,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/55.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'潭',
            'name_tr'=>'潭',
            'image_id'=>$image_id,
            'content'=>'潭花散清香，潭影照眉宇。',
            'content_tr'=>'潭花散清香，潭影照眉宇。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa51b166d7f00081b3505',
            'status'=>1,
            'sort'=>55,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/56.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'洲',
            'name_tr'=>'洲',
            'image_id'=>$image_id,
            'content'=>'烟开兰叶香风暖，岸夹桃花锦浪生。',
            'content_tr'=>'煙開蘭葉香風暖，岸夾桃花錦浪生。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa522d322a50009d35ab5',
            'status'=>1,
            'sort'=>56,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/57.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'瀑布',
            'name_tr'=>'瀑布',
            'image_id'=>$image_id,
            'content'=>'飞流直下三千尺，疑是银河落九天。',
            'content_tr'=>'飛流直下三千尺，疑是銀河落九天。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa52666beda0006dbb334',
            'status'=>1,
            'sort'=>57,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/58.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'农',
            'name_tr'=>'農',
            'image_id'=>$image_id,
            'content'=>'农月无闲人，倾家事南亩。',
            'content_tr'=>'農月無閑人，傾家事南畝。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa52ce416aa0006aeec34',
            'status'=>1,
            'sort'=>58,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/59.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'蚕',
            'name_tr'=>'蠶',
            'image_id'=>$image_id,
            'content'=>'妇姑相唤浴蚕去，闲着中庭栀子花。',
            'content_tr'=>'婦姑相喚浴蠶去，閑著中庭梔子花。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa550b3ef60000649cc66',
            'status'=>1,
            'sort'=>59,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/60.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'织',
            'name_tr'=>'織',
            'image_id'=>$image_id,
            'content'=>'秋深未寄衣，络纬上寒机。',
            'content_tr'=>'秋深未寄衣，絡緯上寒機。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa559b3ef60000649cc86',
            'status'=>1,
            'sort'=>60,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/61.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'牧',
            'name_tr'=>'牧',
            'image_id'=>$image_id,
            'content'=>'牧竖持蓑笠，逢人气傲然。',
            'content_tr'=>'牧豎持蓑笠，逢人氣傲然。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa560cd056c0008a5de13',
            'status'=>1,
            'sort'=>61,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/62.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'樵',
            'name_tr'=>'樵',
            'image_id'=>$image_id,
            'content'=>'上山采樵选枯树，深处樵多出辛苦。',
            'content_tr'=>'上山采樵選枯樹，深處樵多出辛苦。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa567166d7f00081b364f',
            'status'=>1,
            'sort'=>62,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/63.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'渔',
            'name_tr'=>'漁',
            'image_id'=>$image_id,
            'content'=>'枫叶落，荻花乾，醉宿渔舟不觉寒。',
            'content_tr'=>'楓葉落，荻花乾，醉宿漁舟不覺寒。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa56e9ddf340006ad85e2',
            'status'=>1,
            'sort'=>63,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/64.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'火',
            'name_tr'=>'火',
            'image_id'=>$image_id,
            'content'=>'夜烧松明火，照室红龙鸾。',
            'content_tr'=>'夜燒松明火，照室紅龍鸞。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa59e7d5d410006c7d491',
            'status'=>1,
            'sort'=>64,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/65.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'烟',
            'name_tr'=>'煙',
            'image_id'=>$image_id,
            'content'=>'翠与晴云合，轻将淑气和。',
            'content_tr'=>'翠與晴雲合，輕將淑氣和。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa5a266beda0006dbb519',
            'status'=>1,
            'sort'=>65,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/66.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'舟',
            'name_tr'=>'舟',
            'image_id'=>$image_id,
            'content'=>'水阔风高日复斜，扁舟独宿芦花里。',
            'content_tr'=>'水闊風高日復斜，扁舟獨宿蘆花裏。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa5a5673fde00090690d3',
            'status'=>1,
            'sort'=>66,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/describe_scenerys_mountain/67.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_describe_scenerys_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'车',
            'name_tr'=>'車',
            'image_id'=>$image_id,
            'content'=>'青天白日有时住，无人止得车轮声。',
            'content_tr'=>'青天白日有時住，無人止得車輪聲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa5c5673fde000906913d',
            'status'=>1,
            'sort'=>67,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'春节',
            'name_tr'=>'春節',
            'image_id'=>$image_id,
            'content'=>'千门万户曈曈日，总把新桃换旧符。',
            'content_tr'=>'千門萬戶曈曈日，總把新桃換舊符。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d5c4c971005f84e0e9',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'元宵',
            'name_tr'=>'元宵',
            'image_id'=>$image_id,
            'content'=>'火树银花合，星桥铁锁开。',
            'content_tr'=>'火樹銀花合，星橋鐵鎖開。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d65bbb50005d71f9d4',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'寒食',
            'name_tr'=>'寒食',
            'image_id'=>$image_id,
            'content'=>'春城无处不飞花，寒食东风御柳斜。',
            'content_tr'=>'春城無處不飛花，寒食東風禦柳斜。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d62e958a0054554dd7',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'清明',
            'name_tr'=>'清明',
            'image_id'=>$image_id,
            'content'=>'清明时节雨纷纷，路上行人欲断魂。',
            'content_tr'=>'清明時節雨紛紛，路上行人欲斷魂。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d6a22b9d006167e9e3',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'端午',
            'name_tr'=>'端午',
            'image_id'=>$image_id,
            'content'=>'端午临中夏，时清日复长。',
            'content_tr'=>'端午臨中夏，時清日復長。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e40e3dd90058377c39',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'七夕',
            'name_tr'=>'七夕',
            'image_id'=>$image_id,
            'content'=>'天阶夜色凉如水，坐看牵牛织女星。',
            'content_tr'=>'天階夜色涼如水，坐看牽牛織女星。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d767f3560057afc171',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'中秋',
            'name_tr'=>'中秋',
            'image_id'=>$image_id,
            'content'=>'海上生明月，天涯共此时。',
            'content_tr'=>'海上生明月，天涯共此時。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d7a22b9d006167e9e4',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'重阳',
            'name_tr'=>'重陽',
            'image_id'=>$image_id,
            'content'=>'佳节又重阳，玉枕纱厨，半夜凉初透。',
            'content_tr'=>'佳節又重陽，玉枕紗廚，半夜涼初透。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81d7da2f600060e88956',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'人日',
            'name_tr'=>'人日',
            'image_id'=>$image_id,
            'content'=>'入春才七日，离家已二年。',
            'content_tr'=>'入春才七日，離家已二年。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa7e97d5d410006c7dd56',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'花朝',
            'name_tr'=>'花朝',
            'image_id'=>$image_id,
            'content'=>'莺花世界春方半，灯火楼台月正圆。',
            'content_tr'=>'鶯花世界春方半，燈火樓臺月正圓。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb87dcd056c0008a630e8',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'上巳',
            'name_tr'=>'上巳',
            'image_id'=>$image_id,
            'content'=>'巳日帝城春，倾城祓禊辰。',
            'content_tr'=>'巳日帝城春，傾城祓禊辰。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fb93ecd056c0008a63467',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'中元',
            'name_tr'=>'中元',
            'image_id'=>$image_id,
            'content'=>'今朝道是中元节，天气过於初伏时。',
            'content_tr'=>'今朝道是中元節，天氣過於初伏時。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fbab3feb0380006829b83',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'佛日',
            'name_tr'=>'佛日',
            'image_id'=>$image_id,
            'content'=>'向来参底语，不堕有无间。',
            'content_tr'=>'向來參底語，不墮有無間。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fbb2d99ba270008f16ac7',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/14.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'社日',
            'name_tr'=>'社日',
            'image_id'=>$image_id,
            'content'=>'燕子今年掯社来，翠瓶犹有去年梅。',
            'content_tr'=>'燕子今年掯社來，翠瓶猶有去年梅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fbb5afeb0380006829e40',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/15.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'春社',
            'name_tr'=>'春社',
            'image_id'=>$image_id,
            'content'=>'桑柘影斜春社散，家家扶得醉人归。',
            'content_tr'=>'桑柘影斜春社散，家家扶得醉人歸。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fbb5e94d48b0006bb9bb3',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/festivals_mountain/16.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_festivals_mountains')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'龙抬头',
            'name_tr'=>'龍擡頭',
            'image_id'=>$image_id,
            'content'=>'二月二日新雨晴，草芽菜甲一时生。',
            'content_tr'=>'二月二日新雨晴，草芽菜甲一時生。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f851292720b12685f71d13b',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'立春',
            'name_tr'=>'立春',
            'image_id'=>$image_id,
            'content'=>'柳色早黄浅，水文新绿微。',
            'content_tr'=>'柳色早黃淺，水文新綠微。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dda22b9d006167ea2b',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'雨水',
            'name_tr'=>'雨水',
            'image_id'=>$image_id,
            'content'=>'随风潜入夜，润物细无声。',
            'content_tr'=>'隨風潛入夜，潤物細無聲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dd0bd1d0005b1a9015',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'惊蛰',
            'name_tr'=>'驚蟄',
            'image_id'=>$image_id,
            'content'=>'微雨众卉新，一雷惊蛰始。',
            'content_tr'=>'微雨眾卉新，一雷驚蟄始。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dea0bb9f00585c9416',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'春分',
            'name_tr'=>'春分',
            'image_id'=>$image_id,
            'content'=>'江南无所有，聊赠一枝春。',
            'content_tr'=>'江南無所有，聊贈一枝春。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81de128fe10057219aa7',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'清明',
            'name_tr'=>'清明',
            'image_id'=>$image_id,
            'content'=>'清明时节雨纷纷，路上行人欲断魂。',
            'content_tr'=>'清明時節雨紛紛，路上行人欲斷魂。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dea0bb9f00585c941b',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'谷雨',
            'name_tr'=>'谷雨',
            'image_id'=>$image_id,
            'content'=>'鸟弄桐花日，鱼翻谷雨萍。',
            'content_tr'=>'鳥弄桐花日，魚翻谷雨萍。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81df128fe10057219aac',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'立夏',
            'name_tr'=>'立夏',
            'image_id'=>$image_id,
            'content'=>'槐柳阴初密，帘栊暑尚微。',
            'content_tr'=>'槐柳陰初密，簾櫳暑尚微。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81df5bbb50005d71fa16',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'小满',
            'name_tr'=>'小滿',
            'image_id'=>$image_id,
            'content'=>'夜莺啼绿柳，皓月醒长空。',
            'content_tr'=>'夜鶯啼綠柳，皓月醒長空。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81dfa22b9d006167ea40',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'芒种',
            'name_tr'=>'芒種',
            'image_id'=>$image_id,
            'content'=>'家家麦饭美，处处菱歌长。',
            'content_tr'=>'家家麥飯美，處處菱歌長。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81df816dfa005eff4c93',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'夏至',
            'name_tr'=>'夏至',
            'image_id'=>$image_id,
            'content'=>'绿筠尚含粉，圆荷始散芳。',
            'content_tr'=>'綠筠尚含粉，圓荷始散芳。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e067f3560057afc1bd',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'小暑',
            'name_tr'=>'小暑',
            'image_id'=>$image_id,
            'content'=>'荷风送香气，竹露滴清响。',
            'content_tr'=>'荷風送香氣，竹露滴清響。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e0a0bb9f00585c9426',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'大暑',
            'name_tr'=>'大暑',
            'image_id'=>$image_id,
            'content'=>'清风不肯来，烈日不肯暮。',
            'content_tr'=>'清風不肯來，烈日不肯暮。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e05bbb50005d71fa2f',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'立秋',
            'name_tr'=>'立秋',
            'image_id'=>$image_id,
            'content'=>'睡起秋声无觅处，满阶梧桐月明中。',
            'content_tr'=>'睡起秋聲無覓處，滿階梧桐月明中。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e1c4c971005f84e173',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/14.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'处暑',
            'name_tr'=>'處暑',
            'image_id'=>$image_id,
            'content'=>'露蝉声渐咽，秋日景初微。',
            'content_tr'=>'露蟬聲漸咽，秋日景初微。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e1a0bb9f00585c9459',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/15.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'白露',
            'name_tr'=>'白露',
            'image_id'=>$image_id,
            'content'=>'蒹葭苍苍，白露为霜。',
            'content_tr'=>'蒹葭蒼蒼，白露為霜。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e1da2f600060e889a7',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/16.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'秋分',
            'name_tr'=>'秋分',
            'image_id'=>$image_id,
            'content'=>'秋分客尚在，竹露夕微微。',
            'content_tr'=>'秋分客尚在，竹露夕微微。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e1a0bb9f00585c947a',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/17.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'寒露',
            'name_tr'=>'寒露',
            'image_id'=>$image_id,
            'content'=>'交映凝寒露，相和起夜风。',
            'content_tr'=>'交映凝寒露，相和起夜風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e2da2f600060e889b7',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/18.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'霜降',
            'name_tr'=>'霜降',
            'image_id'=>$image_id,
            'content'=>'泊舟淮水次，霜降夕流清。',
            'content_tr'=>'泊舟淮水次，霜降夕流清。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e2816dfa005eff4d05',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/19.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'立冬',
            'name_tr'=>'立冬',
            'image_id'=>$image_id,
            'content'=>'半夜风翻屋，侵晨雪满船。',
            'content_tr'=>'半夜風翻屋，侵晨雪滿船。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e2a34131006252a58b',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/20.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'小雪',
            'name_tr'=>'小雪',
            'image_id'=>$image_id,
            'content'=>'片片互玲珑，飞扬玉漏终。',
            'content_tr'=>'片片互玲瓏，飛揚玉漏終。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e38ac247005be562f6',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/21.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'大雪',
            'name_tr'=>'大雪',
            'image_id'=>$image_id,
            'content'=>'夜深知雪重，时闻折竹声。',
            'content_tr'=>'夜深知雪重，時聞折竹聲。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e367f3560057afc1ee',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/22.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'冬至',
            'name_tr'=>'冬至',
            'image_id'=>$image_id,
            'content'=>'邯郸驿里逢冬至，抱膝灯前影伴身。',
            'content_tr'=>'邯鄲驛裏逢冬至，抱膝燈前影伴身。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e3816dfa005eff4d31',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/23.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'小寒',
            'name_tr'=>'小寒',
            'image_id'=>$image_id,
            'content'=>'东君先递玉麟香，冷蕊幽芳满。',
            'content_tr'=>'東君先遞玉麟香，冷蕊幽芳滿。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e4da2f600060e889d3',
            'status'=>1,
            'sort'=>23,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/solar_term/24.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_solar_terms')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'大寒',
            'name_tr'=>'大寒',
            'image_id'=>$image_id,
            'content'=>'阶前冻银床，檐头冰钟乳。',
            'content_tr'=>'階前凍銀床，檐頭冰鐘乳。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=57ea81e4a0bb9f00585c94ce',
            'status'=>1,
            'sort'=>24,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'樱桃',
            'name_tr'=>'櫻桃',
            'image_id'=>$image_id,
            'content'=>'樱桃落尽春归去。',
            'content_tr'=>'櫻桃落盡春歸去。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae0a8ccee920a67f6f35800',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'荔枝',
            'name_tr'=>'荔枝',
            'image_id'=>$image_id,
            'content'=>'一骑红尘妃子笑。',
            'content_tr'=>'一騎紅塵妃子笑。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae0aa191b69e619db5b0e34',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'杨梅',
            'name_tr'=>'楊梅',
            'image_id'=>$image_id,
            'content'=>'罗浮山下四时春。',
            'content_tr'=>'羅浮山下四時春。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae137149f54541f91ed39d1',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桃子',
            'name_tr'=>'桃子',
            'image_id'=>$image_id,
            'content'=>'王母仙桃子渐成。',
            'content_tr'=>'王母仙桃子漸成。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aec5585fb4ffe0068ae3bf7',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'竹笋',
            'name_tr'=>'竹筍',
            'image_id'=>$image_id,
            'content'=>'蒲笋初生竹笋肥。',
            'content_tr'=>'蒲筍初生竹筍肥。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aec4c12ac502e2f6afc766d',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'梅子',
            'name_tr'=>'梅子',
            'image_id'=>$image_id,
            'content'=>'梅子青时节。',
            'content_tr'=>'梅子青時節。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae13a89a22b9d0040253733',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'红豆',
            'name_tr'=>'紅豆',
            'image_id'=>$image_id,
            'content'=>'红豆生南国。',
            'content_tr'=>'紅豆生南國。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aec4da77f6fd3003873bd91',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'杏',
            'name_tr'=>'杏',
            'image_id'=>$image_id,
            'content'=>'杏有海东红。',
            'content_tr'=>'杏有海東紅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae14c229f545463fe56b8de',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'柿子',
            'name_tr'=>'柿子',
            'image_id'=>$image_id,
            'content'=>'柿红梨紫漫山熟。',
            'content_tr'=>'柿紅梨紫漫山熟。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae1ae9e1b69e619db5fef52',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桂圆',
            'name_tr'=>'桂圓',
            'image_id'=>$image_id,
            'content'=>'龙眼玉生津。',
            'content_tr'=>'龍眼玉生津。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aec5b0d2f301e43708b95b7',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'葡萄',
            'name_tr'=>'葡萄',
            'image_id'=>$image_id,
            'content'=>'葡萄美酒夜光杯。',
            'content_tr'=>'葡萄美酒夜光杯。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae957f6ee920a00432895a7',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'菱角',
            'name_tr'=>'菱角',
            'image_id'=>$image_id,
            'content'=>'藕花菱角满池塘。',
            'content_tr'=>'藕花菱角滿池塘。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae9594fac502e00627e5957',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'茄子',
            'name_tr'=>'茄子',
            'image_id'=>$image_id,
            'content'=>'清波引烧茄。',
            'content_tr'=>'清波引燒茄。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae14a91ee920a67f6f5a74e',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/14.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'西瓜',
            'name_tr'=>'西瓜',
            'image_id'=>$image_id,
            'content'=>'西瓜大如鼎。',
            'content_tr'=>'西瓜大如鼎。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aec53492f301e43708b5261',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/15.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'枇杷',
            'name_tr'=>'枇杷',
            'image_id'=>$image_id,
            'content'=>'枇杷花里旧知名。',
            'content_tr'=>'枇杷花裏舊知名。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ae1b2209f545405f20a9242',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/16.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'橙子',
            'name_tr'=>'橙子',
            'image_id'=>$image_id,
            'content'=>'半青橙子带香开。',
            'content_tr'=>'半青橙子帶香開。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5aec58639f54546d18da150d',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/17.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'芋',
            'name_tr'=>'芋',
            'image_id'=>$image_id,
            'content'=>'榾柮无烟雪夜长，地炉煨酒暖如汤。',
            'content_tr'=>'榾柮無煙雪夜長，地爐煨酒暖如湯。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa691e416aa0006aef1aa',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/18.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'李',
            'name_tr'=>'李',
            'image_id'=>$image_id,
            'content'=>'秾华春发彩，结实下成蹊。',
            'content_tr'=>'秾華春發彩，結實下成蹊。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa692166d7f00081b3b7f',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/19.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'梅',
            'name_tr'=>'梅',
            'image_id'=>$image_id,
            'content'=>'梅叶未藏禽，梅子青可摘。',
            'content_tr'=>'梅葉未藏禽，梅子青可摘。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6949ddf340006ad8aaf',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/20.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'梨',
            'name_tr'=>'梨',
            'image_id'=>$image_id,
            'content'=>'接枝秋转脆，含情落更香。',
            'content_tr'=>'接枝秋轉脆，含情落更香。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa697d322a50009d360c8',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/21.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'柑',
            'name_tr'=>'柑',
            'image_id'=>$image_id,
            'content'=>'绿叶萋以布，素荣芬且郁。',
            'content_tr'=>'綠葉萋以布，素榮芬且郁。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa69e99ba270008f110bb',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/22.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'橘',
            'name_tr'=>'橘',
            'image_id'=>$image_id,
            'content'=>'橘柚垂华实，乃在深山侧。',
            'content_tr'=>'橘柚垂華實，乃在深山側。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6a394d48b0006bb4220',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/23.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'金橘',
            'name_tr'=>'金橘',
            'image_id'=>$image_id,
            'content'=>'安得一株擎雨露，画图传与世人看。',
            'content_tr'=>'安得壹株擎雨露，畫圖傳與世人看。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6acfeb03800068242e7',
            'status'=>1,
            'sort'=>23,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/24.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'柰',
            'name_tr'=>'柰',
            'image_id'=>$image_id,
            'content'=>'红紫夺夏藻，芬芳掩春蕙。',
            'content_tr'=>'紅紫奪夏藻，芬芳掩春蕙。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6adcd056c0008a5e37c',
            'status'=>1,
            'sort'=>24,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/25.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'枣',
            'name_tr'=>'棗',
            'image_id'=>$image_id,
            'content'=>'在实为美果，论材又良木。',
            'content_tr'=>'在實為美果，論材又良木。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6afea0d8d0008a4b53c',
            'status'=>1,
            'sort'=>25,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/26.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'栗',
            'name_tr'=>'栗',
            'image_id'=>$image_id,
            'content'=>'山禽毛如白练带，栖我庭前栗树枝。',
            'content_tr'=>'山禽毛如白練帶，棲我庭前栗樹枝。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6b2b3ef60000649d215',
            'status'=>1,
            'sort'=>26,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/27.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'林檎',
            'name_tr'=>'林檎',
            'image_id'=>$image_id,
            'content'=>'一露一朝新，帘栊晓景分。',
            'content_tr'=>'壹露壹朝新，簾櫳曉景分。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6b535432a0006622b26',
            'status'=>1,
            'sort'=>27,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/28.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'木瓜',
            'name_tr'=>'木瓜',
            'image_id'=>$image_id,
            'content'=>'木瓜大如拳，橙橘家家悬。',
            'content_tr'=>'木瓜大如拳，橙橘家家懸。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6b735432a0006622b32',
            'status'=>1,
            'sort'=>28,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/29.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'芭蕉',
            'name_tr'=>'芭蕉',
            'image_id'=>$image_id,
            'content'=>'芭蕉为雨移，故向窗前种。',
            'content_tr'=>'芭蕉為雨移，故向窗前種。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6bdd322a50009d3613f',
            'status'=>1,
            'sort'=>29,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/30.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'瓜',
            'name_tr'=>'瓜',
            'image_id'=>$image_id,
            'content'=>'欲识东陵味，青门五色瓜。',
            'content_tr'=>'欲識東陵味，青門五色瓜。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6c399ba270008f1113c',
            'status'=>1,
            'sort'=>30,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/31.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桑椹',
            'name_tr'=>'桑椹',
            'image_id'=>$image_id,
            'content'=>'盘箸索然君勿笑，桑间紫椹正累累。',
            'content_tr'=>'盤箸索然君勿笑，桑間紫椹正累累。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6c6673fde000906952d',
            'status'=>1,
            'sort'=>31,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/32.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'瓠',
            'name_tr'=>'瓠',
            'image_id'=>$image_id,
            'content'=>'岂是阶庭物，支离亦自奇。',
            'content_tr'=>'豈是階庭物，支離亦自奇。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6c7ea0d8d0008a4b5ac',
            'status'=>1,
            'sort'=>32,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/33.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'芦菔',
            'name_tr'=>'蘆菔',
            'image_id'=>$image_id,
            'content'=>'熟登甘似芋，生荐脆如梨。',
            'content_tr'=>'熟登甘似芋，生薦脆如梨。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6c935432a0006622b7c',
            'status'=>1,
            'sort'=>33,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/34.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'橙',
            'name_tr'=>'橙',
            'image_id'=>$image_id,
            'content'=>'年好景君须记，正是橙黄橘绿时。',
            'content_tr'=>'年好景君須記，正是橙黃橘綠時。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6cb35432a0006622b84',
            'status'=>1,
            'sort'=>34,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/35.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'龙眼',
            'name_tr'=>'龍眼',
            'image_id'=>$image_id,
            'content'=>'幽株旁挺绿婆娑，啄咂虽微奈美何。',
            'content_tr'=>'幽株旁挺綠婆娑，啄咂雖微奈美何。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6cd522787000907a05d',
            'status'=>1,
            'sort'=>35,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/36.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'橄榄',
            'name_tr'=>'橄欖',
            'image_id'=>$image_id,
            'content'=>'纷纷青子落红盐，正味森森苦且严。',
            'content_tr'=>'紛紛青子落紅鹽，正味森森苦且嚴。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6cf35432a0006622b9c',
            'status'=>1,
            'sort'=>36,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/37.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'榅桲',
            'name_tr'=>'榅桲',
            'image_id'=>$image_id,
            'content'=>'秦中物专美，榅桲为嘉果。',
            'content_tr'=>'秦中物專美，榅桲為嘉果。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6d0e416aa0006aef2cf',
            'status'=>1,
            'sort'=>37,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/38.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'棠梨',
            'name_tr'=>'棠梨',
            'image_id'=>$image_id,
            'content'=>'甘棠诗所歌，自足夸众果。',
            'content_tr'=>'甘棠詩所歌，自足誇眾果。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6d1e416aa0006aef2d3',
            'status'=>1,
            'sort'=>38,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/39.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'蘋果',
            'name_tr'=>'蘋果',
            'image_id'=>$image_id,
            'content'=>'红紫夺夏藻，芬芳掩春蕙。',
            'content_tr'=>'紅紫奪夏藻，芬芳掩春蕙。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6d299ba270008f11174',
            'status'=>1,
            'sort'=>39,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/40.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'馀甘子',
            'name_tr'=>'餘甘子',
            'image_id'=>$image_id,
            'content'=>'炎方橄榄佳，馀柑岂苗裔。',
            'content_tr'=>'炎方橄欖佳，餘柑豈苗裔。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6d5d14faa00083c9635',
            'status'=>1,
            'sort'=>40,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/41.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'石榴',
            'name_tr'=>'石榴',
            'image_id'=>$image_id,
            'content'=>'榴枝婀娜榴实繁，榴膜轻明榴子鲜。',
            'content_tr'=>'榴枝婀娜榴實繁，榴膜輕明榴子鮮。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6d794d48b0006bb42e0',
            'status'=>1,
            'sort'=>41,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/42.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'榧',
            'name_tr'=>'榧',
            'image_id'=>$image_id,
            'content'=>'极知人口无正味，苦谈甘酸各矜美。',
            'content_tr'=>'極知人口無正味，苦談甘酸各矜美。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6dce416aa0006aef303',
            'status'=>1,
            'sort'=>42,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/43.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'松子',
            'name_tr'=>'松子',
            'image_id'=>$image_id,
            'content'=>'千岩玉立尽长松，半夜珠玑落雪风。',
            'content_tr'=>'千巖玉立盡長松，半夜珠璣落雪風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6df99ba270008f111a8',
            'status'=>1,
            'sort'=>43,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/44.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'核桃',
            'name_tr'=>'核桃',
            'image_id'=>$image_id,
            'content'=>'胡桃壳坚乳肉肥，香茶雀舌细叶奇。',
            'content_tr'=>'胡桃殼堅乳肉肥，香茶雀舌細葉奇。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6dfb3ef60000649d2cc',
            'status'=>1,
            'sort'=>44,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/45.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'银杏',
            'name_tr'=>'銀杏',
            'image_id'=>$image_id,
            'content'=>'风韵雍容未甚都，尊前甘橘可为奴。',
            'content_tr'=>'風韻雍容未甚都，尊前甘橘可為奴。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6e09ddf340006ad8c66',
            'status'=>1,
            'sort'=>45,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/season/46.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_seasons')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'椑柿',
            'name_tr'=>'椑柿',
            'image_id'=>$image_id,
            'content'=>'秋林黄叶晚霜严，熟蒂甘香味独兼。',
            'content_tr'=>'秋林黃葉晚霜嚴，熟蒂甘香味獨兼。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2fa6e3e416aa0006aef320',
            'status'=>1,
            'sort'=>46,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/1.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'牡丹',
            'name_tr'=>'牡丹',
            'image_id'=>$image_id,
            'content'=>'唯有牡丹真国色，花开时节动京城。',
            'content_tr'=>'唯有牡丹真國色，花開時節動京城。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7b3c522787000906f036',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/2.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'梅花',
            'name_tr'=>'梅花',
            'image_id'=>$image_id,
            'content'=>'有梅无雪不精神，有雪无诗俗了人。',
            'content_tr'=>'有梅無雪不精神，有雪無詩俗了人。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7b3c522787000906f036',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/3.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桃花',
            'name_tr'=>'桃花',
            'image_id'=>$image_id,
            'content'=>'人间四月芳菲尽, 山寺桃花始盛开。',
            'content_tr'=>'人間四月芳菲盡, 山寺桃花始盛開。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7d3cea0d8d0008a40d04',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/4.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'梨花',
            'name_tr'=>'梨花',
            'image_id'=>$image_id,
            'content'=>'梨花院落溶溶月，柳絮池塘淡淡风。',
            'content_tr'=>'梨花院落溶溶月，柳絮池塘淡淡風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7d6b66beda0006db0e9c',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/5.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'桂花',
            'name_tr'=>'桂花',
            'image_id'=>$image_id,
            'content'=>'人闲桂花落，夜静春山空。',
            'content_tr'=>'人閑桂花落，夜靜春山空。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7c63166d7f00081a9072',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'杏花',
            'name_tr'=>'杏花',
            'image_id'=>$image_id,
            'content'=>'杏花墙外一枝横，半面宫妆出晓晴。',
            'content_tr'=>'杏花墻外壹枝橫，半面宮妝出曉晴。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7d24166d7f00081a9359',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/7.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'李花',
            'name_tr'=>'李花',
            'image_id'=>$image_id,
            'content'=>'小小琼英舒嫩白，未饶深紫与轻红。',
            'content_tr'=>'小小瓊英舒嫩白，未饒深紫與輕紅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7d6535432a00066183e6',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/8.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'荷花',
            'name_tr'=>'荷花',
            'image_id'=>$image_id,
            'content'=>'接天莲叶无穷碧，映日荷花别样红。',
            'content_tr'=>'接天蓮葉無窮碧，映日荷花別樣紅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7d90166d7f00081a954a',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/9.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'菊花',
            'name_tr'=>'菊花',
            'image_id'=>$image_id,
            'content'=>'不是花中偏爱菊，此花开尽更无花。',
            'content_tr'=>'不是花中偏愛菊，此花開盡更無花。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7e68fe7b0b0008f08bad',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'海棠',
            'name_tr'=>'海棠',
            'image_id'=>$image_id,
            'content'=>'幽梦锦城西，海棠如旧时。',
            'content_tr'=>'幽夢錦城西，海棠如舊時。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7b74ea0d8d0008a40580',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/11.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'芍药',
            'name_tr'=>'芍藥',
            'image_id'=>$image_id,
            'content'=>'今日阶前红芍药，几花欲老几花新。',
            'content_tr'=>'今日階前紅芍藥，幾花欲老幾花新。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7e369ddf340006ace405',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/12.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'玉兰',
            'name_tr'=>'玉蘭',
            'image_id'=>$image_id,
            'content'=>'霓裳片片晚妆新，束素亭亭玉殿春。',
            'content_tr'=>'霓裳片片晚妝新，束素亭亭玉殿春。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7ba9e416aa0006ae4237',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/13.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'秋海棠',
            'name_tr'=>'秋海棠',
            'image_id'=>$image_id,
            'content'=>'暗中自有清香在，不是幽人不得知。',
            'content_tr'=>'暗中自有清香在，不是幽人不得知。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7b9cd322a50009d2b099',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/14.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'辛夷',
            'name_tr'=>'辛夷',
            'image_id'=>$image_id,
            'content'=>'紫粉笔含尖火焰，红胭脂染小莲花。',
            'content_tr'=>'紫粉筆含尖火焰，紅胭脂染小蓮花。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bac99ba270008f0616d',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/15.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'茉莉',
            'name_tr'=>'茉莉',
            'image_id'=>$image_id,
            'content'=>'情味于人最浓处，梦回犹觉髻边香。',
            'content_tr'=>'情味於人最濃處，夢回猶覺髻邊香。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7be6b3ef600006492391',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/16.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'紫荆',
            'name_tr'=>'紫荊',
            'image_id'=>$image_id,
            'content'=>'风吹紫荆树，色与春庭暮。',
            'content_tr'=>'風吹紫荊樹，色與春庭暮。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bb0b3ef6000064922ca',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/17.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'蜡梅',
            'name_tr'=>'蠟梅',
            'image_id'=>$image_id,
            'content'=>'墙角数枝梅，凌寒独自开。',
            'content_tr'=>'墻角數枝梅，淩寒獨自開。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bbafe7b0b0008f08043',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/18.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'月季',
            'name_tr'=>'月季',
            'image_id'=>$image_id,
            'content'=>'只道花无十日红，此花无日不春风。',
            'content_tr'=>'只道花無十日紅，此花無日不春風。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7c01cd056c0008a537a6',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/19.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'山茶',
            'name_tr'=>'山茶',
            'image_id'=>$image_id,
            'content'=>'茶树树采山坳，恍如赤霞彩云飘。',
            'content_tr'=>'茶樹樹采山坳，恍如赤霞彩雲飄。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bb166beda0006db07a2',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/20.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'紫薇',
            'name_tr'=>'紫薇',
            'image_id'=>$image_id,
            'content'=>'独坐黄昏谁是伴，紫薇花对紫微郎。',
            'content_tr'=>'獨坐黃昏誰是伴，紫薇花對紫微郎。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7b9d66beda0006db0716',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/21.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'萱花',
            'name_tr'=>'萱花',
            'image_id'=>$image_id,
            'content'=>'北堂花在亲何在，几对薰风泪湿衣。',
            'content_tr'=>'北堂花在親何在，幾對薰風淚濕衣。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7e4f94d48b0006ba9e4f',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/22.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'木槿',
            'name_tr'=>'木槿',
            'image_id'=>$image_id,
            'content'=>'夜合朝开秋露新，幽庭雅称画屏清。',
            'content_tr'=>'夜合朝開秋露新，幽庭雅稱畫屏清。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bd0cd056c0008a536f5',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/23.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'兰花',
            'name_tr'=>'蘭花',
            'image_id'=>$image_id,
            'content'=>'幽谷出幽兰，秋来花畹畹。',
            'content_tr'=>'幽谷出幽蘭，秋來花畹畹。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7c7035432a0006617f96',
            'status'=>1,
            'sort'=>23,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/24.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'凌霄',
            'name_tr'=>'淩霄',
            'image_id'=>$image_id,
            'content'=>'披云似有凌云志，向日宁无捧日心。',
            'content_tr'=>'披雲似有淩雲誌，向日寧無捧日心。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7be099ba270008f06222',
            'status'=>1,
            'sort'=>24,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/25.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'水仙',
            'name_tr'=>'水仙',
            'image_id'=>$image_id,
            'content'=>'澹墨轻和玉露香，水中仙子素衣裳。',
            'content_tr'=>'淡墨輕和玉露香，水中仙子素衣裳。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7c07ea0d8d0008a407ee',
            'status'=>1,
            'sort'=>25,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/26.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'蔷薇',
            'name_tr'=>'薔薇',
            'image_id'=>$image_id,
            'content'=>'水晶帘动微风起，满架蔷薇一院香。',
            'content_tr'=>'水晶簾動微風起，滿架薔薇壹院香。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7e0bb3ef600006492cc1',
            'status'=>1,
            'sort'=>26,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/27.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'瑞香',
            'name_tr'=>'瑞香',
            'image_id'=>$image_id,
            'content'=>'瑞香旬占深兰暖，帘外春寒都不知。',
            'content_tr'=>'瑞香旬占深蘭暖，簾外春寒都不知。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bd799ba270008f06207',
            'status'=>1,
            'sort'=>27,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/28.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'玫瑰',
            'name_tr'=>'玫瑰',
            'image_id'=>$image_id,
            'content'=>'折得玫瑰花一朵，凭君簪向凤凰钗。',
            'content_tr'=>'折得玫瑰花壹朵，憑君簪向鳳凰釵。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bf035432a0006617d70',
            'status'=>1,
            'sort'=>28,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/29.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'玉蕊',
            'name_tr'=>'玉蕊',
            'image_id'=>$image_id,
            'content'=>'蔌蔌碎金英，丝丝镂玉茎。',
            'content_tr'=>'蔌蔌碎金英，絲絲鏤玉莖。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7ba3ea0d8d0008a4066a',
            'status'=>1,
            'sort'=>29,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/30.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'岩桂',
            'name_tr'=>'巖桂',
            'image_id'=>$image_id,
            'content'=>'团团岩下桂，表表木中犀。',
            'content_tr'=>'團團巖下桂，表表木中犀。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7def6d60e70008399ada',
            'status'=>1,
            'sort'=>30,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/31.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'扶桑',
            'name_tr'=>'扶桑',
            'image_id'=>$image_id,
            'content'=>'日出扶桑一丈高，人间万事细如毛。',
            'content_tr'=>'日出扶桑壹丈高，人間萬事細如毛。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bb9feb0380006819286',
            'status'=>1,
            'sort'=>31,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/32.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'红梅',
            'name_tr'=>'紅梅',
            'image_id'=>$image_id,
            'content'=>'桃李莫相妒，夭姿元不同。',
            'content_tr'=>'桃李莫相妒，夭姿元不同。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7d1599ba270008f06751',
            'status'=>1,
            'sort'=>32,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/33.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'兰蕙',
            'name_tr'=>'蘭蕙',
            'image_id'=>$image_id,
            'content'=>'山居种兰蕙，岁寒久当知。',
            'content_tr'=>'山居種蘭蕙，歲寒久當知。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7e23d322a50009d2ba8e',
            'status'=>1,
            'sort'=>33,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/34.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'杜鹃',
            'name_tr'=>'杜鵑',
            'image_id'=>$image_id,
            'content'=>'百紫千红过了春，杜鹃声苦不堪闻。',
            'content_tr'=>'百紫千紅過了春，杜鵑聲苦不堪聞。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7dde9ddf340006ace287',
            'status'=>1,
            'sort'=>34,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/35.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'荼蘼',
            'name_tr'=>'荼蘼',
            'image_id'=>$image_id,
            'content'=>'开到荼蘼花事了，丝丝天棘出莓墙。',
            'content_tr'=>'開到荼蘼花事了，絲絲天棘出莓墻。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7bf1fe7b0b0008f08112',
            'status'=>1,
            'sort'=>35,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/36.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'杜鹃花',
            'name_tr'=>'杜鵑花',
            'image_id'=>$image_id,
            'content'=>'一园红艳醉坡陀，自地连梢簇蒨罗。',
            'content_tr'=>'壹園紅艷醉坡陀，自地連梢簇蒨羅。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7c9a7d5d410006c72ba3',
            'status'=>1,
            'sort'=>36,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/37.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'木芙蓉',
            'name_tr'=>'木芙蓉',
            'image_id'=>$image_id,
            'content'=>'木末芙蓉花，山中发红萼。',
            'content_tr'=>'木末芙蓉花，山中發紅萼。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7c52fe7b0b0008f082bb',
            'status'=>1,
            'sort'=>37,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/38.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'百合花',
            'name_tr'=>'百合花',
            'image_id'=>$image_id,
            'content'=>'孤芳寡所合，百合种何因。',
            'content_tr'=>'孤芳寡所合，百合種何因。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7e59166d7f00081a9881',
            'status'=>1,
            'sort'=>38,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/39.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'石榴花',
            'name_tr'=>'石榴花',
            'image_id'=>$image_id,
            'content'=>'五月榴花照眼明，枝间时见子初成。',
            'content_tr'=>'五月榴花照眼明，枝間時見子初成。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7d8399ba270008f0691e',
            'status'=>1,
            'sort'=>39,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/40.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'栀子花',
            'name_tr'=>'梔子花',
            'image_id'=>$image_id,
            'content'=>'晚来骤雨山头过，栀子花开满院香。',
            'content_tr'=>'晚來驟雨山頭過，梔子花開滿院香。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7dd899ba270008f06a90',
            'status'=>1,
            'sort'=>40,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/flowers_plant/41.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_flowers_plants')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'樱桃花',
            'name_tr'=>'櫻桃花',
            'image_id'=>$image_id,
            'content'=>'花砖曾立摘花人，窣破罗裙红似火。',
            'content_tr'=>'花磚曾立摘花人，窣破羅裙紅似火。',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5f2f7d7f522787000906f933',
            'status'=>1,
            'sort'=>41,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/1.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学一册',
            'name_tr'=>'小學壹冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学一年级「上册」古诗文',
            'content_tr'=>'部編版小學壹年級「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab2529975657116a39b9e1f',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/2.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学二册',
            'name_tr'=>'小學二冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学一年级「下册」古诗文',
            'content_tr'=>'部編版小學壹年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab252fc9f54540037759da6',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/3.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学三册',
            'name_tr'=>'小學三冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学二年级「上册」古诗文篇目',
            'content_tr'=>'部編版小學二年級「上冊」古詩文篇目',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab255c4a22b9d0045d796c1',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/4.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学四册',
            'name_tr'=>'小學四冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学二年级「下册」古诗文',
            'content_tr'=>'部編版小學二年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab2573c17d0096887642bbc',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/5.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学五册',
            'name_tr'=>'小學五冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学三年级「上册」古诗文',
            'content_tr'=>'部編版小學三年級「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab2575b2f301e003686bd90',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/6.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学六册',
            'name_tr'=>'小學六冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学三年级「下册」古诗文',
            'content_tr'=>'部編版小學三年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab2578aa22b9d0045d7a511',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/7.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学七册',
            'name_tr'=>'小學七冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学四年级「上册」古诗文',
            'content_tr'=>'部編版小學四年級「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab2585344d90418a2142368',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/8.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学八册',
            'name_tr'=>'小學八冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学四年级「下册」古诗文',
            'content_tr'=>'部編版小學四年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25d519f54545172649f29',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/9.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学九册',
            'name_tr'=>'小學九冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学五年级「上册」古诗文',
            'content_tr'=>'部編版小學五年級「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25dee9f54543b1898ae75',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/10.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学十册',
            'name_tr'=>'小學十冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学五年级「下册」古诗文',
            'content_tr'=>'部編版小學五年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25e0d44d90418a2145424',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/11.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学十一册',
            'name_tr'=>'小學十壹冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学六年级「上册」古诗文',
            'content_tr'=>'部編版小學六年級「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25e2d2f301e003686f263',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/12.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学十二册',
            'name_tr'=>'小學十二冊',
            'image_id'=>$image_id,
            'content'=>'部编版小学六年级「下册」古诗文',
            'content_tr'=>'部編版小學六年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25e439f5454517264a66b',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/13.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学人教版',
            'name_tr'=>'小學人教版',
            'image_id'=>$image_id,
            'content'=>'人教版小学古诗文课本',
            'content_tr'=>'人教版小學古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78db6b5620710075162642',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/14.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学苏教版',
            'name_tr'=>'小學蘇教版',
            'image_id'=>$image_id,
            'content'=>'苏教版小学古诗文课本',
            'content_tr'=>'蘇教版小學古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78dc3191db28007794d0ca',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/15.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学北师版',
            'name_tr'=>'小學北師版',
            'image_id'=>$image_id,
            'content'=>'北师版小学古诗文课本',
            'content_tr'=>'北師版小學古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78dcfe7796d90076847c4b',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/16.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学沪教版',
            'name_tr'=>'小學滬教版',
            'image_id'=>$image_id,
            'content'=>'沪教版小学古诗文课本',
            'content_tr'=>'滬教版小學古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78ddb891db28007794e5a9',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/17.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学语文版（A）',
            'name_tr'=>'小學語文版（A）',
            'image_id'=>$image_id,
            'content'=>'语文版A版小学古诗文课本',
            'content_tr'=>'語文版A版小學古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78dea25620710075164dc6',
            'status'=>1,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/18.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学语文版（S）',
            'name_tr'=>'小學語文版（S）',
            'image_id'=>$image_id,
            'content'=>'语文版S版小学古诗文课本',
            'content_tr'=>'語文版S版小學古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78dee491db28007794f3de',
            'status'=>1,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/19.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学古诗词',
            'name_tr'=>'小學古詩詞',
            'image_id'=>$image_id,
            'content'=>'部编版小学课本古诗词',
            'content_tr'=>'部編版小學課本古詩詞',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=59e584da7565714641f9abff',
            'status'=>1,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/20.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'小学文言文',
            'name_tr'=>'小學文言文',
            'image_id'=>$image_id,
            'content'=>'部编版小学文言文课本',
            'content_tr'=>'部編版小學文言文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=59e588d81b69e6004010a048',
            'status'=>1,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/21.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初一上册',
            'name_tr'=>'初壹上冊',
            'image_id'=>$image_id,
            'content'=>'部编版初中一年级「上册」古诗文',
            'content_tr'=>'部編版初中壹年級「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25f1e9f5454003775fc3f',
            'status'=>1,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/22.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初一下册',
            'name_tr'=>'初壹下冊',
            'image_id'=>$image_id,
            'content'=>'部编版初中一年级「下册」古诗文',
            'content_tr'=>'部編版初中壹年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25f4d9f54543b1898ba4c',
            'status'=>1,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/23.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初二上册',
            'name_tr'=>'初二上冊',
            'image_id'=>$image_id,
            'content'=>'部编版初中二年级「上册」古诗文',
            'content_tr'=>'部編版初中二年級「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25fd39f5454003776023a',
            'status'=>1,
            'sort'=>23,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/24.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初二下册',
            'name_tr'=>'初二下冊',
            'image_id'=>$image_id,
            'content'=>'部编版初中二年级「下册」古诗文',
            'content_tr'=>'部編版初中二年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab25ff69f5454003776033a',
            'status'=>1,
            'sort'=>24,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/25.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初三上册',
            'name_tr'=>'初三上冊',
            'image_id'=>$image_id,
            'content'=>'部编版初中三年级「上册」古诗文',
            'content_tr'=>'部編版初中三年級「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab260c62f301e003687066f',
            'status'=>1,
            'sort'=>25,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/26.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初三下册',
            'name_tr'=>'初三下冊',
            'image_id'=>$image_id,
            'content'=>'部编版初中三年级「下册」古诗文',
            'content_tr'=>'部編版初中三年級「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab260e017d0096887647450',
            'status'=>1,
            'sort'=>26,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/27.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中人教新版',
            'name_tr'=>'初中人教新版',
            'image_id'=>$image_id,
            'content'=>'人教新版初中古诗文课本',
            'content_tr'=>'人教新版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e0ad21460d006b5bc402',
            'status'=>1,
            'sort'=>27,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/28.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中苏教版',
            'name_tr'=>'初中蘇教版',
            'image_id'=>$image_id,
            'content'=>'苏教版初中古诗文课本',
            'content_tr'=>'蘇教版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e10f91db280077950af9',
            'status'=>1,
            'sort'=>28,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/29.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中北师版',
            'name_tr'=>'初中北師版',
            'image_id'=>$image_id,
            'content'=>'北师版初中古诗文课本',
            'content_tr'=>'北師版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e2131358aa007402b4aa',
            'status'=>1,
            'sort'=>29,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/30.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中人教版',
            'name_tr'=>'初中人教版',
            'image_id'=>$image_id,
            'content'=>'人教版初中古诗文课本',
            'content_tr'=>'人教版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e2652a6bfd0075ec08c3',
            'status'=>1,
            'sort'=>30,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/31.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中沪教版',
            'name_tr'=>'初中滬教版',
            'image_id'=>$image_id,
            'content'=>'沪教版初中古诗文课本',
            'content_tr'=>'滬教版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e3155620710075167cc7',
            'status'=>1,
            'sort'=>31,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/32.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中长春版',
            'name_tr'=>'初中長春版',
            'image_id'=>$image_id,
            'content'=>'长春版初中古诗文课本',
            'content_tr'=>'長春版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e36afc36ed0076670874',
            'status'=>1,
            'sort'=>32,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/33.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中鄂教版',
            'name_tr'=>'初中鄂教版',
            'image_id'=>$image_id,
            'content'=>'鄂教版初中古诗文课本',
            'content_tr'=>'鄂教版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e3d191db28007795247c',
            'status'=>1,
            'sort'=>33,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/34.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中鲁教版',
            'name_tr'=>'初中魯教版',
            'image_id'=>$image_id,
            'content'=>'鲁教版初中古诗文课本',
            'content_tr'=>'魯教版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e41c8a84ab007775e9f3',
            'status'=>1,
            'sort'=>34,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/35.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中语文版',
            'name_tr'=>'初中語文版',
            'image_id'=>$image_id,
            'content'=>'语文版初中古诗文课本',
            'content_tr'=>'語文版初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e481fc36ed0076671227',
            'status'=>1,
            'sort'=>35,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/36.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中河大版',
            'name_tr'=>'初中河大版',
            'image_id'=>$image_id,
            'content'=>'河北大学初中古诗文课本',
            'content_tr'=>'河北大學初中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e4ef5620710075168f14',
            'status'=>1,
            'sort'=>36,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/37.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中五四版',
            'name_tr'=>'初中五四版',
            'image_id'=>$image_id,
            'content'=>'人教五四版古诗文课本',
            'content_tr'=>'人教五四版古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e57f2a6bfd0075ec2595',
            'status'=>1,
            'sort'=>37,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/38.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中北京版',
            'name_tr'=>'初中北京版',
            'image_id'=>$image_id,
            'content'=>'北京课改版古诗文课本',
            'content_tr'=>'北京課改版古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e5d421460d006b5bf412',
            'status'=>1,
            'sort'=>38,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/39.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中古诗词',
            'name_tr'=>'初中古詩詞',
            'image_id'=>$image_id,
            'content'=>'部编版初中课本古诗词',
            'content_tr'=>'部編版初中課本古詩詞',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=59e58949a22b9d0061dea646',
            'status'=>1,
            'sort'=>39,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/40.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>2,
            'name'=>'初中文言文',
            'name_tr'=>'初中文言文',
            'image_id'=>$image_id,
            'content'=>'部编版初中课本文言文',
            'content_tr'=>'部編版初中課本文言文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=59e58ab8570c35088c35d0be',
            'status'=>1,
            'sort'=>40,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/41.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高一上册',
            'name_tr'=>'高壹上冊',
            'image_id'=>$image_id,
            'content'=>'部编版高一语文必修 「上册」古诗文',
            'content_tr'=>'部編版高壹語文必修 「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab260fe75657116a39c0c31',
            'status'=>1,
            'sort'=>41,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/42.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高一下册',
            'name_tr'=>'高壹下冊',
            'image_id'=>$image_id,
            'content'=>'部编版高一语文必修 「下册」古诗文',
            'content_tr'=>'部編版高壹語文必修 「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab26220ac502e57c935b720',
            'status'=>1,
            'sort'=>42,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/43.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高二上册',
            'name_tr'=>'高二上冊',
            'image_id'=>$image_id,
            'content'=>'部编版高二选择性必修「上册」古诗文',
            'content_tr'=>'部編版高二選擇性必修「上冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab26249a22b9d0045d7f800',
            'status'=>1,
            'sort'=>43,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/44.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高二下册',
            'name_tr'=>'高二下冊',
            'image_id'=>$image_id,
            'content'=>'部编版高二选择性必修「下册」古诗文',
            'content_tr'=>'部編版高二選擇性必修「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab2628fa22b9d0045d7fb73',
            'status'=>1,
            'sort'=>44,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/45.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高三上册',
            'name_tr'=>'高三上冊',
            'image_id'=>$image_id,
            'content'=>'部编版高三选择性必修「下册」古诗文',
            'content_tr'=>'部編版高三選擇性必修「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab262b59f545444f0f18c94',
            'status'=>1,
            'sort'=>45,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/46.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高三下册',
            'name_tr'=>'高三下冊',
            'image_id'=>$image_id,
            'content'=>'部编版高三选择性必修「下册」古诗文',
            'content_tr'=>'部編版高三選擇性必修「下冊」古詩文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5ab262d29f545437fe82817e',
            'status'=>1,
            'sort'=>46,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/47.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中人教版',
            'name_tr'=>'高中人教版',
            'image_id'=>$image_id,
            'content'=>'人教版高中古诗文课本',
            'content_tr'=>'人教版高中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e6621358aa007402dbc9',
            'status'=>1,
            'sort'=>47,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/48.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中北师版',
            'name_tr'=>'高中北師版',
            'image_id'=>$image_id,
            'content'=>'北师版初高古诗文课本',
            'content_tr'=>'北師版初高古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e6d791db280077953dee',
            'status'=>1,
            'sort'=>48,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/49.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中苏教版',
            'name_tr'=>'高中蘇教版',
            'image_id'=>$image_id,
            'content'=>'苏教版高中古诗文课本',
            'content_tr'=>'蘇教版高中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e71991db280077953fb9',
            'status'=>1,
            'sort'=>49,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/50.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中粤教版',
            'name_tr'=>'高中粵教版',
            'image_id'=>$image_id,
            'content'=>'粤教版高中古诗文课本',
            'content_tr'=>'粵教版高中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e7ad7796d9007684ea0f',
            'status'=>1,
            'sort'=>50,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/51.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中语文版',
            'name_tr'=>'高中語文版',
            'image_id'=>$image_id,
            'content'=>'语文版高中古诗文课本',
            'content_tr'=>'語文版高中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e8bffc36ed0076673481',
            'status'=>1,
            'sort'=>51,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/52.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中鲁人版',
            'name_tr'=>'高中魯人版',
            'image_id'=>$image_id,
            'content'=>'鲁人版高中古诗文课本',
            'content_tr'=>'魯人版高中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e93491db280077955136',
            'status'=>1,
            'sort'=>52,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/53.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中北京版',
            'name_tr'=>'高中北京版',
            'image_id'=>$image_id,
            'content'=>'北京版高中古诗文课本',
            'content_tr'=>'北京版高中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78e9897796d9007684f9c8',
            'status'=>1,
            'sort'=>53,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/54.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中华师版',
            'name_tr'=>'高中華師版',
            'image_id'=>$image_id,
            'content'=>'华师版高中古诗文课本',
            'content_tr'=>'華師版高中古詩文課本',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=5e78ea1c21460d006b5c14a8',
            'status'=>1,
            'sort'=>54,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/55.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中古诗词',
            'name_tr'=>'高中古詩詞',
            'image_id'=>$image_id,
            'content'=>'部编版高中课本古诗词',
            'content_tr'=>'部編版高中課本古詩詞',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=59e58b54128fe12e51bfac52',
            'status'=>1,
            'sort'=>55,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/static/WritingAdmin/textbook/56.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_textbooks')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'type'=>3,
            'name'=>'高中文言文',
            'name_tr'=>'高中文言文',
            'image_id'=>$image_id,
            'content'=>'部编版高中课本文言文',
            'content_tr'=>'部編版高中課本文言文',
            'baidu_wiki'=>'https://avoscloud.com/1.1/call/getWorksByCollection?collectionId=59e58b75a22b9d0061deb75c',
            'status'=>1,
            'sort'=>56,
            'created_at'=>date('Y-m-d H:i:s')
        ]);




        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'商',
            'name_tr'=>'商',
            'status'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'周',
            'name_tr'=>'周',
            'status'=>1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'秦',
            'name_tr'=>'秦',
            'status'=>1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'汉',
            'name_tr'=>'漢',
            'status'=>1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'三国',
            'name_tr'=>'三國',
            'status'=>1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'晋',
            'name_tr'=>'晉',
            'status'=>1,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'南北朝',
            'name_tr'=>'南北朝',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'隋',
            'name_tr'=>'隋',
            'status'=>1,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'唐',
            'name_tr'=>'唐',
            'status'=>1,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'五代十国',
            'name_tr'=>'五代十國',
            'status'=>1,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'宋',
            'name_tr'=>'宋',
            'status'=>1,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'金',
            'name_tr'=>'金',
            'status'=>1,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'元',
            'name_tr'=>'元',
            'status'=>1,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'明',
            'name_tr'=>'明',
            'status'=>1,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'清',
            'name_tr'=>'清',
            'status'=>1,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('writing_dynastys')->insert([
            'project_id'=>9,
            'admin_id'=>1,
            'name'=>'现代',
            'name_tr'=>'現代',
            'status'=>1,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
    }
}
