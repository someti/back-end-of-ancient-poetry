<?php

namespace Modules\WritingAdmin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class WritingTableSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /****************************古文管理******************************************/
        $pid1 = DB::table('admin_rules')->insertGetId([
            'name'=>'古文管理',
            'status'=>1,
            'auth_open'=>1,
            'path'=>'/writingAdmin',
            'pid'=>0,
            'level'=>1,
            'type'=>1,
            'sort'=>3,
            'icon'=>'Form',
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************数据看板******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'数据看板',
            'path'=>'/writingAdmin/dashboard',
            'url'=>'./writingAdmin/dashboard/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'AreaChart',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************配置管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'配置管理',
            'path'=>'/writingAdmin/setting',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Setting',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************项目配置******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'项目配置',
            'path'=>'/writingAdmin/setting/project',
            'url'=>'./writingAdmin/setting/project/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************用典管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'用典管理',
            'path'=>'/writingAdmin/setting/allusion',
            'url'=>'./writingAdmin/setting/allusion/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************选集管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'选集管理',
            'path'=>'/writingAdmin/setting/anthology',
            'url'=>'./writingAdmin/setting/anthology/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************词牌管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'词牌管理',
            'path'=>'/writingAdmin/setting/brand',
            'url'=>'./writingAdmin/setting/brand/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************城市管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'城市管理',
            'path'=>'/writingAdmin/setting/citysMountain',
            'url'=>'./writingAdmin/setting/citysMountain/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************写景管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'写景管理',
            'path'=>'/writingAdmin/setting/describeSceneryMountain',
            'url'=>'./writingAdmin/setting/describeSceneryMountain/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************朝代管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'朝代管理',
            'path'=>'/writingAdmin/setting/dynasty',
            'url'=>'./writingAdmin/setting/dynasty/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>8,
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        /****************************名山管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'名山管理',
            'path'=>'/writingAdmin/setting/famousMountain',
            'url'=>'./writingAdmin/setting/famousMountain/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>9,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************节日管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'节日管理',
            'path'=>'/writingAdmin/setting/festivalsMountain',
            'url'=>'./writingAdmin/setting/festivalsMountain/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************花卉管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'花卉管理',
            'path'=>'/writingAdmin/setting/flowersPlant',
            'url'=>'./writingAdmin/setting/flowersPlant/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>11,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************地理管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'地理管理',
            'path'=>'/writingAdmin/setting/geography',
            'url'=>'./writingAdmin/setting/geography/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>12,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************诗单管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'诗单管理',
            'path'=>'/writingAdmin/setting/poemBook',
            'url'=>'./writingAdmin/setting/poemBook/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>13,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************时令管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'时令管理',
            'path'=>'/writingAdmin/setting/season',
            'url'=>'./writingAdmin/setting/season/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>14,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************节气管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'节气管理',
            'path'=>'/writingAdmin/setting/solarTerm',
            'url'=>'./writingAdmin/setting/solarTerm/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>15,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************主题管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'主题管理',
            'path'=>'/writingAdmin/setting/theme',
            'url'=>'./writingAdmin/setting/theme/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>16,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************时间管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'时间管理',
            'path'=>'/writingAdmin/setting/timeMsMountain',
            'url'=>'./writingAdmin/setting/timeMsMountain/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>17,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************课本管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'课本管理',
            'path'=>'/writingAdmin/setting/textbook',
            'url'=>'./writingAdmin/setting/textbook/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>18,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************作品管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'作品管理',
            'path'=>'/writingAdmin/setting/work',
            'url'=>'./writingAdmin/setting/work/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>19,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************作者管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'作者管理',
            'path'=>'/writingAdmin/setting/author',
            'url'=>'./writingAdmin/setting/author/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>20,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************图片管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'图片管理',
            'path'=>'/writingAdmin/setting/picture',
            'url'=>'./writingAdmin/setting/picture/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Picture',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************通知管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'通知管理',
            'path'=>'/writingAdmin/setting/notice',
            'url'=>'./writingAdmin/setting/notice/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Picture',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>22,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************用户管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'用户管理',
            'path'=>'/writingAdmin/user',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'User',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************会员管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'会员管理',
            'path'=>'/writingAdmin/user/user',
            'url'=>'./writingAdmin/user/user/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'User',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
    }
}
