<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateWritingAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writing_authors', function (Blueprint $table) {
            $table->comment = '作者表';
            $table->increments('id')->comment('作者ID');
            $table->integer('project_id')->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->string('name')->default('')->comment('姓名');
            $table->string('name_tr')->default('')->comment('姓名繁体');
            $table->string('death_year',20)->nullable()->default('')->comment('死亡年份');
            $table->string('birth_year',20)->nullable()->default('')->comment('出生年份');
            $table->text('desc')->nullable()->comment('描述');
            $table->text('desc_tr')->nullable()->comment('描述繁体');
            $table->integer('dynasty_id')->nullable()->comment('朝代ID');
            $table->string('dynasty',20)->nullable()->default('')->comment('朝代');
            $table->string('dynasty_tr',20)->nullable()->default('')->comment('朝代繁体');
            $table->text('baidu_wiki')->nullable()->comment('抓取地址');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->integer('sort')->default(1)->comment('排序');
            $table->integer('works_count')->default(0)->comment('作品数量');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writing_authors');
    }
}
