<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateWritingPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writing_pictures', function (Blueprint $table) {
            $table->comment = '图片管理表';
            $table->increments('id')->comment('图片管理ID');
            $table->integer('project_id')->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->tinyInteger('type')->comment('图片类型');
            $table->tinyInteger('open')->comment('打开方式');
            $table->integer('image_id')->comment('图片');
            $table->longtext('content')->nullable()->comment('描述');
            $table->string('url')->nullable()->default('')->comment('链接地址');
            $table->integer('sort')->default(1)->comment('排序');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writing_pictures');
    }
}
