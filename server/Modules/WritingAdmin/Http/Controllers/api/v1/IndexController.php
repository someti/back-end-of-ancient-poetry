<?php


namespace Modules\WritingAdmin\Http\Controllers\api\v1;


use Illuminate\Http\Request;
use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Http\Requests\Common\ProjectRequest;
use Modules\WritingAdmin\Services\Notice\NoticeServices;
use Modules\WritingAdmin\Services\Picture\PictureServices;
use Modules\WritingAdmin\Services\Work\WorkServices;

class IndexController extends BaseApiControllers
{
    public function getQuoteCardList(ProjectRequest $request){
        return (new WorkServices())->getQuoteCardList($request->only([
            "project_id",
            "noIds"
        ]));
    }

    public function getPictureList(ProjectRequest $request){
        return (new PictureServices())->getPictureList($request->only([
            "project_id",
            "type"
        ]));
    }
    public function getPictureDetails(ProjectRequest $request){
        return (new PictureServices())->getPictureDetails($request->only([
            "project_id",
            "id"
        ]));
    }

    public function getNoticeList(ProjectRequest $request){
        return (new NoticeServices())->getNoticeList($request->only([
            "project_id",
            "type"
        ]));
    }
    public function getNoticeDetails(ProjectRequest $request){
        return (new NoticeServices())->getNoticeDetails($request->only([
            "project_id",
            "id"
        ]));
    }
}
