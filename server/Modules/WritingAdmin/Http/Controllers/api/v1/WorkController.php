<?php


namespace Modules\WritingAdmin\Http\Controllers\api\v1;

use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Http\Requests\Common\PageRequest;
use Modules\WritingAdmin\Http\Requests\Common\ProjectRequest;
use Modules\WritingAdmin\Services\Work\WorkServices;

class WorkController extends BaseApiControllers
{
    public function getWorkList(PageRequest $request){
        return (new WorkServices())->getWorkList($request->only([
            "project_id",
            "limit",
            "key",
            "quote_status",
            "status",
            "author_id",
            "dynasty_id",
            "anthology_id",
            "theme_id",
            "brand_id",
            "geography_id",
            "famous_mountain_id",
            "city_id",
            "time_m_id",
            "describe_scenery_id",
            "festival_id",
            "solar_term_id",
            "season_id",
            "flowers_plant_id",
            "textbook_id",
            "poem_book_id",
            "kind"
        ]));
    }

    public function getWorkDetails(ProjectRequest $request){
        return (new WorkServices())->getWorkDetails($request->only([
            "project_id",
            "id",
            "user_id"
        ]));
    }
}
