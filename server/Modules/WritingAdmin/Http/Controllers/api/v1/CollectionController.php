<?php


namespace Modules\WritingAdmin\Http\Controllers\api\v1;

use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Http\Requests\Common\UserPageRequest;
use Modules\WritingAdmin\Http\Requests\Common\UserProjectRequest;
use Modules\WritingAdmin\Services\CollectionAuthor\CollectionAuthorServices;
use Modules\WritingAdmin\Services\CollectionWork\CollectionWorkServices;

class CollectionController extends BaseApiControllers
{
    public function setWorks(UserProjectRequest $request){
        return (new CollectionWorkServices())->setWorks($request->only([
            "project_id",
            "id",
            "user_id"
        ]));
    }
    public function getWorksList(UserPageRequest $request){
        return (new CollectionWorkServices())->getWorksList($request->only([
            "project_id",
            "user_id",
            "limit",
            "textbook_id",
            "solar_term_id",
            "dynasty_id"
        ]));
    }

    public function setAuthor(UserProjectRequest $request){
        return (new CollectionAuthorServices())->setAuthor($request->only([
            "project_id",
            "id",
            "user_id"
        ]));
    }
    public function getAuthorList(UserPageRequest $request){
        return (new CollectionAuthorServices())->getAuthorList($request->only([
            "project_id",
            "user_id",
            "limit",
            "dynasty_id"
        ]));
    }
}
