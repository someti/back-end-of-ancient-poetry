<?php


namespace Modules\WritingAdmin\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Http\Requests\Common\ProjectRequest;
use Modules\WritingAdmin\Services\Project\ProjectServices;
use Modules\WritingAdmin\Services\User\MyServices;

class MyController extends BaseApiControllers
{

    public function getAboutUs(ProjectRequest $request){
        return (new ProjectServices())->getAboutUs($request->only([
            "project_id"
        ]));
    }

    public function login(Request $request){
        return (new MyServices())->login($request->only([
            "project_id",
            "code",
            "iv",
            "encryptedData",
            "pid"
        ]));
    }

    public function getUserInfoData(Request $request){
        return (new MyServices())->getUserInfoData($request->only([
            "project_id",
            "user_id"
        ]));
    }

    public function getDynamicInformation(Request $request){
        return (new MyServices())->getDynamicInformation($request->only([
            "project_id",
            "user_id"
        ]));
    }

    public function getUelQrCode(ProjectRequest $request){
        return (new MyServices())->getUelQrCode($request->only([
            "project_id",
            "page",
            "scene"
        ]));
    }
}
