<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\WritingAdmin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\WritingAdmin\Services\User\UserServices;
use Modules\Common\Requests\CommonIdArrRequest;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonStatusRequest;
use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;

class UserController extends BaseApiControllers
{
    public function index(CommonPageRequest $request)
    {
        return (new UserServices())->index($request->only([
            "limit",
            "nick_name",
            "name",
            "mobile",
            "type",
            "pid",
            "created_at",
            "updated_at",
            "status",
            "sort"
        ]));
    }
    public function edit(int $id)
    {
        return (new UserServices())->edit($id);
    }
    public function update(Request $request,int $id)
    {
        return (new UserServices())->update($id,$request->only([
            "name",
            "mobile",
            "type",
            "pid",
            "status"
        ]));
    }
    public function status(CommonStatusRequest $request,int $id){
        return (new UserServices())->status($id,$request->only([
            "status"
        ]));
    }
    public function del(int $id){
        return (new UserServices())->del($id);
    }
    public function delAll(CommonIdArrRequest $request)
    {
        return (new UserServices())->delAll($request->get('idArr'));
    }
}
