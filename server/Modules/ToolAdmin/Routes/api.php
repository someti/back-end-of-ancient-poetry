<?php


Route::group(["prefix"=>"v1/toolAdmin","middleware"=>"AdminApiAuth"],function (){
    // 项目页面
    Route::get('/project/index', 'v1\ProjectController@index');
    // 编辑提交
    Route::put('/project/update', 'v1\ProjectController@update');


    // 会员管理
    // 列表
    Route::get('/user/index', 'v1\UserController@index');

    // 编辑页面
    Route::get('/user/edit/{id}', 'v1\UserController@edit');

    // 编辑提交
    Route::put('/user/update/{id}', 'v1\UserController@update');

    // 调整状态
    Route::put('/user/status/{id}', 'v1\UserController@status');

    // 删除
    Route::delete('/user/del/{id}', 'v1\UserController@del');

    //批量删除
    Route::delete('/user/delAll', 'v1\UserController@delAll');

    // 图片管理
    // 列表
    Route::get('/picture/index', 'v1\PictureController@index');

    // 添加
    Route::post('/picture/add', 'v1\PictureController@add');

    // 编辑页面
    Route::get('/picture/edit/{id}', 'v1\PictureController@edit');

    // 编辑提交
    Route::put('/picture/update/{id}', 'v1\PictureController@update');

    // 调整状态
    Route::put('/picture/status/{id}', 'v1\PictureController@status');

    // 排序
    Route::put('/picture/sorts/{id}', 'v1\PictureController@sorts');

    // 删除
    Route::delete('/picture/del/{id}', 'v1\PictureController@del');

    //批量删除
    Route::delete('/picture/delAll', 'v1\PictureController@delAll');
});

Route::group(["prefix"=>"v1/tool"],function (){
    Route::get('/index/getQuoteCardList', 'api\v1\IndexController@getQuoteCardList');
    Route::post('/my/login', 'api\v1\MyController@login');
});
