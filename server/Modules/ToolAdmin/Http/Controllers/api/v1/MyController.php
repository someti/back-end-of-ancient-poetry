<?php


namespace Modules\ToolAdmin\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use Modules\ToolAdmin\Http\Controllers\BaseApiControllers;
use Modules\ToolAdmin\Services\User\MyServices;

class MyController extends BaseApiControllers
{

    public function login(Request $request){
        return (new MyServices())->login($request->only([
            "project_id",
            "code",
            "pid"
        ]));
    }
}
