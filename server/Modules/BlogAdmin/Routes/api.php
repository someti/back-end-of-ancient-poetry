<?php



use Illuminate\Support\Facades\Route;
Route::group(["prefix"=>"v1/blogAdmin","middleware"=>"AdminApiAuth"],function (){


    // 文章分类
    Route::get('/articleType/index', 'v1\ArticleTypeController@index');
    // 添加
    Route::post('/articleType/add', 'v1\ArticleTypeController@add');

    // 编辑页面
    Route::get('/articleType/edit/{id}', 'v1\ArticleTypeController@edit');

    // 编辑提交
    Route::put('/articleType/update/{id}', 'v1\ArticleTypeController@update');

    // 调整状态
    Route::put('/articleType/status/{id}', 'v1\ArticleTypeController@status');

    // 排序
    Route::put('/articleType/sorts/{id}', 'v1\ArticleTypeController@sorts');

    // 父级菜单列表
    Route::get('/articleType/getPidList', 'v1\ArticleTypeController@getPidList');

    // 删除
    Route::delete('/articleType/del/{id}', 'v1\ArticleTypeController@del');

    // 标签管理
    // 列表
    Route::get('/label/index', 'v1\LabelController@index');

    // 添加
    Route::post('/label/add', 'v1\LabelController@add');

    // 编辑页面
    Route::get('/label/edit/{id}', 'v1\LabelController@edit');

    // 编辑提交
    Route::put('/label/update/{id}', 'v1\LabelController@update');

    // 调整状态
    Route::put('/label/status/{id}', 'v1\LabelController@status');

    // 排序
    Route::put('/label/sorts/{id}', 'v1\LabelController@sorts');

    // 删除
    Route::delete('/label/del/{id}', 'v1\LabelController@del');

    //批量删除
    Route::delete('/label/delAll', 'v1\LabelController@delAll');



    // 文章列表
    Route::get('/article/index', 'v1\ArticleController@index');

    // 添加
    Route::post('/article/add', 'v1\ArticleController@add');

    // 编辑页面
    Route::get('/article/edit/{id}', 'v1\ArticleController@edit');

    // 编辑提交
    Route::put('/article/update/{id}', 'v1\ArticleController@update');

    // 调整状态
    Route::put('/article/status/{id}', 'v1\ArticleController@status');

    // 是否推荐
    Route::put('/article/open/{id}', 'v1\ArticleController@open');

    // 排序
    Route::put('/article/sorts/{id}', 'v1\ArticleController@sorts');

    // 删除
    Route::delete('/article/del/{id}', 'v1\ArticleController@del');

    //批量删除
    Route::delete('/article/delAll', 'v1\ArticleController@delAll');

    //获取文章分类列表
    Route::get('/article/getTypeList', 'v1\ArticleController@getTypeList');

    //获取文章标签列表
    Route::get('/article/getLabelList', 'v1\ArticleController@getLabelList');

    // 批量推送
    Route::post('/article/propellingMovementAll', 'v1\ArticleController@propellingMovementAll');


    // 友情链接
    // 列表
    Route::get('/link/index', 'v1\LinkController@index');

    // 添加
    Route::post('/link/add', 'v1\LinkController@add');

    // 编辑页面
    Route::get('/link/edit/{id}', 'v1\LinkController@edit');

    // 编辑提交
    Route::put('/link/update/{id}', 'v1\LinkController@update');

    // 调整状态
    Route::put('/link/status/{id}', 'v1\LinkController@status');

    // 排序
    Route::put('/link/sorts/{id}', 'v1\LinkController@sorts');

    // 删除
    Route::delete('/link/del/{id}', 'v1\LinkController@del');

    //批量删除
    Route::delete('/link/delAll', 'v1\LinkController@delAll');





    // 图片管理
    // 列表
    Route::get('/picture/index', 'v1\PictureController@index');

    // 添加
    Route::post('/picture/add', 'v1\PictureController@add');

    // 编辑页面
    Route::get('/picture/edit/{id}', 'v1\PictureController@edit');

    // 编辑提交
    Route::put('/picture/update/{id}', 'v1\PictureController@update');

    // 调整状态
    Route::put('/picture/status/{id}', 'v1\PictureController@status');

    // 排序
    Route::put('/picture/sorts/{id}', 'v1\PictureController@sorts');

    // 删除
    Route::delete('/picture/del/{id}', 'v1\PictureController@del');

    //批量删除
    Route::delete('/picture/delAll', 'v1\PictureController@delAll');


    // 编辑页面
    Route::get('/project/index', 'v1\ProjectController@index');

    // 编辑提交
    Route::put('/project/update', 'v1\ProjectController@update');

    // 视频抓取
    Route::post('/crawler/video', 'v1\CrawlerController@video');
});
