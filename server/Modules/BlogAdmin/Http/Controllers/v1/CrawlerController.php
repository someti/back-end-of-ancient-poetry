<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\BlogAdmin\Http\Controllers\v1;

use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\BlogAdmin\Http\Requests\Crawler\UrlRequest;
use Modules\BlogAdmin\Services\Crawler\CrawlerServices;

class CrawlerController extends BaseApiController
{
    public function video(UrlRequest $request){
        return (new CrawlerServices())->video($request->get('url'));
    }
}
