<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name  项目配置
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 17:43
 */

namespace Modules\BlogAdmin\Http\Controllers\v1;
use Illuminate\Http\Request;
use Modules\BlogAdmin\Http\Controllers\BaseApiControllers;
use Modules\Admin\Http\Requests\Project\UpdateProjectRequest;
use Modules\BlogAdmin\Services\Project\ProjectServices;
class ProjectController extends BaseApiControllers
{
    public function index(){
        return (new ProjectServices())->index();
    }
    public function update(Request $request)
    {
        return (new ProjectServices())->update($request->only([
            "name",
            "logo_id",
            "ico_id",
            "url",
            "description",
            "keywords",
            "status",
            "gzh_image_id",
            "wx_image_id",
            "about",
            "statement",
            "title",
            "icp",
            "header",
            "footer",
            "general_embody",
            "quick_embody",
            "robots",
            "network_name",
            "occupation",
            "current_residence",
            "email",
            "qq",
            "wx",
            "reprint_statement",
            "station_establishment_time",
            "website_program",
            "website_program_url",
            "baidu_propelling_movement_token",
            "baidu_propelling_movement_site",
            "baidu_statistics_url",
        ]));
    }
}
