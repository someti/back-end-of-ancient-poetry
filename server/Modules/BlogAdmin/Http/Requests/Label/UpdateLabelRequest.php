<?php

namespace Modules\BlogAdmin\Http\Requests\Label;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLabelRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|max:100',
            "sort"              =>'required|is_positive_integer',
            "status"            =>'required|is_status_integer',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                 =>'请输入标签名称',
            'name.max'                      =>'标签名称最大长度40个字符',
            'sort.required'                 =>'请输入排序',
            'sort.is_positive_integer'      =>'排序只允许为大于0的数字',
            'status.required'               =>'请选择状态',
            'status.is_status_integer'      =>'您选择的状态异常',
        ];
    }
}
