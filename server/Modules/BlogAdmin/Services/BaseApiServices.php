<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 当前模块服务基类
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:07
 */

namespace Modules\BlogAdmin\Services;


use Modules\Admin\Models\AdminProject;
use Modules\Admin\Services\Auth\TokenServices;
use Modules\Common\Services\BaseService;

class BaseApiServices extends BaseService
{
    protected function getCommonId(array $data):Array
    {
        $adminInfo = TokenServices::my();
        $data['admin_id'] = $adminInfo->id;
        $data['project_id'] = AdminProject::query()->whereIn('id',explode('|',$adminInfo->project_id))->where('type','=',1)->value('id');
        return $data;
    }

    protected function setWhereQueryProject(object $model,bool $status = false):Object
    {
        $adminInfo = TokenServices::my();
        $model = $model->where('project_id',AdminProject::query()->whereIn('id',explode('|',$adminInfo->project_id))->where('type','=',1)->value('id'));
        if($status){
            $model = $model->where('admin_id',$adminInfo->id);
        }
        return $model;
    }

    protected function getProjectId():int
    {
        $adminInfo = TokenServices::my();
        return AdminProject::query()->whereIn('id',explode('|',$adminInfo->project_id))->where('type','=',1)->value('id');
    }
}
