<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;
class CreateAuthsTable extends Migration
{
    /**
     *创建模块    php artisan module:make ToolAdmin
     *1.创建迁移文件：php artisan module:make-migration  create_tool_pictures_table ToolAdmin
     *php artisan make:migration add_images_to_articles_table --table=articles
     *2.执行迁移文件：php artisan module:migrate BlogAdmin
     *3.修改表字段：php artisan module:make-migration update_moments_table
     *4.重新执行迁移文件：php artisan module:migrate-refresh Admin
     *5.创建数据填充文件：php artisan module:make-seed  auths_table_seeder AuthAdmin
     *6.执行数据填充文件：php artisan module:seed BlogAdmin
     * php artisan module:make-request AdminRequest Admin   创建验证器
     *
     * 创建消息队列  php artisan module:make-job AfReportQueue DataAdmin
     * php artisan migrate:fresh --seed
     * php artisan module:migrate-fresh --seed Admin
     *redis-cli
     * auth 123456
     * flushall
     *php artisan queue:clear
     *php artisan queue:restart
     *php artisan optimize
     * php artisan migrate
     *
     * php artisan module:migrate  Admin
     * 添加字段
     * php artisan module:make-migration add_field_blog_articles BlogAdmin
     */
    public function up()
    {
        /**
         * 管理员表
         */
        Schema::create('admin_admins', function (Blueprint $table) {
            $table->comment = '管理员表';
            $table->increments('id')->comment('管理员ID');
            $table->string('name',100)->default('')->comment('姓名');
            $table->string('phone',100)->default('')->comment('手机号');
            $table->string('username',50)->unique()->default('')->comment('账号');
            $table->string('password')->default('')->comment('密码');
            $table->integer('group_id')->nullable()->comment('角色ID');
            $table->string('project_id')->nullable()->default('')->comment('项目ID，多个用|线隔开');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
        /**
         * 角色表
         */
        Schema::create('admin_groups', function (Blueprint $table) {
            $table->comment = '角色表';
            $table->increments('id')->comment('角色ID');
            $table->string('name',100)->unique()->default('')->comment('角色名称');
            $table->string('content',100)->nullable()->default('')->comment('角色描述');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->longtext('rules')->nullable()->comment('菜单规则多个用|隔开');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
        /**
         * 菜单表
         */
        Schema::create('admin_rules', function (Blueprint $table) {
            $table->comment = '菜单表';
            $table->increments('id')->comment('菜单ID');
            $table->string('path',100)->nullable()->default('')->comment('标识');
            $table->string('url',100)->nullable()->default('')->comment('路由文件');
            $table->string('redirect',100)->nullable()->default('')->comment('重定向路径');
            $table->string('name',100)->default('')->comment('菜单名称');
            $table->tinyInteger('type')->default(1)->comment('菜单类型:1=模块,2=目录,3=菜单');
            $table->tinyInteger('status')->default(1)->comment('侧边栏显示状态:0=隐藏,1=显示');
            $table->tinyInteger('auth_open')->default(1)->comment('是否验证权限:0=否,1=是');
            $table->tinyInteger('level')->default(1)->comment('级别');
            $table->tinyInteger('affix')->default(0)->comment('是否固定面板:0=否,1=是');
            $table->string('icon',50)->nullable()->default('')->comment('图标');
            $table->integer('pid')->default(0)->comment('父级ID');
            $table->integer('sort')->default(1)->comment('排序');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
        /**
         * 项目表
         */
        Schema::create('admin_projects', function (Blueprint $table) {
            $table->comment = '项目表';
            $table->increments('id')->comment('项目ID');
            $table->string('name',100)->unique()->default('')->comment('项目名称');
            $table->integer('logo_id')->nullable()->comment('站点logo');
            $table->integer('ico_id')->nullable()->comment('站点标识');
            $table->string('url',100)->default('')->comment('项目地址');
            $table->string('description')->default('')->comment('项目描述');
            $table->string('keywords')->default('')->comment('项目关键词');
            $table->longtext('ext')->nullable()->comment('其它信息');
            $table->tinyInteger('type')->default(1)->comment('项目类型:1=内容管理，2=企业官网，3=数据中心');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_admins');
        Schema::dropIfExists('admin_groups');
        Schema::dropIfExists('admin_rules');
        Schema::dropIfExists('admin_projects');
    }
}
