<?php

return [
    'name' => 'Admin',
    'reset_password'=>env('RESET_PASSWORD_ADMIN','123456'),
    'api_key'=>env('API_KEY_ADMIN','123456'),
    'jwt_ttl'=>env('JWT_TTL_ADMIN',60),
    'app_dev'=>env('APP_DEV',false),
];
