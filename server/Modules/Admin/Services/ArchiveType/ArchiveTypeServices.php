<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2022/1/15 16:33
 */

namespace Modules\Admin\Services\ArchiveType;

use Modules\Admin\Models\AdminArchiveType;
use Modules\Admin\Models\AdminImage;
use Modules\Admin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class ArchiveTypeServices extends BaseApiServices
{
    public function getPidList($id){
        $list = AdminArchiveType::query();
        if($id){
            $list = $list->where('id','!=',$id);
        }
        $list = $list->select('id','name','pid')->get()->toArray();
        $list = $this->tree($list);
        $list = array_merge([['value'=>0,'title'=>'默认顶级']],$list);
        return $this->apiSuccess('',$list);
    }

    public function getArchiveList($data){
        $list = AdminArchiveType::query()->where('status',1)
            ->with([
                'archiveTo'=>function($query)use($data){
                    $query->where('personnel_id',$data['personnel_id']);
                }
            ])
            ->orderBy('sort','asc')
            ->orderBy('id','desc')
            ->get()->toArray();
        foreach ($list as $k=>$v) {
            if ($v['archive_to'] && $v['archive_to']['images']) {
                $v['images'] = AdminImage::query()->whereIn('id', explode('|', $v['archive_to']['images']))->get()->toArray();
                $v['sc_description'] = $v['archive_to']['description'];
            } else {
                $v['images'] = [];
                $v['sc_description'] = '';
            }
            $list[$k] = $v;
        }
        return $this->apiSuccess('',$this->tree($list));
    }



    public function index($data){
        $model = AdminArchiveType::query();
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }else{
            $model = $model->orderBy('sort','asc');
        }
        $list = $model->orderBy('sort','asc')->orderBy('id','desc')
            ->get()->toArray();
        return $this->apiSuccess('',$this->tree($list));
    }

    public function add(array $data){
        if($data['pid'] == 0){
            $data['level'] = 1;
        }else{
            $data['level'] = AdminArchiveType::where(['id'=>$data['pid']])->value('level') + 1;
        }
        return $this->commonCreate(AdminArchiveType::query(),$data);
    }

    public function edit(int $id){
        $data = AdminArchiveType::query()->select( "id", 'name', 'description', "status", "pid", "sort")->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }

    public function update(int $id,array $data){
        return $this->commonUpdate(AdminArchiveType::query(),$id,$data);
    }

    public function status(int $id,array $data){
        return $this->commonStatusUpdate(AdminArchiveType::query(),$id,$data);
    }

    public function sorts(int $id,array $data){
        return $this->commonSortsUpdate(AdminArchiveType::query(),$id,$data);
    }

    public function del(int $id){
        $idArr = $this->getDelIdArr($id);
        return $this->commonDestroy(AdminArchiveType::query(),$idArr);
    }

    public  function getDelIdArr(int $id){
        $ruleList = AdminArchiveType::query()->select('id','pid')->get()->toArray();
        $arr = $this->delSort($ruleList,$id);
        $arr[] = $id;
        return $arr;
    }

    private function delSort(array $ruleList,int $id)
    {
        static $arr = [];
        foreach ($ruleList as $k=>$v){
            if($v['pid'] == $id){
                $arr[] = $v['id'];
                unset($ruleList[$k]);
                return $this->delSort($ruleList,$v['id']);
            }
        }
        return $arr;
    }

    public  function getIdArr(int $id){
        $list = AdminArchiveType::query()->select('id','pid')->get()->toArray();
        $arr = $this->idSort($list,$id);
        $arr[] = $id;
        return $arr;
    }

    private function idSort(array $list,int $id)
    {
        static $arr = [];
        foreach ($list as $k=>$v){
            if($v['id'] == $id){
                if($v['pid'] == 0){
                    return $arr;
                }
                $arr[] = $v['pid'];
                unset($list[$k]);
                return $this->idSort($list,$v['pid']);
            }
        }
        return $arr;
    }
}
