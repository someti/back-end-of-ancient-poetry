<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 管理员服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */

namespace Modules\Admin\Services\Admin;


use Modules\Admin\Models\AdminAdmin;
use Modules\Admin\Models\AdminProject;
use Modules\Admin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class AdminServices extends BaseApiServices
{
    public function getAdminList(){
        $list = AdminAdmin::query()->select('id','username')->orderBy('id','desc')->get()->toArray();
        return $this->apiSuccess('',$list);
    }
    public function index(array $data){
        $model = AdminAdmin::query();
        $model = $this->queryCondition($model,$data);
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }
        if (!empty($data['name'])){
            $model = $model->where('name','like','%' . $data['name'] . '%');
        }
        if (isset($data['group_id']) && is_numeric($data['group_id']) && $data['group_id'] > 0){
            $model = $model->where('group_id',$data['group_id']);
        }
        if (isset($data['project_id']) && is_numeric($data['project_id']) && $data['project_id'] > 0){
            $model = $model->where('project_id','like','%' . $data['project_id'] . '%');
        }
        $list = $model->with([
            'adminGroup'=>function ($query){
                $query->select('id','name');
            }
        ])
            ->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();
        foreach ($list['data'] as $k=>$v){
            $list['data'][$k]['project_value_arr'] = AdminProject::query()->whereIn('id',explode('|',$v['project_id']))->select('id','name')->get()->toArray();
        }
        return $this->apiSuccess('',[
            'list'=>$list['data'],
            'total'=>$list['total']
        ]);
    }


    public function add(array $data){

        $typeArr = AdminProject::query()->whereIn('id',$data['project_id'])->pluck('type')->toArray();;
        if (!count($typeArr) || (count($typeArr) != count(array_unique((array)$typeArr)))){
            $this->apiError('选择的项目中存在重复类型');
        }
        $data['password'] = bcrypt($data['password']);
        $data['project_id'] = implode('|',$data['project_id']);

        return $this->commonCreate(AdminAdmin::query(),$data);
    }

    public function edit(int $id){
        $data = AdminAdmin::query()->select( "id","name", "phone", "username",'group_id','project_id','status')->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data['project_id'] = explode('|',$data['project_id']);
        $project_id = [];
        foreach($data['project_id'] as $k=>$v){
            $project_id[] = (int)$v;
        }
        $data['project_id'] = $project_id;
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }

    public function update(int $id,array $data){
        $typeArr = AdminProject::query()->whereIn('id',$data['project_id'])->pluck('type')->toArray();
        if (!count($typeArr) || (count($typeArr) != count(array_unique($typeArr)))){
            $this->apiError('选择的项目中存在重复类型');
        }
        $data['project_id'] = implode('|',$data['project_id']);
        return $this->commonUpdate(AdminAdmin::query(),$id,$data);
    }
    public function status(int $id,array $data){
        return $this->commonStatusUpdate(AdminAdmin::query(),$id,$data);
    }

    public function resetPwd(int $id){
        $password = config('admin.reset_password');
        $data = ['password'=>bcrypt($password)];
        return $this->commonStatusUpdate(AdminAdmin::query(),$id,$data,"初始化密码为（{$password}）成功",'初始化密码失败');
    }

    public function del(int $id){
        if($id == 1){
            $this->apiError('系统管理员不可被删除');
        }
        return $this->commonDestroy(AdminAdmin::query(),['id'=>$id]);
    }
}
