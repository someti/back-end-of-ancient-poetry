<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 角色服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */

namespace Modules\Admin\Services\Rule;


use Modules\Admin\Models\AdminGroup;
use Modules\Admin\Models\AdminRule;
use Modules\Admin\Services\Auth\TokenServices;
use Modules\Admin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class RuleServices extends BaseApiServices
{
    public function access($id){
        $list = AdminRule::query()->select('id','name','pid')->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();
        $rules = AdminGroup::query()->where('id','=',$id)->value('rules');
        $rules = explode('|',$rules);
        $rules = AdminRule::query()->whereIn('id',$rules)->where('type','=',3)->pluck('id');
        if(count($rules)){
            $rules = $rules->toArray();
            $rules = explode(',',implode(',',$rules));
        }
        return $this->apiSuccess('',[
            'list'=>$this->tree($list),
            'rules'=>$rules
        ]);
    }
    public function getMenuList(){
        $userInfo = (new TokenServices())->my();
        $list = AdminRule::query();
        if($userInfo->group_id != 1){
            $rules = AdminGroup::query()->where('id','=',$userInfo->group_id)->value('rules');
            $rules = explode('|',$rules);
            $list = $list->whereIn('id',$rules);
        }
        $list = $list->select('id','name','path','url as component','icon','redirect','pid')->orderBy('sort','asc')->orderBy('id','desc')->where('status','=',1)->get()->toArray();
        return $this->apiSuccess('',$this->getMenus($list));
    }
    private function getMenus(array $array,$pid = 0)
    {
        $arr = array();
        foreach ($array as $key => $value) {
            if ($value['pid'] == $pid) {
                $value['routes'] = $this->getMenus($array, $value['id']);
                if (!$value['routes']) {
                    unset($value['routes']);
                }
                $arr[] = $value;
            }
        }
        return $arr;
    }
    public function getPidList($id){
		$list = AdminRule::query();
		if($id){
			$list = $list->where('id','!=',$id);
		}
        $list = $list->select('id','name','pid')->get()->toArray();
		$list = $this->tree($list);
		$list = array_merge([['value'=>0,'title'=>'默认顶级菜单']],$list);
        return $this->apiSuccess('',$list);
    }
    public function index($data){
        $model = AdminRule::query();
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }else{
            $model = $model->orderBy('sort','asc');
        }
        $list = $model->orderBy('sort','asc')->orderBy('id','desc')
            ->get()->toArray();
        return $this->apiSuccess('',$this->tree($list));
    }

    public function add(array $data){
		if($data['pid'] == 0){
			$data['level'] = 1;
		}else{
			$data['level'] = AdminRule::where(['id'=>$data['pid']])->value('level') + 1;
		}
        return $this->commonCreate(AdminRule::query(),$data);
    }

    public function edit(int $id){
        $data = AdminRule::query()->select( "id", 'path', 'url', 'redirect', 'name', 'type', 'icon', "affix", "pid", "sort", "auth_open", "status",)->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }

    public function update(int $id,array $data){
        return $this->commonUpdate(AdminRule::query(),$id,$data);
    }

    public function status(int $id,array $data){
        return $this->commonStatusUpdate(AdminRule::query(),$id,$data);
    }

    public function sorts(int $id,array $data){
        return $this->commonSortsUpdate(AdminRule::query(),$id,$data);
    }

    public function del(int $id){
        $idArr = $this->getDelIdArr($id);
        return $this->commonDestroy(AdminRule::query(),$idArr);
    }

    private function getDelIdArr(int $id){
        $ruleList = AdminRule::query()->select('id','pid')->get()->toArray();
        $arr = $this->delSort($ruleList,$id);
        $arr[] = $id;
        return $arr;
    }

    private function delSort(array $ruleList,int $id)
    {
        static $arr = [];
        foreach ($ruleList as $k=>$v){
            if($v['pid'] == $id){
                $arr[] = $v['id'];
                unset($ruleList[$k]);
                return $this->delSort($ruleList,$v['id']);
            }
        }
        return $arr;
    }
}
