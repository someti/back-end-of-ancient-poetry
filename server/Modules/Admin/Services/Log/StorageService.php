<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 日志文件管理
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/6/23 16:54
 */

namespace Modules\Admin\Services\Log;
use Illuminate\Support\Facades\Log;
use Modules\Admin\Services\BaseApiServices;
use Illuminate\Support\Facades\Storage;
class StorageService extends BaseApiServices
{
    public function index(){
        $fileList = Storage::disk('logs')->files();
        $fileArray = array();
        $http = self::getHttp(1);
        $total = 0;
        foreach ($fileList  as $i => $file) {
            $size = Storage::disk('logs')->size($file);
            $fileArray[] = array(
                'name' => $file,
                'date' => date('Y-m-d H:i:s',Storage::disk('logs')->lastModified($file)),
                'size' => self::formatBytes($size),
                'url'=>$http . '/' . Storage::disk('logs')->url($file)
            );
            $total += $size;
        }
        return $this->apiSuccess('',[
            'data'=>$fileArray,
            'tableNum'=>count($fileArray),
            'total'=>self::formatBytes($total)
        ]);
    }
    public function getFiles(string $name)
    {
        $info = Storage::disk('logs')->get($name);
        if($info){
            return $this->apiSuccess('',[
                'content'=>$info
            ]);
        }
        $this->apiError();
    }
    public function delSqlFiles(array $nameArr)
    {
        if(Storage::disk('logs')->delete($nameArr)){
            return $this->apiSuccess('删除文件成功！');
        }
        $this->apiError();
    }
}
