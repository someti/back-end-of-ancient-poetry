<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name  日志记录服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/6/23 10:18
 */

namespace Modules\Admin\Services\Log;


use Modules\Admin\Models\AdminGroup;
use Modules\Admin\Models\AdminOperationLog;
use Modules\Admin\Models\AdminRule;
use Modules\Admin\Services\BaseApiServices;

class OperationLogService extends BaseApiServices
{
    public function store(object $adminInfo,string $url):int
    {
        $pathname = request()->header('pathname');
        $pathname = '/' . implode('/',array_filter(explode('/',$pathname)));
        $method = request()->getMethod();
        if(in_array($pathname,
            [
                '/login',
                '/writingAdmin/dashboard'
            ]
        )){
            return 1;
        }
        if(config('admin.app_dev') && $method != 'GET'){
            return 2;
        }
        $AdminRuleModel = AdminRule::query();
        if($adminInfo->group_id != 1){
            $rules = AdminGroup::query()->where('id','=',$adminInfo->group_id)->value('rules');
            $rules = explode('|',$rules);
            $AdminRuleModel = $AdminRuleModel->whereIn('id',$rules);
        }
        $ruleName = $AdminRuleModel->where([['auth_open','=',1],['path','=',$pathname]])->value('name');
        if(!$ruleName){
            return 3;
        }
//        $data = [
//            'name'=>$ruleName,
//            'url'=>$url,
//            'pathname'=>$pathname,
//            'method'=>$method,
//            'ip'=>self::getClientIp(),
////            'ip'=>request()->ip(),
//            'admin_id'=>$adminInfo->id,
//            'data'=>json_encode(request()->all()),
//            'header'=>json_encode(request()->header())
//        ];
//        $this->commonCreate(AdminOperationLog::query(),$data);
        return 1;
    }
    public function index(array $data)
    {
        $model = AdminOperationLog::query();
        $model = $this->queryCondition($model,$data,'name');
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }
        if (!empty($data['url'])){
            $model = $model->where('url',$data['url']);
        }
        if (!empty($data['pathname'])){
            $model = $model->where('pathname',$data['pathname']);
        }
        if (!empty($data['method'])){
            $model = $model->where('method',$data['method']);
        }
        if (!empty($data['ip'])){
            $model = $model->where('ip',$data['ip']);
        }
        if (isset($data['admin_id']) && is_numeric($data['admin_id']) && $data['admin_id'] > 0){
            $model = $model->where('admin_id',$data['admin_id']);
        }
        $list = $model->with([
                'admin_one'=>function($query){
                    $query->select('id','username');
                }
            ])
            ->orderBy('id','desc')
            ->paginate($data['limit'])
            ->toArray();
        return $this->apiSuccess('',[
            'list'=>$list['data'],
            'total'=>$list['total']
        ]);
    }
    public function del(int $id){
        return $this->commonDestroy(AdminOperationLog::query(),[$id]);
    }
    public function delAll(array $idArr){
        return $this->commonDestroy(AdminOperationLog::query(),$idArr);
    }
}
