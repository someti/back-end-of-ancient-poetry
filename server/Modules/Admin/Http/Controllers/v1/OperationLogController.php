<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 操作日志控制器
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/6/23 12:44
 */

namespace Modules\Admin\Http\Controllers\v1;


use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\Common\Requests\CommonIdArrRequest;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Admin\Services\log\OperationLogService;

class OperationLogController extends BaseApiController
{
    public function index(CommonPageRequest $request)
    {
        return (new OperationLogService())->index($request->only([
            "limit",
            "name",
            "url",
            "pathname",
            "method",
            "ip",
            "admin_id",
            "created_at",
            "sort"
        ]));
    }
    public function del(int $id)
    {
        return (new OperationLogService())->del($id);
    }
    public function delAll(CommonIdArrRequest $request)
    {
        return (new OperationLogService())->delAll($request->get('idArr'));
    }

}
