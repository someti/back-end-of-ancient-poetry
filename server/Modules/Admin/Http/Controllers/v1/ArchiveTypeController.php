<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------


namespace Modules\Admin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\Admin\Services\ArchiveType\ArchiveTypeServices;
use Modules\Common\Requests\CommonSortRequest;
use Modules\Common\Requests\CommonStatusRequest;

class ArchiveTypeController extends BaseApiController
{
    public function index(Request $request){
        return (new ArchiveTypeServices())->index($request->only(['sort']));
    }
    public function add(Request $request)
    {
        return (new ArchiveTypeServices())->add($request->only([
            'name',
            'description',
            "status",
            "level",
            "pid",
            "sort",
        ]));
    }
    public function edit(int $id){
        return (new ArchiveTypeServices())->edit($id);
    }
    public function update(Request $request,int $id)
    {
        return (new ArchiveTypeServices())->update($id,$request->only([
            'name',
            'description',
            "status",
            "level",
            "pid",
            "sort",
        ]));
    }
    public function status(CommonStatusRequest $request,int $id){
        return (new ArchiveTypeServices())->status($id,$request->only([
            "status"
        ]));
    }
    public function sorts(CommonSortRequest $request,int $id){
        return (new ArchiveTypeServices())->sorts($id,$request->only([
            "sort"
        ]));
    }
    public function getPidList(Request $request){
        return (new ArchiveTypeServices())->getPidList($request->get('id'));
    }

    public function del(int $id){
        return (new ArchiveTypeServices())->del($id);
    }
}
