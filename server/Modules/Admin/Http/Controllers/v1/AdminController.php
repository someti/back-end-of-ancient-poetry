<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 管理员管理
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 17:43
 */

namespace Modules\Admin\Http\Controllers\v1;


use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\Admin\Http\Requests\Admin\AddAdminRequest;
use Modules\Admin\Http\Requests\Admin\UpdateAdminRequest;
use Modules\Admin\Services\Admin\AdminServices;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonStatusRequest;

class AdminController extends BaseApiController
{
    /**
     * @OA\Get(
     *     path="/admin/admin/index",
     *     operationId="admin-admin-index",
     *     tags={"Admin"},
     *     summary="管理员列表",
     *     description="管理员列表。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/page"),
     *     @OA\Parameter(ref="#/components/parameters/limit"),
     *     @OA\Parameter(ref="#/components/parameters/status"),
     *     @OA\Parameter(ref="#/components/parameters/created_at[0]"),
     *     @OA\Parameter(ref="#/components/parameters/created_at[1]"),
     *     @OA\Parameter(ref="#/components/parameters/updated_at[0]"),
     *     @OA\Parameter(ref="#/components/parameters/updated_at[1]"),
     *     @OA\Parameter(
     *          name="name",
     *          in="query",
     *          description="姓名",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="username",
     *          in="query",
     *          description="账号",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回具体的列表数据",required={"total","list"},
     *                  @OA\Property(property="total",type="integer",description="数据总条数"),
     *                  @OA\Property(property="list", type="array",description="管理员列表信息",
     *                       @OA\Items(type="object",description="单条管理员信息",required={"id","name","phone","username","group_id","project_id","status","created_at","updated_at","admin_group","admin_project"},
     *                          @OA\Property(property="id", type="integer",description="管理员ID"),
     *                          @OA\Property(property="name", type="string",description="姓名"),
     *                          @OA\Property(property="phone", type="string",description="手机号"),
     *                          @OA\Property(property="username", type="string",description="账号"),
     *                          @OA\Property(property="group_id", type="integer",description="角色ID"),
     *                          @OA\Property(property="project_id", type="integer",description="项目ID"),
     *                          @OA\Property(property="status", type="integer",description="状态:0=禁用,1=启用"),
     *                          @OA\Property(property="created_at", type="date",description="创建时间(Y-m-d H:i:s)"),
     *                          @OA\Property(property="updated_at", type="date",description="更新时间(Y-m-d H:i:s 或 null )"),
     *                          @OA\Property(property="admin_group", type="object",description="角色数据",required={"id","name"},
     *                              @OA\Property(property="id", type="integer",description="角色ID"),
     *                              @OA\Property(property="name", type="string",description="角色名称")
     *                          ),
     *@OA\Property(property="admin_project", type="object",description="项目数据",required={"id","name"},
     *                              @OA\Property(property="id", type="integer",description="项目ID"),
     *                              @OA\Property(property="name", type="string",description="项目名称")
     *                          )
     *                       )
     *                  )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {"list": {{"id": 4,"name": "测试用户","phone": "18092444782","username": "admin","group_id": 1,"project_id": 1,"status": 1,"created_at": "2021-11-23 22:39:46","updated_at": "2021-11-27 18:39:53","admin_group": {"id": 1,"name": "测试角色"},"admin_project": {"id": 1,"name": "测试项目1"}}},"total": 1}}
     *          )
     *       )
     * )
     * @param CommonPageRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function index(CommonPageRequest $request){
        return (new AdminServices())->index($request->only([
            "limit",
            "name",
            "username",
            "created_at",
            "updated_at",
            "status",
            "sort",
            "group_id",
            "project_id"
        ]));
    }


    /**
     * @OA\Post(
     *     path="/admin/admin/add",
     *     operationId="admin-admin-add",
     *     tags={"Admin"},
     *     summary="添加管理员",
     *     description="添加管理员。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"name","group_id","project_id","username","password","password_confirmation","phone","status"},
     *                 @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="姓名",
     *                      default="测试用户"
     *                  ),
     *                  @OA\Property(
     *                      property="group_id",
     *                      type="integer",
     *                      description="角色ID",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="project_id",
     *                      type="integer",
     *                      description="项目ID",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="username",
     *                      type="string",
     *                      description="账号",
     *                      default="admin"
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="string",
     *                      description="密码",
     *                      default="123456"
     *                  ),
     *                  @OA\Property(
     *                      property="password_confirmation",
     *                      type="string",
     *                      description="确认密码",
     *                      default="123456"
     *                  ),
     *                  @OA\Property(
     *                      property="phone",
     *                      type="string",
     *                      description="手机号",
     *                      default="18092444782"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="integer",
     *                      description="状态:0=禁用,1=启用",
     *                      default=1
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param AddAdminRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function add(AddAdminRequest $request)
    {
        return (new AdminServices())->add($request->only([
            'name',
            'group_id',
            'project_id',
            'username',
            'password',
            'phone',
            "status",
        ]));
    }

    /**
     * @OA\Get(
     *     path="/admin/admin/edit/{id}",
     *     operationId="admin-admin-edit",
     *     tags={"Admin"},
     *     summary="编辑管理员页面",
     *     description="获取管理员编辑页面信息。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回具体的管理员数据",required={"id","name","phone","username","group_id","project_id","status"},
     *                  @OA\Property(property="id",type="integer",description="管理员ID"),
     *                  @OA\Property(property="name",type="string",description="姓名"),
     *                  @OA\Property(property="phone",type="string",description="手机号"),
     *                  @OA\Property(property="username",type="string",description="账号"),
     *                  @OA\Property(property="group_id",type="integer",description="角色ID"),
     *                  @OA\Property(property="project_id",type="integer",description="项目ID"),
     *                  @OA\Property(property="status",type="integer",description="状态:0=禁用,1=启用"),
     *              ),
     *              example={"status": 20000,"message": "获取成功！","data": {"id": 1,"name": "测试用户","phone": "18092444782","username": "admin","group_id": 1,"project_id": 1,"status": 1}}
     *          )
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function edit(int $id){
        return (new AdminServices())->edit($id);
    }

    /**
     * @OA\Put(
     *     path="/admin/admin/update/{id}",
     *     operationId="admin-admin-update",
     *     tags={"Admin"},
     *     summary="编辑管理员",
     *     description="编辑管理员提交。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"name","group_id","project_id","username","phone","status"},
     *                 @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="姓名",
     *                      default="测试用户"
     *                  ),
     *                  @OA\Property(
     *                      property="group_id",
     *                      type="integer",
     *                      description="角色ID",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="project_id",
     *                      type="integer",
     *                      description="项目ID",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="username",
     *                      type="string",
     *                      description="账号",
     *                      default="admin"
     *                  ),
     *                  @OA\Property(
     *                      property="phone",
     *                      type="string",
     *                      description="手机号",
     *                      default="18092444782"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="integer",
     *                      description="状态:0=禁用,1=启用",
     *                      default=1
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param UpdateAdminRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function update(UpdateAdminRequest $request,int $id)
    {
        return (new AdminServices())->update($id,$request->only([
            'name',
            'group_id',
            'project_id',
            'username',
            'phone',
            "status",
        ]));
    }

    /**
     * @OA\Put(
     *     path="/admin/admin/status/{id}",
     *     operationId="admin-admin-status",
     *     tags={"Admin"},
     *     summary="管理员调整状态",
     *     description="管理员调整状态。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/status")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param CommonStatusRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function status(CommonStatusRequest $request,int $id){
        return (new AdminServices())->status($id,$request->only([
            "status"
        ]));
    }

    /**
     * @OA\Delete(
     *     path="/admin/admin/del/{id}",
     *     operationId="admin-admin-del",
     *     tags={"Admin"},
     *     summary="管理员删除",
     *     description="管理员删除。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function del(int $id){
        return (new AdminServices())->del($id);
    }

    /**
     * @OA\Put(
     *     path="/admin/admin/resetPwd/{id}",
     *     operationId="admin-admin-resetPwd",
     *     tags={"Admin"},
     *     summary="管理员初始化密码",
     *     description="管理员初始化密码。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function resetPwd(int $id){
        return (new AdminServices())->resetPwd($id);
    }
}
