<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 日志文件管理
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/6/23 16:50
 */

namespace Modules\Admin\Http\Controllers\v1;
use Modules\Admin\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;
use Modules\Admin\Services\Log\StorageService;

class StorageController extends BaseApiController
{
    public function index(){
        return (new StorageService())->index();
    }
    public function getFiles(Request $request) {
        return (new StorageService())->getFiles($request->get('name'));
    }
    public function del(Request $request) {
        return (new StorageService())->delSqlFiles($request->get('nameArr'));
    }
}
