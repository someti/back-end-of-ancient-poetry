<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------


namespace Modules\Admin\Http\Controllers\v1;
use Illuminate\Http\Request;
use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\Admin\Http\Requests\Admin\LoginAdminRequest;
use Modules\Admin\Http\Requests\Admin\UpdatePwdAdminRequest;
use Modules\Admin\Services\Admin\AdminServices;
use Modules\Admin\Services\Auth\AuthServices;
use Modules\Admin\Services\Auth\LoginServices;
use Modules\Admin\Services\Config\ConfigServices;
use Modules\Admin\Services\Group\GroupServices;
use Modules\Admin\Services\Image\ImageServices;
use Modules\Admin\Services\Project\ProjectServices;
use Modules\Admin\Services\Rule\RuleServices;
use Modules\Common\Requests\CommonPageRequest;

class CommonController extends BaseApiController
{
    /**
     * @OA\Get(
     *     path="/admin/common/getGroupList",
     *     operationId="admin-common-getGroupList",
     *     tags={"Common"},
     *     summary="获取角色列表",
     *     description="获取角色列表，用于获取后台的所有角色列表。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="array",description="角色相关数据多条",
     *                  @OA\Items(type="object",description="角色相关数据单条数据",required={"id","name"},
     *                      @OA\Property(property="id", type="integer",description="角色id"),
     *                      @OA\Property(property="name", type="string",description="角色名称")
     *                  )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {{"id": 1,"name": "测试角色"}}}
     *          )
     *       )
     * )
     */

    public function getGroupList(){
       return (new GroupServices())->getGroupList();
    }
    public function getAdminList(){
        return (new AdminServices())->getAdminList();
    }
    /**
     * @OA\Get(
     *     path="/admin/common/getProjectList",
     *     operationId="admin-common-getProjectList",
     *     tags={"Common"},
     *     summary="获取项目列表",
     *     description="获取项目列表，用于获取后台的所有项目列表。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="array",description="项目列表数据",
     *                  @OA\Items(type="object",description="单条项目数据",required={"id","name"},
     *                      @OA\Property(property="id", type="integer",description="项目id"),
     *                      @OA\Property(property="name", type="string",description="项目名称")
     *                  )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {{"id": 1,"name": "测试项目1"}}}
     *          ),
     *       )
     * )
     */
    public function getProjectList(){
        return (new ProjectServices())->getProjectList();
    }

    /**
     * @OA\Post(
     *     path="/admin/common/upload",
     *     operationId="admin-common-upload",
     *     tags={"Common"},
     *     summary="图片上传",
     *     description="图片上传，用于所有后台模块的公共图片上传，适用于单图上传和多图上传。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 required={"file"},
     *                 @OA\Property(
     *                      property="file",
     *                      type="file",
     *                      description="图片"
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回图片信息",required={"image_id","url"},
     *                  @OA\Property(property="image_id", type="integer",description="图片id"),
     *                  @OA\Property(property="url", type="string",description="图片路径")
     *              ),
     *              example={"status": 20000,"message": "上传成功！","data": {"image_id": 3,"url": "http://www.lvuni.com/storage/20211127/XdHgRaTFPczBDnwtAksv9mxIdfXbauVhYOEEeHxx.png"}}
     *          ),
     *       )
     * )
     * @param Request $request
     * @return \Modules\Common\Services\JSON
     */
    public function upload(Request $request){
        return (new ImageServices())->upload($request);
    }

    /**
     * @OA\Get(
     *     path="/admin/common/getImageList",
     *     operationId="admin-common-getImageList",
     *     tags={"Common"},
     *     summary="图片列表",
     *     description="图片列表，当前图片列表目前适用于封装相应的图片上传组件。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/page"),
     *     @OA\Parameter(ref="#/components/parameters/limit"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回具体的列表数据",required={"total","list"},
     *                  @OA\Property(property="total",type="integer",description="数据总条数"),
     *                  @OA\Property(property="list", type="array",description="图片列表数据",
     *                       @OA\Items(type="object",description="图片列表单条数据",required={"id","url","status","open","created_at","updated_at","http_url"},
     *                          @OA\Property(property="id", type="integer",description="图片id"),
     *                          @OA\Property(property="url", type="string",description="图片初始路径"),
     *                          @OA\Property(property="status", type="integer",description="图片状态：0=初始化，1=启用，目前为预留字段"),
     *                          @OA\Property(property="open", type="integer",description="图片类型:1=本地,2=七牛云"),
     *                          @OA\Property(property="created_at", type="date",description="创建时间(Y-m-d H:i:s)"),
     *                          @OA\Property(property="updated_at", type="date",description="更新时间(Y-m-d H:i:s 或 null )"),
     *                          @OA\Property(property="http_url", type="string",description="图片完整路径"),
     *                      )
     *                 )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {"list": {{"id": 1,"url": "/storage/20211127/XdHgRaTFPczBDnwtAksv9mxIdfXbauVhYOEEeHxx.png","status": 0,"open": 1,"created_at": "2021-11-27 18:30:47","updated_at": null,"http_url": "http://www.lvuni.com/storage/20211127/XdHgRaTFPczBDnwtAksv9mxIdfXbauVhYOEEeHxx.png"}},"total": 1}}
     *          ),
     *       )
     * )
     * @param CommonPageRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function getImageList(CommonPageRequest $request){
        return (new ImageServices())->getImageList($request->only([
            "limit"
        ]));
    }
    /**
     * @OA\Post(
     *     path="/admin/common/login",
     *     operationId="admin-common-login",
     *     tags={"Common"},
     *     summary="登录",
     *     description="登录，登录接口可以获取到相应管理员的token、token的有效期、token的类型。",
     *     security={{ "Authorization-key":{} }},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"username","password"},
     *                 @OA\Property(
     *                      property="username",
     *                      type="string",
     *                      description="账号",
     *                      default="admin"
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="string",
     *                      description="密码",
     *                      default="123456"
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回token信息",required={"token","token_type","expires_in"},
     *                   @OA\Property(property="token", type="string",description="token"),
     *                   @OA\Property(property="token_type", type="string",description="token类型"),
     *                   @OA\Property(property="expires_in", type="integer",description="token有效期"),
     *              ),
     *              example={"status": 20000,"message": "登录成功！","data": {"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC93d3cubHZ1bmkuY29tXC9hcGlcL3YxXC9hZG1pblwvY29tbW9uXC9sb2dpbiIsImlhdCI6MTYzODAwODA2NCwiZXhwIjoxNjM4MDExNjY0LCJuYmYiOjE2MzgwMDgwNjQsImp0aSI6IjQxNDQ5bExXcjZRVjhVSXgiLCJzdWIiOjQsInBydiI6IjkxOTI4OGZkZWVjMjI3MDkxMGJmOTUyNjZjY2VhNjE3M2JmNDhiNTkifQ.2eIn87e-A4Q7xfm9hEHE8a0qA86JsB8N6MYbjkuI0kI","token_type": "bearer","expires_in": 3600}}
     *          ),
     *       )
     * )
     */
    public function login(LoginAdminRequest $request){
        return (new LoginServices())->login($request->only([
            "username",
            "password",
            "captcha",
            "key"
        ]));
    }

    public function getCode(){
        return (new LoginServices())->getCode();
    }


    /**
     * @OA\Get(
     *     path="/admin/common/my",
     *     operationId="admin-common-my",
     *     tags={"Common"},
     *     summary="获取管理员信息",
     *     description="获取管理员信息，可以拿到当前管理员的账号信息。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回管理员信息",required={"username"},
     *                   @OA\Property(property="username", type="string",description="管理员账号")
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {"username": "admin"}}
     *          )
     *      )
     * )
     */
    public function my(){
        return (new AuthServices())->my();
    }
    /**
     * @OA\Delete(
     *     path="/admin/common/logout",
     *     operationId="admin-common-logout",
     *     tags={"Common"},
     *     summary="退出登录",
     *     description="退出登录，退出整个后台系统。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *      )
     * )
     */
    public function logout(){
        return (new AuthServices())->logout();
    }
    /**
     * @OA\Put(
     *     path="/admin/common/refreshToken",
     *     operationId="admin-common-refreshToken",
     *     tags={"Common"},
     *     summary="刷新token",
     *     description="刷新token，用于token过期后的刷新，获取到新的token、token类型、token有效期。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回token信息",required={"token","token_type","expires_in"},
     *                   @OA\Property(property="token", type="string",description="token"),
     *                   @OA\Property(property="token_type", type="string",description="token类型"),
     *                   @OA\Property(property="expires_in", type="integer",description="token有效期"),
     *              ),
     *              example={"status": 20000,"message": "刷新成功","data": {"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC93d3cubHZ1bmkuY29tXC9hcGlcL3YxXC9hZG1pblwvY29tbW9uXC9sb2dpbiIsImlhdCI6MTYzODAwODA2NCwiZXhwIjoxNjM4MDExNjY0LCJuYmYiOjE2MzgwMDgwNjQsImp0aSI6IjQxNDQ5bExXcjZRVjhVSXgiLCJzdWIiOjQsInBydiI6IjkxOTI4OGZkZWVjMjI3MDkxMGJmOTUyNjZjY2VhNjE3M2JmNDhiNTkifQ.2eIn87e-A4Q7xfm9hEHE8a0qA86JsB8N6MYbjkuI0kI","token_type": "bearer","expires_in": 3600}}
     *          )
     *      )
     * )
     */
    public function refreshToken(){
        return (new AuthServices())->refreshToken();
    }

    /**
     * @OA\Put(
     *     path="/admin/common/updatePwd",
     *     operationId="admin-common-updatePwd",
     *     tags={"Common"},
     *     summary="修改密码",
     *     description="修改密码，用于后台管理员的密码修改。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"y_password","password","password_confirmation"},
     *                 @OA\Property(
     *                      property="y_password",
     *                      type="string",
     *                      description="原密码",
     *                      default="123456"
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="string",
     *                      description="密码",
     *                      default="123456"
     *                  ),
     *                  @OA\Property(
     *                      property="password_confirmation",
     *                      type="string",
     *                      description="确认密码",
     *                      default="123456"
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param UpdatePwdAdminRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function updatePwd(UpdatePwdAdminRequest $request){
        return (new AuthServices())->updatePwd($request->only([
            "y_password",
            "password"
        ]));
    }

    public function getMenuList(){
        return (new RuleServices())->getMenuList();
    }

    public function setContent(Request $request){
        return (new ConfigServices())->setContent($request->get('content'));
    }


    public function getConfigInfo(){
        return (new ConfigServices())->getConfigInfo();
    }

    public function download(Request $request){
        $url = $request->get('url');
        $filePath = storage_path($url);
        //下载文件
        $filename = basename($filePath);
        header("Content-type: application/octet-stream");
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header("Content-Length: " . filesize($filePath));
        readfile($filePath);
    }
}
