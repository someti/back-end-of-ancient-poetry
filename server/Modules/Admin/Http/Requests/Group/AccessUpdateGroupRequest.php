<?php

namespace Modules\Admin\Http\Requests\Group;

use Illuminate\Foundation\Http\FormRequest;

class AccessUpdateGroupRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pathArr = explode('/',$this->path());
        $id = array_pop($pathArr);
        return [

            "rules"        =>'required|is_array_integer',
        ];
    }

    public function messages()
    {
        return [
            'rules.required'                =>'请选择权限规则',
            'rules.is_array_integer'        =>'规则错误',
        ];
    }
}
