<?php

namespace Modules\Admin\Http\Requests\Group;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGroupRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pathArr = explode('/',$this->path());
        $id = array_pop($pathArr);
        return [
            'name'          => 'required|max:100|unique:admin_projects,name,'.$id,
            "content"       =>'required|max:255',
            "status"        =>'required|is_status_integer',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                 =>'请输入角色名称',
            'name.max'                      =>'角色名称最大长度100个字符',
            'name.unique'                   =>'角色名称已存在',

            'content.required'              =>'请输入角色描述',
            'content.max'                   =>'角色描述最大长度255个字符',

            'status.required'               =>'请选择状态',
            'status.is_status_integer'      =>'您选择的状态异常',
        ];
    }
}
