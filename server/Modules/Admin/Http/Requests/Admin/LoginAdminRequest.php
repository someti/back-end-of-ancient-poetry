<?php

namespace Modules\Admin\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class LoginAdminRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'username'      => 'required|regex:/^[a-zA-Z0-9]{4,14}$/',
            'password'      => 'required|regex:/^[a-zA-Z0-9]{4,14}$/'
        ];
    }

    public function messages()
    {
        return [


            'username.required'             =>'请输入账号',
            'username.regex'                =>'账号必须4到14位的数字或字母',


            'password.required'             =>'请输入密码',
            'password.regex'                =>'密码必须4到14位的数字或字母',

        ];
    }
}
