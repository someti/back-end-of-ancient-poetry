<?php

namespace Modules\Admin\Http\Requests\Rule;

use Illuminate\Foundation\Http\FormRequest;

class AuthOpenRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "auth_open"         =>'required|is_status_integer'
        ];
    }

    public function messages()
    {
        return [
            'auth_open.required'            =>'缺少参数',
            'auth_open.is_status_integer'   =>'参数错误',
        ];
    }
}
