<?php



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


use Illuminate\Support\Facades\Route;


Route::get('v1/admin/test/index', 'v1\TestController@index');

Route::group(["prefix"=>"v1/admin","middleware"=>"AdminApiAuth"],function (){

    // 公共接口
    // 获取权限组列表
    Route::get('/common/getGroupList', 'v1\CommonController@getGroupList');

    // 获取项目列表
    Route::get('/common/getProjectList', 'v1\CommonController@getProjectList');
    // 管理员列表
    Route::get('/common/getAdminList', 'v1\CommonController@getAdminList');
    // 图片上传
    Route::post('/common/upload', 'v1\CommonController@upload');

    // 图片列表
    Route::get('/common/getImageList', 'v1\CommonController@getImageList');

    // 登录
    Route::post('/common/login', 'v1\CommonController@login');

    // 生成验证码
    Route::get('/common/getCode', 'v1\CommonController@getCode');

    // 获取管理员信息
    Route::get('/common/my', 'v1\CommonController@my');

    // 退出登录
    Route::delete('/common/logout', 'v1\CommonController@logout');

    // 刷新token
    Route::put('/common/refreshToken', 'v1\CommonController@refreshToken');

    // 修改密码
    Route::put('/common/updatePwd', 'v1\CommonController@updatePwd');


    // 菜单列表
    Route::get('/common/getMenuList', 'v1\CommonController@getMenuList');


    // 设置远程编辑内容
    Route::post('/common/setContent', 'v1\CommonController@setContent');

    // 获取站点配置
    Route::get('/common/getConfigInfo', 'v1\CommonController@getConfigInfo');


    // 项目管理
    // 列表
    Route::get('/project/index', 'v1\ProjectController@index');

    // 添加
    Route::post('/project/add', 'v1\ProjectController@add');

    // 编辑页面
    Route::get('/project/edit/{id}', 'v1\ProjectController@edit');

    // 编辑提交
    Route::put('/project/update/{id}', 'v1\ProjectController@update');

    // 调整状态
    Route::put('/project/status/{id}', 'v1\ProjectController@status');

    // 删除
    Route::delete('/project/del/{id}', 'v1\ProjectController@del');



    // 角色管理
    // 列表
    Route::get('/group/index', 'v1\GroupController@index');

    // 添加
    Route::post('/group/add', 'v1\GroupController@add');

    // 编辑页面
    Route::get('/group/edit/{id}', 'v1\GroupController@edit');

    // 编辑提交
    Route::put('/group/update/{id}', 'v1\GroupController@update');

    // 调整状态
    Route::put('/group/status/{id}', 'v1\GroupController@status');

    // 权限规则页面
    Route::get('/group/access/{id}', 'v1\GroupController@access');

    // 权限规则提交
    Route::put('/group/accessUpdate/{id}', 'v1\GroupController@accessUpdate');


    // 删除
    Route::delete('/group/del/{id}', 'v1\GroupController@del');



    // 管理员管理
    // 列表
    Route::get('/admin/index', 'v1\AdminController@index');

    // 添加
    Route::post('/admin/add', 'v1\AdminController@add');

    // 编辑页面
    Route::get('/admin/edit/{id}', 'v1\AdminController@edit');

    // 编辑提交
    Route::put('/admin/update/{id}', 'v1\AdminController@update');

    // 调整状态
    Route::put('/admin/status/{id}', 'v1\AdminController@status');

    // 删除
    Route::delete('/admin/del/{id}', 'v1\AdminController@del');

    // 初始化密码
    Route::put('/admin/resetPwd/{id}', 'v1\AdminController@resetPwd');



    // 菜单管理
    // 列表
    Route::get('/rule/index', 'v1\RuleController@index');

    // 添加
    Route::post('/rule/add', 'v1\RuleController@add');

    // 编辑页面
    Route::get('/rule/edit/{id}', 'v1\RuleController@edit');

    // 编辑提交
    Route::put('/rule/update/{id}', 'v1\RuleController@update');

    // 调整状态
    Route::put('/rule/status/{id}', 'v1\RuleController@status');

    // 是否验证权限
    Route::put('/rule/authOpen/{id}', 'v1\RuleController@authOpen');

    // 是否固定面板
    Route::put('/rule/affix/{id}', 'v1\RuleController@affix');

    // 排序
    Route::put('/rule/sorts/{id}', 'v1\RuleController@sorts');

    // 父级菜单列表
    Route::get('/rule/getPidList', 'v1\RuleController@getPidList');

    // 删除
    Route::delete('/rule/del/{id}', 'v1\RuleController@del');



    // 配置页面数据
    Route::get('/config/getConfig/{code}', 'v1\ConfigController@getConfig');

    // 配置页面数据提交
    Route::put('/config/setConfig/{code}', 'v1\ConfigController@setConfig');


    /***********************************数据库管理***************************************/
    //数据表管理
    Route::get('dataBase/tables','v1\DataBaseController@tables');
    // 表详情
    Route::get('dataBase/tableData', 'v1\DataBaseController@tableData');
    // 备份表
    Route::post('dataBase/backUp', 'v1\DataBaseController@backUp');
    // 备份列表
    Route::get('dataBase/restoreData', 'v1\DataBaseController@restoreData');
    // 查询文件详情
    Route::get('dataBase/getFiles', 'v1\DataBaseController@getFiles');
    // 删除
    Route::delete('dataBase/delSqlFiles', 'v1\DataBaseController@delSqlFiles');



    /***********************************操作日志***************************************/
    //操作日志
    Route::get('operationLog/index', 'v1\OperationLogController@index');
    //删除
    Route::delete('operationLog/del/{id}', 'v1\OperationLogController@del');
    //批量删除
    Route::delete('operationLog/delAll', 'v1\OperationLogController@delAll');






    // 日志文件管理
    Route::get('storage/index', 'v1\StorageController@index');
    // 查询文件详情
    Route::get('storage/getFiles', 'v1\StorageController@getFiles');
    // 删除
    Route::delete('storage/del', 'v1\StorageController@del');


    // 部门管理
    Route::get('/department/index', 'v1\DepartmentController@index');

    // 添加
    Route::post('/department/add', 'v1\DepartmentController@add');

    // 编辑页面
    Route::get('/department/edit/{id}', 'v1\DepartmentController@edit');

    // 编辑提交
    Route::put('/department/update/{id}', 'v1\DepartmentController@update');

    // 调整状态
    Route::put('/department/status/{id}', 'v1\DepartmentController@status');

    // 排序
    Route::put('/department/sorts/{id}', 'v1\DepartmentController@sorts');

    // 删除
    Route::delete('/department/del/{id}', 'v1\DepartmentController@del');

    //批量删除
    Route::delete('/department/delAll', 'v1\DepartmentController@delAll');

    // 部门列表
    Route::get('/department/getDepartmentList', 'v1\DepartmentController@getDepartmentList');


    // 人员管理
    Route::get('/personnel/index', 'v1\PersonnelController@index');

    // 添加
    Route::post('/personnel/add', 'v1\PersonnelController@add');

    // 编辑页面
    Route::get('/personnel/edit/{id}', 'v1\PersonnelController@edit');

    // 编辑提交
    Route::put('/personnel/update/{id}', 'v1\PersonnelController@update');

    // 调整状态
    Route::put('/personnel/status/{id}', 'v1\PersonnelController@status');

    // 删除
    Route::delete('/personnel/del/{id}', 'v1\PersonnelController@del');

    //批量删除
    Route::delete('/personnel/delAll', 'v1\PersonnelController@delAll');

    // 人员列表
    Route::get('/personnel/getPersonnelList', 'v1\PersonnelController@getPersonnelList');




    // 档案模板
    Route::get('/archiveType/index', 'v1\ArchiveTypeController@index');
    // 添加
    Route::post('/archiveType/add', 'v1\ArchiveTypeController@add');

    // 编辑页面
    Route::get('/archiveType/edit/{id}', 'v1\ArchiveTypeController@edit');

    // 编辑提交
    Route::put('/archiveType/update/{id}', 'v1\ArchiveTypeController@update');

    // 调整状态
    Route::put('/archiveType/status/{id}', 'v1\ArchiveTypeController@status');

    // 排序
    Route::put('/archiveType/sorts/{id}', 'v1\ArchiveTypeController@sorts');

    // 父级菜单列表
    Route::get('/archiveType/getPidList', 'v1\ArchiveTypeController@getPidList');

    // 删除
    Route::delete('/archiveType/del/{id}', 'v1\ArchiveTypeController@del');



    // 档案列表
    Route::get('/archive/index', 'v1\ArchiveController@index');

    // 添加
    Route::post('/archive/add', 'v1\ArchiveController@add');

    // 编辑页面
    Route::get('/archive/edit/{id}', 'v1\ArchiveController@edit');

    // 编辑提交
    Route::put('/archive/update/{id}', 'v1\ArchiveController@update');

    // 调整状态
    Route::put('/archive/status/{id}', 'v1\ArchiveController@status');

    // 排序
    Route::put('/archive/sorts/{id}', 'v1\ArchiveController@sorts');

    // 删除
    Route::delete('/archive/del/{id}', 'v1\ArchiveController@del');

    //批量删除
    Route::delete('/archive/delAll', 'v1\ArchiveController@delAll');


    // 信息管理
    Route::get('/personnelData/index', 'v1\PersonnelDataController@index');

    // 文件管理
    Route::get('/personnelData/getArchiveList', 'v1\PersonnelDataController@getArchiveList');
});

